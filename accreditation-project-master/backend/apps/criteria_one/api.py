from apps.criteria_one.models import *
from apps.criteria_one.serializers import *
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend

class GraduatedVerificationProcessDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de Proceso de verificación graduados
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = GraduatedVerificationProcessDocument.objects.all()
    serializer_class = GraduatedVerificationProcessDocument_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_criteria']

class BachellorDegreeVerificationProcessDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de Proceso de verificación bachilleres
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = BachellorDegreeVerificationProcessDocument.objects.all()
    serializer_class = BachellorDegreeVerificationProcessDocument_Serializer
    filter_fields = ['id_criteria']

class ProfessionalEngineereVerificationProcessDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de proceso de verificación de ingeniero profesional
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = ProfessionalEngineereVerificationProcessDocument.objects.all()
    serializer_class = ProfessionalEngineereVerificationProcessDocument_Serializer
    filter_fields = ['id_criteria']

class PreProfessionalPracticesDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de Prácticas preprofesionales
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = PreProfessionalPracticesDocument.objects.all()
    serializer_class = PreProfessionalPracticesDocument_Serializer
    filter_fields = ['id_criteria']

class TransferStudentDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de estudiantes tranferidos
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = TransferStudentDocument.objects.all()
    serializer_class = TransferStudentDocument_Serializer
    filter_fields = ['id_criteria']

class StudentProjecFairDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de feria de proyectos estudiantiles
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = StudentProjecFairDocument.objects.all()
    serializer_class = StudentProjecFairDocument_Serializer
    filter_fields = ['id_criteria']

class StudentResearchActivitiesDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de actividades de investigación del estudiante
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = StudentResearchActivitiesDocument.objects.all()
    serializer_class = StudentResearchActivitiesDocument_Serializer
    filter_fields = ['id_criteria']

class StudentWellbeingReportDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de informe de bienestar del estudiante
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = StudentWellbeingReportDocument.objects.all()
    serializer_class = StudentWellbeingReportDocument_Serializer
    filter_fields = ['id_criteria']

class MedicalPsychologicalServiceDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de servicio medico y psicologico
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = MedicalPsychologicalServiceDocument.objects.all()
    serializer_class = MedicalPsychologicalServiceDocument_Serializer
    filter_fields = ['id_criteria']

class NationalInternationalAgreementsDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de acuerdos nacionales e internacionales
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = NationalInternationalAgreementsDocument.objects.all()
    serializer_class = NationalInternationalAgreementsDocument_Serializer
    filter_fields = ['id_criteria']

class StudentMobilityDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de movilidad estudiantil
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = StudentMobilityDocument.objects.all()
    serializer_class = StudentMobilityDocument_Serializer
    filter_fields = ['id_criteria']

class AcademicFridaysDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de viernes academicos
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = AcademicFridaysDocument.objects.all()
    serializer_class = AcademicFridaysDocument_Serializer
    filter_fields = ['id_criteria']

class RegulationDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento reglamentario
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = RegulationDocument.objects.all()
    serializer_class = RegulationDocument_Serializer
    filter_fields = ['id_criteria']

class UnsaAnnualMemories_PresidentReportsDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de Memorias anuales de la Unsa - Informes del presidente
    ENTRADA: id_criteria, doc_annexed, year
    SALIDA: id, id_criteria, doc_annexed, year
    '''
    queryset = UnsaAnnualMemories_PresidentReportsDocument.objects.all()
    serializer_class = UnsaAnnualMemories_PresidentReportsDocument_Serializer
    filter_fields = ['id_criteria']