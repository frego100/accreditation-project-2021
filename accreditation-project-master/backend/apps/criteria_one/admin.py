from django.contrib import admin
from apps.criteria_one.models import *
# Register your models here.

class GraduatedVerificationProcessDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de Proceso de verificación graduados en administración'''
    model   = GraduatedVerificationProcessDocument
    classes = ('collapse',)
    extra   = 0

class BachellorDegreeVerificationProcessDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de Proceso de verificación bachilleres en administración'''
    model   = BachellorDegreeVerificationProcessDocument
    classes = ('collapse',)
    extra   = 0

class ProfessionalEngineereVerificationProcessDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de proceso de verificación de ingeniero profesional en administración'''
    model   = ProfessionalEngineereVerificationProcessDocument
    classes = ('collapse',)
    extra   = 0

class PreProfessionalPracticesDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de Prácticas preprofesionales en administración'''
    model   = PreProfessionalPracticesDocument
    classes = ('collapse',)
    extra   = 0

class TransferStudentDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de estudiantes tranferidos en administración'''
    model   = TransferStudentDocument
    classes = ('collapse',)
    extra   = 0

class StudentProjecFairDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de feria de proyectos estudiantiles en administración'''
    model   = StudentProjecFairDocument
    classes = ('collapse',)
    extra   = 0

class StudentResearchActivitiesDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de actividades de investigación del estudiante en administración'''
    model   = StudentResearchActivitiesDocument
    classes = ('collapse',)
    extra   = 0

class StudentWellbeingReportDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de informe de bienestar del estudiante en administración'''
    model   = StudentWellbeingReportDocument
    classes = ('collapse',)
    extra   = 0

class MedicalPsychologicalServiceDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de servicio medico y psicologico en administracion'''
    model   = MedicalPsychologicalServiceDocument
    classes = ('collapse',)
    extra   = 0

class NationalInternationalAgreementsDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de acuerdos nacionales e internacionales en administración'''
    model   = NationalInternationalAgreementsDocument
    classes = ('collapse',)
    extra   = 0

class StudentMobilityDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de movilidad estudiantil en administración'''
    model   = StudentMobilityDocument
    classes = ('collapse',)
    extra   = 0

class AcademicFridaysDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de viernes academicos en administración'''
    model   = AcademicFridaysDocument
    classes = ('collapse',)
    extra   = 0

class RegulationDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de Regulacion en administración'''
    model   = RegulationDocument
    classes = ('collapse',)
    extra   = 0

class UnsaAnnualMemories_PresidentReportsDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de Memorias anuales de la Unsa - Informes del presidente en administración'''
    model   = UnsaAnnualMemories_PresidentReportsDocument
    classes = ('collapse',)
    extra   = 0


