from django.shortcuts import render
from apps.criteria_one.api import *
from rest_framework import routers
from django.urls import path

router = routers.SimpleRouter()
router.register('graduated_verification_process', GraduatedVerificationProcessDocument_API)
router.register('bachellor_degree_verification_process', BachellorDegreeVerificationProcessDocument_API)
router.register('professional_engineer_verification_process', ProfessionalEngineereVerificationProcessDocument_API)

router.register('preprofessional_practices', PreProfessionalPracticesDocument_API)
router.register('transfer_student', TransferStudentDocument_API)
router.register('student_project_fair', StudentProjecFairDocument_API)

router.register('student_research_activities', StudentResearchActivitiesDocument_API)
router.register('student_wellbeing_report', StudentWellbeingReportDocument_API)
router.register('medical_psychological_service', MedicalPsychologicalServiceDocument_API)

router.register('national_international_agreements', NationalInternationalAgreementsDocument_API)
router.register('student_mobility', StudentMobilityDocument_API)
router.register('academic_fridays', AcademicFridaysDocument_API)
router.register('regulation', RegulationDocument_API)
router.register('unsa_annual_memories_president_reports', UnsaAnnualMemories_PresidentReportsDocument_API)

urlpatterns = router.urls