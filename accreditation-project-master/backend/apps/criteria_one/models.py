from apps.process.models import *
from apps.portfolio.models import *
from django.core.validators import FileExtensionValidator, MinValueValidator, MaxValueValidator
from django.db import models

# Create your models here.

def get_filename(instance): return instance.name.split("/")[-1] if instance else None

def Criteria_path(Criteria): return f'documents/{Criteria.id_criteria}'
'''Función para obtener la dirección donde se almacena los documentos de un criterio'''


def GraduatedVerificationProcessDocument_path(instance, filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

def BachellorDegreeVerificationProcessDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

def ProfessionalEngineereVerificationProcessDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

def PreProfessionalPracticesDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

def TransferStudentDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

def StudentProjecFairDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

def StudentResearchActivitiesDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

def StudentWellbeingReportDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

def MedicalPsychologicalServiceDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

def NationalInternationalAgreementsDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

def StudentMobilityDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

def AcademicFridaysDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

def RegulationDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de processria

def UnsaAnnualMemories_PresidentReportsDocument_path(instance,filename): return f'{Criteria_path(instance)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un criterio'''
#id_criteria de process

class GraduatedVerificationProcessDocument(models.Model):
    '''Modelo de documento de Proceso de verificación graduados de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de Proceso de verificación graduados
    doc_annexed = models.FileField(null=False,blank=False,upload_to=GraduatedVerificationProcessDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de Proceso de verificación graduados
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del documento
    class Meta:
        verbose_name = 'documento de Proceso de verificación graduados del criterio (por periodo)'
        verbose_name_plural = 'documentos de Proceso de verificación graduados del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento Proceso de verificación graduados | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class BachellorDegreeVerificationProcessDocument(models.Model):
    '''Modelo de documento de Proceso de verificación bachiller de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de proceso de verificación bachiller
    doc_annexed = models.FileField(null=False,blank=False,upload_to=BachellorDegreeVerificationProcessDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de proceso de verificación bachiller
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del documento
    class Meta:
        verbose_name = 'documento de Proceso de verificación bachiller del criterio (por periodo)'
        verbose_name_plural = 'documentos de Proceso de verificación bachiller del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento Proceso de verificación bachiller | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class ProfessionalEngineereVerificationProcessDocument(models.Model):
    '''Modelo de documento de proceso de verificación de ingeniero profesional de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de proceso de verificación de ingeniero profesional
    doc_annexed = models.FileField(null=False,blank=False,upload_to=ProfessionalEngineereVerificationProcessDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de proceso de verificación de ingeniero profesional 
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del año
    class Meta:
        verbose_name = 'documento de proceso de verificación de ingeniero profesional del criterio (por periodo)'
        verbose_name_plural = 'documentos de proceso de verificación de ingeniero profesional del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento proceso de verificación de ingeniero profesional | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class PreProfessionalPracticesDocument(models.Model):
    '''Modelo de documento de Prácticas preprofesionales de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de Prácticas preprofesionales
    doc_annexed = models.FileField(null=False,blank=False,upload_to=PreProfessionalPracticesDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de Prácticas preprofesionales 
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del año
    class Meta:
        verbose_name = 'documento de Prácticas preprofesionales del criterio (por periodo)'
        verbose_name_plural = 'documentos de Prácticas preprofesionales del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento Prácticas preprofesionales | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class TransferStudentDocument(models.Model):
    '''Modelo de documento de estudiantes tranferidos de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de estudiantes tranferidos
    doc_annexed = models.FileField(null=False,blank=False,upload_to=TransferStudentDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de proceso de estudiantes tranferidos
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del año
    class Meta:
        verbose_name = 'documento de estudiantes tranferidos del criterio (por periodo)'
        verbose_name_plural = 'documentos de estudiantes tranferidos del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento estudiantes tranferidos | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class StudentProjecFairDocument(models.Model):
    '''Modelo de documento de feria de proyectos estudiantiles de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de feria de proyectos estudiantiles
    doc_annexed = models.FileField(null=False,blank=False,upload_to=StudentProjecFairDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de proceso de feria de proyectos estudiantiles
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del año
    class Meta:
        verbose_name = 'documento de feria de proyectos estudiantiles del criterio (por periodo)'
        verbose_name_plural = 'documentos de feria de proyectos estudiantiles del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento feria de proyectos estudiantiles | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class StudentResearchActivitiesDocument(models.Model):
    '''Modelo de documento de actividades de investigación del estudiante de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de actividades de investigación del estudiante
    doc_annexed = models.FileField(null=False,blank=False,upload_to=StudentResearchActivitiesDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de proceso de actividades de investigación del estudiante
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del año
    class Meta:
        verbose_name = 'documento de actividades de investigación del estudiante del criterio (por periodo)'
        verbose_name_plural = 'documentos de actividades de investigación del estudiante del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento actividades de investigación del estudiante | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class StudentWellbeingReportDocument(models.Model):
    '''Modelo de documento de actividades de informe de bienestar del estudiante de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de informe de bienestar del estudiante
    doc_annexed = models.FileField(null=False,blank=False,upload_to=StudentWellbeingReportDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de proceso de informe de bienestar del estudiante
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del año
    class Meta:
        verbose_name = 'documento de informe de bienestar del estudiante del criterio (por periodo)'
        verbose_name_plural = 'documentos de informe de bienestar del estudiante del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento informe de bienestar del estudiante | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class MedicalPsychologicalServiceDocument(models.Model):
    '''Modelo de documento de actividades de servicio medico y psicologico de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de servicio medico y psicologico
    doc_annexed = models.FileField(null=False,blank=False,upload_to=MedicalPsychologicalServiceDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de proceso de servicio medico y psicologico
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del año
    class Meta:
        verbose_name = 'documento de servicio medico y psicologico del criterio (por periodo)'
        verbose_name_plural = 'documentos de servicio medico y psicologico del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento servicio medico y psicologico | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class NationalInternationalAgreementsDocument(models.Model):
    '''Modelo de documento de acuerdos nacionales e internacionales de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de acuerdos nacionales e internacionales
    doc_annexed = models.FileField(null=False,blank=False,upload_to=NationalInternationalAgreementsDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de proceso de acuerdos nacionales e internacionales
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del año
    class Meta:
        verbose_name = 'documento de acuerdos nacionales e internacionales del criterio (por periodo)'
        verbose_name_plural = 'documentos de acuerdos nacionales e internacionales del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento acuerdos nacionales e internacionales | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class StudentMobilityDocument(models.Model):
    '''Modelo de documento de movilidad estudiantil de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de movilidad estudiantil
    doc_annexed = models.FileField(null=False,blank=False,upload_to=StudentMobilityDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de proceso de movilidad estudiantil
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del año
    class Meta:
        verbose_name = 'documento de movilidad estudiantil del criterio (por periodo)'
        verbose_name_plural = 'documentos de movilidad estudiantil del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento movilidad estudiantil | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class AcademicFridaysDocument(models.Model):
    '''Modelo de documento de viernes academicos de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de viernes academicos
    doc_annexed = models.FileField(null=False,blank=False,upload_to=AcademicFridaysDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de proceso de viernes academicos
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del año
    class Meta:
        verbose_name = 'documento de viernes academicos del criterio (por periodo)'
        verbose_name_plural = 'documentos de viernes academicos del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento viernes academicos | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class RegulationDocument(models.Model):
    '''Modelo de documento reglamentario de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento reglamentario
    doc_annexed = models.FileField(null=False,blank=False,upload_to=RegulationDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de proceso reglamentario
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del año
    class Meta:
        verbose_name = 'documento reglamentario del criterio (por periodo)'
        verbose_name_plural = 'documentos reglamentario del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento reglamentario | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class UnsaAnnualMemories_PresidentReportsDocument(models.Model):
    '''Modelo de documento de Memorias anuales de la Unsa - Informes del presidente de criterio en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='Criteria')
    # id del criterio al que pertenece el documento de Memorias anuales de la Unsa - Informes del presidente
    doc_annexed = models.FileField(null=False,blank=False,upload_to=UnsaAnnualMemories_PresidentReportsDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de proceso de Memorias anuales de la Unsa - Informes del presidente
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año del año
    class Meta:
        verbose_name = 'documento de Memorias anuales de la Unsa - Informes del presidente del criterio (por periodo)'
        verbose_name_plural = 'documentos de Memorias anuales de la Unsa - Informes del presidente del criterio (por periodo)'

    def __str__(self): return f'[{self.id}] documento de Memorias anuales de la Unsa - Informes del presidente | {self.id_criteria}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)