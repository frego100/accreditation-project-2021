from apps.criteria_one.models import *
from rest_framework import serializers

class GraduatedVerificationProcessDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento proceso de verificación de ingeniero profesional'''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = GraduatedVerificationProcessDocument
        fields = '__all__'

class BachellorDegreeVerificationProcessDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de proceso de verificación de ingeniero profesional'''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = BachellorDegreeVerificationProcessDocument
        fields = '__all__'

class ProfessionalEngineereVerificationProcessDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de proceso de verificación de ingeniero profesional '''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = ProfessionalEngineereVerificationProcessDocument
        fields = '__all__'

class PreProfessionalPracticesDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de Prácticas preprofesionales '''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = PreProfessionalPracticesDocument
        fields = '__all__'

class TransferStudentDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de estudiantes tranferidos '''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = TransferStudentDocument
        fields = '__all__'

class StudentProjecFairDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de feria de proyectos estudiantiles '''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = StudentProjecFairDocument
        fields = '__all__'

class StudentResearchActivitiesDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de actividades de investigación del estudiante '''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = StudentResearchActivitiesDocument
        fields = '__all__'

class StudentWellbeingReportDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de informe de bienestar del estudiante '''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = StudentWellbeingReportDocument
        fields = '__all__'

class MedicalPsychologicalServiceDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de servicio medico y psicologico '''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = MedicalPsychologicalServiceDocument
        fields = '__all__'

class NationalInternationalAgreementsDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de acuerdos nacionales e internacionales '''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = NationalInternationalAgreementsDocument
        fields = '__all__'

class StudentMobilityDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de movilidad estudiantil '''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = StudentMobilityDocument
        fields = '__all__'

class AcademicFridaysDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de viernes academicos '''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = AcademicFridaysDocument
        fields = '__all__'

class RegulationDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento reglamentario '''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = RegulationDocument
        fields = '__all__'

class UnsaAnnualMemories_PresidentReportsDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de Memorias anuales de la Unsa - Informes del presidente '''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = UnsaAnnualMemories_PresidentReportsDocument
        fields = '__all__'