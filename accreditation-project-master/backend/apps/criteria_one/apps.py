from django.apps import AppConfig

class CriteriaoneConfig(AppConfig):
    name = 'criteria_one'
