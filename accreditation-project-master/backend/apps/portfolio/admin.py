from apps.portfolio.models import *
from django.contrib import admin
from multiselectfield import MultiSelectField

'''Clases de los modelos de la aplicación de Portafolio en administración'''

# GENERAL ═════════════════════════════════════════════════════════════════════════════════════════

class Subarea_Inline(admin.TabularInline):
    ''''Clase en linea de Subáreas en administración'''
    model = Subarea
    extra = 0

class FormationArea_Admin(admin.ModelAdmin):
    ''''Clase principal de Áreas de Formación en administración'''
    list_display       = ('id','name','subarea_count','status')
    list_display_links = ('name',)
    list_filter        = ('status',)
    search_fields      = ('name',)
    readonly_fields    = ('created','updated')
    inlines            = (Subarea_Inline,)
    list_per_page      = 25
    def subarea_count(self, obj): return obj.subarea_set.count()
    subarea_count.short_description = 'numero de subáreas'

class Department_Admin(admin.ModelAdmin):
    ''''Clase principal de Departamentos en administración'''
    list_display       = ('id','name','code','status')
    list_display_links = ('name',)
    list_filter        = ('status',)
    search_fields      = ('name',)
    readonly_fields    = ('created','updated')
    list_per_page      = 25

class Template_Admin(admin.ModelAdmin):
    ''''Clase principal de Plantillas en administración'''
    list_display = ('get_basename','get_ext',)
    search_fields = ('template',)
    list_per_page = 25
    def get_basename_ext(self,obj):
        filename = obj.template.name.split("/")[-1]
        ext = obj.template.name.split('.')[-1]
        size = len(filename)-len(ext)-1
        basename = filename[:size]
        return basename, ext
    def get_basename(self,obj): return self.get_basename_ext(obj)[0]
    get_basename.short_description = 'nombre'
    def get_ext(self,obj): return self.get_basename_ext(obj)[1]
    get_ext.short_description = 'extensión'

# RECURSOS ════════════════════════════════════════════════════════════════════════════════════════

class Resource_Admin(admin.ModelAdmin):
    ''''Clase principal de Recursos en administración'''
    list_display       = ('id','name','id_process','status')
    list_display_links = ('name',)
    list_filter        = ('status','id_process')
    search_fields      = ('name',)
    readonly_fields    = ('created','updated')
    list_per_page      = 25

# RESULTADOS DEL ESTUDIANTE ───────────────────────────────────────────────────

class Criteria_Inline(admin.StackedInline):
    ''''Clase en linea de Criterios de Resultados del estudiante en administración'''
    model     = Criteria
    extra     = 0
    fieldsets = (
        (None,{'classes':('wide','extrapretty'),'fields':(('name','level_suggested'),)}),
        ('Otros datos',{'classes':('collapse',),'fields':('description','level1','level2','level3','level4')}),
    )

class StudentResult_Admin(admin.ModelAdmin):
    ''''Clase principal de Resultados del estudiante en administración'''
    list_display       = ('id','name','resource','process','status')
    list_display_links = ('name',)
    list_filter        = ('status','id_resource__id_process')
    search_fields      = ('name',)
    readonly_fields    = ('created','updated')
    inlines            = (Criteria_Inline,)
    list_per_page      = 25
    def resource(self, obj): return obj.id_resource.name
    resource.short_description = 'recurso'
    def process(self, obj): return obj.id_resource.id_process
    process.short_description = 'proceso'

# COMPETENCIAS ────────────────────────────────────────────────────────────────

class Competence_Admin(admin.ModelAdmin):
    ''''Clase principal de Competencias en administración'''
    list_display       = ('id','name','resource','process','status')
    list_display_links = ('name',)
    list_filter        = ('status','id_resource__id_process')
    search_fields      = ('name',)
    readonly_fields    = ('created','updated')
    filter_horizontal  = ('student_results',)
    list_per_page      = 25
    def resource(self, obj): return obj.id_resource.name
    resource.short_description = 'recurso'
    def process(self, obj): return obj.id_resource.id_process
    process.short_description = 'proceso'

# CURSOS ──────────────────────────────────────────────────────────────────────

class Course_Competence_Inline(admin.TabularInline):
    ''''Clase en linea de Competencias por Curso en administración'''
    model   = Course_Competence
    classes = ('collapse',)
    extra   = 0

class Course_StudentResult_Inline(admin.TabularInline):
    ''''Clase en linea de Resultados de estudiante por Curso en administración'''
    model   = Course_StudentResult
    classes = ('collapse',)
    extra   = 0

class Course_Admin(admin.ModelAdmin):
    ''''Clase principal de Cursos en administración'''
    list_display       = ('id','code','name','resource','process','id_subarea','type','status')
    list_display_links = ('code','name')
    list_filter        = ('status','type','id_resource__id_process')
    search_fields      = ('name','code')
    readonly_fields    = ('created','updated')
    filter_horizontal  = ('departments','pre_requirements')
    inlines            = (Course_Competence_Inline,Course_StudentResult_Inline)
    list_per_page      = 25
    fieldsets = (
        (None,{'classes':('wide','extrapretty'),'fields':(
            ('id_resource','id_subarea'),('code','name'),('semester','credit'),
            ('hours_theory','hours_seminar','hours_theopractice','hours_practice','hours_laboratory'),
            'type','elective','status'
        )}),
        ('Otros datos',{'classes':('collapse',),'fields':(('description','content'),'departments','pre_requirements')}),
        (None,{'fields':('created','updated')}),
    )
    def resource(self, obj): return obj.id_resource.name
    resource.short_description = 'recurso'
    def process(self, obj): return obj.id_resource.id_process
    process.short_description = 'proceso'

# PORTAFOLIO ══════════════════════════════════════════════════════════════════════════════════════

class CourseActive_Inline(admin.TabularInline):
    ''''Clase en linea de Curso Activo en administración'''
    model   = CourseActive
    classes = ('collapse',)
    fields  = ('id_course','status')
    extra   = 0

class Portfolio_Admin(admin.ModelAdmin):
    ''''Clase principal de Portafolios en administración'''
    list_display       = ('id','semester_complete','id_process','id_user','courses_count','status')
    list_display_links = ('semester_complete',)
    list_filter        = ('id_process','year','semester','status')
    readonly_fields    = ('created','updated')
    filter_horizontal  = ('users',)
    inlines            = (CourseActive_Inline,)
    list_per_page      = 25
    fieldsets = (
        (None,{'classes':('wide','extrapretty'),'fields':(
            ('id_process','id_user'),('semester','year'),'start_date',
            ('end_phase1','end_phase2','end_phase3','end_phase4'),
            'users','status','created','updated',
            'collaborators',
        )}),
    )
    def courses_count(self, obj): return obj.courseactive_set.count()
    courses_count.short_description = 'numero de cursos'

# CURSOS ACTIVOS ──────────────────────────────────────────────────────────────

class CourseActive_User_Inline(admin.TabularInline):
    ''''Clase en linea de Responsables de Curso Activo en administración'''
    model   = CourseActive_User
    classes = ('collapse',)
    extra   = 0

class Stage_Inline(admin.StackedInline):
    ''''Clase en linea de Etapas en Curso Activo en administración'''
    model           = Stage
    classes         = ('collapse',)
    readonly_fields = ('stage',)
    max_num         = 4
    extra           = 0
    fieldsets = (
        (None,{'fields':(('stage','status'),)}),
        ('Observacion',{'classes':('collapse',),'fields':('observation',)})
    )

class EntranceExamination_Inline(admin.StackedInline):
    ''''Clase en linea de Exámenes de Entrada en administración'''
    model   = EntranceExamination
    classes = ('collapse',)
    extra   = 0

class Material_Inline(admin.TabularInline):
    ''''Clase en linea de Material en administración'''
    model   = Material
    classes = ('collapse',)
    extra   = 0

class ContinuousImprovement_Inline(admin.TabularInline):
    ''''Clase en linea de Mejoras Continuas en administración'''
    model   = ContinuousImprovement
    classes = ('collapse',)
    extra   = 0

class ProposalImprovement_Inline(admin.TabularInline):
    ''''Clase en linea de Propuestas de Mejora en administración'''
    model   = ProposalImprovement
    classes = ('collapse',)
    extra   = 0

class AcademicDevelopment_Inline(admin.TabularInline):
    ''''Clase en linea de Desarrollo Académico en administración'''
    model   = AcademicDevelopment
    classes = ('collapse',)
    extra   = 0

class CourseActive_StudentResult_Descriptions_Inline(admin.StackedInline):
    ''''Clase en linea de Resultados de Estudiante por Curso Activo en administración'''
    model   = CourseActive_StudentResult_Descriptions
    classes = ('collapse',)
    extra   = 0
    fieldsets = (
        (None,{'fields':('id_course_student_result',)}),
        ('Otros datos',{'classes':('collapse',),'fields':('description',)}),
    )

class CourseActive_Competence_Descriptions_Inline(admin.StackedInline):
    ''''Clase en linea de Competencias por Curso Activo en administración'''
    model   = CourseActive_Competence_Descriptions
    classes = ('collapse',)
    extra   = 0
    fieldsets = (
        (None,{'fields':('id_course_competence',)}),
        ('Otros datos',{'classes':('collapse',),'fields':('description',)}),
    )

# COMENTARIOS ═════════════════════════════════════════════════════════════════════════════════════════

class Observation_Inline(admin.StackedInline):
    model = Observation
    classes = ('collapse',)
    extra   = 0




class CourseActive_Admin(admin.ModelAdmin):
    ''''Clase principal de Curso Activo en administración'''
    list_display       = ('id','course','portfolio','process','users_count','status')
    list_display_links = ('course',)
    list_filter        = ('status','id_portfolio__id_process','id_portfolio__year','id_portfolio__semester')
    search_fields      = ('id_course__name',)
    readonly_fields    = ('created','updated')
    inlines            = (CourseActive_User_Inline,Stage_Inline,EntranceExamination_Inline,Observation_Inline,Material_Inline,
                         ContinuousImprovement_Inline,ProposalImprovement_Inline,AcademicDevelopment_Inline,
                         CourseActive_StudentResult_Descriptions_Inline,CourseActive_Competence_Descriptions_Inline)
    list_per_page      = 25
    fieldsets = (
        (None,{'classes':('wide','extrapretty'),'fields':(('id_portfolio','id_course'),'silabus_dufa','status','created','updated')}),
        ('Otros datos',{'classes':('collapse',),'fields':('issues','laboratories','methodology','evaluate')}),
    )
    def course(self, obj): return obj.id_course.name
    course.short_description = 'curso'
    def portfolio(self, obj): return obj.id_portfolio.semester_complete()
    portfolio.short_description = 'portafolio'
    def process(self, obj): return obj.id_portfolio.id_process
    process.short_description = 'proceso'
    def users_count(self, obj): return obj.courseactive_user_set.count()
    users_count.short_description = 'numero de responsables'

# class FacultyVitaeAbet_Inline(admin.StackedInline):
#     model   = FacultyVitaeAbet
#     classes = ('collapse',)
#     extra   = 0

class CourseActive_User_Admin(admin.ModelAdmin):
    ''''Clase principal de Responsables de Curso Activo en administración'''
    list_display       = ('id','id_user','course')
    list_display_links = ('id_user',)
    list_filter        = ('id_course_active__id_portfolio__id_process',)
    search_fields      = ('id_course_active__id_course__name','id_user__first_name')
    # inlines            = (FacultyVitaeAbet_Inline,)
    list_per_page      = 25
    fields = (('id_course_active','id_user'),('group_thery','group_laboratory'))
    def course(self, obj): return obj.id_course_active.id_course.name
    course.short_description = 'curso'

# PERIODO ACADEMICO ───────────────────────────────────────────────────────────

class Evidence_Inline(admin.TabularInline):
    ''''Clase en linea de Evidencias en administración'''
    model   = Evidence
    classes = ('collapse',)
    extra   = 0

class EvidenceDocument_Inline(admin.TabularInline):
    ''''Clase en linea de Documentos de Evidencia en administración'''
    model   = EvidenceDocument
    classes = ('collapse',)
    extra   = 0

class PeriodAcademic_StudentResult_Inline(admin.StackedInline):
    ''''Clase en linea de Periodos Académicos en administración'''
    model   = PeriodAcademic_StudentResult
    classes = ('collapse',)
    extra   = 0
    fieldsets = (
        (None,{'fields':('id_course_student_result',)}),
        ('Otros datos',{'classes':('collapse',),'fields':('description','method','result')}),
    )

class PeriodAcademic_Competence_Inline(admin.StackedInline):
    ''''Clase en linea de Competencias por Periodos Académicos en administración'''
    model   = PeriodAcademic_Competence
    classes = ('collapse',)
    extra   = 0
    fieldsets = (
        (None,{'fields':('id_course_competence',)}),
        ('Otros datos',{'classes':('collapse',),'fields':('description',)}),
    )

class PeriodAcademic_Admin(admin.ModelAdmin):
    ''''Clase principal de Resultados del Estudiante por Periodos Académicos en administración'''
    list_display       = ('id','period_complete','course')
    list_display_links = ('period_complete',)
    list_filter        = ('id_course_active__id_portfolio__id_process','period','modality')
    search_fields      = ('id_course_active__id_course__name',)
    inlines            = (Evidence_Inline,EvidenceDocument_Inline,
                         PeriodAcademic_StudentResult_Inline,PeriodAcademic_Competence_Inline)
    list_per_page      = 25
    fields = (('id_course_active','period','modality','note'),)
    def course(self, obj): return obj.id_course_active.id_course.name
    course.short_description = 'curso'

class Evaluation_Inline(admin.StackedInline):
    ''''Clase en linea de Evaluación en administración'''
    model   = Evaluation
    classes = ('collapse',)
    extra   = 0
    fieldsets = (
        (None,{'fields':(('id_criteria','review_week','aspect'),)}),
        ('Otros datos',{'classes':('collapse',),'fields':('criteria','level1','level2','level3','level4')}),
    )

class Evidence_Admin(admin.ModelAdmin):
    ''''Clase principal de Evidencia en administración'''
    list_display       = ('id','period_complete','course')
    list_display_links = ('period_complete',)
    list_filter        = ('id_period_academic__id_course_active__id_portfolio__id_process',
                         'id_period_academic__period','id_period_academic__modality')
    search_fields      = ('id_period_academic__id_course_active__id_course__name',)
    inlines            = (Evaluation_Inline,)
    list_per_page      = 25
    def period_complete(self,obj): return f'(Evidencia) {obj.id_period_academic.period_complete()}'
    period_complete.short_description = '(Evidencia) periodo academico'
    def course(self, obj): return obj.id_period_academic.id_course_active.id_course.name
    course.short_description = 'curso'

# DESARROLLO ACADEMICO ────────────────────────────────────────────────────────

class GradeCriteria_Inline(admin.TabularInline):
    ''''Clase en linea de Notas por Criterios en administración'''
    model   = GradeCriteria
    classes = ('collapse',)
    extra   = 0

class GradePeriod_Inline(admin.TabularInline):
    ''''Clase en linea de Notas por Periodo en administración'''
    model   = GradePeriod
    classes = ('collapse',)
    extra   = 0

class Incidence_Inline(admin.StackedInline):
    ''''Clase en linea de Incidencias en administración'''
    model   = Incidence
    classes = ('collapse',)
    fields  = (('incidence','solution'),('quantity','date','evidence'))
    extra   = 0

class Student_Admin(admin.ModelAdmin):
    ''''Clase principal de Estudiantes en administración'''
    list_display       = ('id','full_name','cui','course','portfolio','process','status')
    list_display_links = ('full_name',)
    list_filter        = ('status','id_course_active__id_portfolio__id_process','id_course_active__id_portfolio__year','id_course_active__id_portfolio__semester')
    search_fields      = ('full_name','cui','id_course_active__id_course__name')
    fields             = ('id_course_active',('cui','full_name'),'grades_entrance')
    inlines            = (GradeCriteria_Inline,GradePeriod_Inline,Incidence_Inline)
    list_per_page      = 25
    def course(self, obj): return obj.id_course_active.id_course.name
    course.short_description = 'curso'
    def portfolio(self, obj): return obj.id_course_active.id_portfolio.semester_complete()
    portfolio.short_description = 'portafolio'
    def process(self, obj): return obj.id_course_active.id_portfolio.id_process
    process.short_description = 'proceso'



# REGISTROS ═══════════════════════════════════════════════════════════════════════════════════════
'''Registro de los modelos de la aplicación de Portafolio'''
admin.site.register(FormationArea,FormationArea_Admin)
admin.site.register(Department,Department_Admin)
admin.site.register(Template,Template_Admin)

admin.site.register(Resource,Resource_Admin)
admin.site.register(StudentResult,StudentResult_Admin)
admin.site.register(Competence,Competence_Admin)
admin.site.register(Course,Course_Admin)

admin.site.register(Portfolio,Portfolio_Admin)
admin.site.register(CourseActive,CourseActive_Admin)
admin.site.register(CourseActive_User,CourseActive_User_Admin)
admin.site.register(PeriodAcademic,PeriodAcademic_Admin)
admin.site.register(Evidence,Evidence_Admin)
admin.site.register(Student,Student_Admin)