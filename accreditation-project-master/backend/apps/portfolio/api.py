from apps.portfolio.models import *
from apps.portfolio.serializers import *
from apps.user.models import *
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework import viewsets, generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.response import Response

# GENERAL ──────────────────────────────────────────────────

class FormationArea_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de áreas de formación
    ENTRADA: id_resource, name, status
    SALIDA: id, id_resource, name, status
    '''
    queryset = FormationArea.objects.all()
    serializer_class = FormationArea_Serializer

class Subarea_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de áreas de formación
    ENTRADA: id_formation_area, name, status
    SALIDA: id, id_formation_area, name, status
    '''
    queryset = Subarea.objects.all()
    serializer_class = Subarea_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_formation_area',]

class Department_API(generics.ListAPIView):
    '''
    API general para todo el CRUD de Departamentos de un curso de malla
    ENTRADA: code, name, status
    SALIDA: id, code, name, status
    '''
    queryset = Department.objects.all()
    serializer_class = Department_Serializer

class Template_API(generics.ListAPIView):
    '''
    API general para las Plantillas en Recursos
    SALIDA: template
    '''
    queryset = Template.objects.all()
    serializer_class = Template_Serializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('template',)

# RECURSOS ──────────────────────────────────────────────────

def resource_permissions(user, action):
    ''' Método para definir permisos '''
    if user.id_role:
        permission = user.id_role.permission_set.values_list('permission','module')
        process = user.id_role.id_process
        if permission[0]:
            for permiso in permission:
                if action == 'create' and permiso[0] == Permission.CREATE[0]:
                    return True
                elif action in ('list', 'retrieve') and permiso[0] == Permission.READ[0]:
                    print('view')
                    return True
                elif action in ('update', 'partial_update') and permiso[0] == Permission.UPDATE[0]:
                    return True
                elif action == 'destroy' and permiso[0] == Permission.DELETE[0]:
                    return True        
    return False

def wPermission(user):
    permission = user.id_role.permission_set.values_list('permission','module')
    if not permission: return True
    return False

class Resource_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de recursos
    ENTRADA: id_process, id_user, name, curriculum_grid, description, percent_due, status
    SALIDA: id, id_process, id_user, name, curriculum_grid, description, percent_due, status
    '''
    queryset = Resource.objects.all()
    serializer_class = Resource_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_process',]

# RESULTADOS DEL ESTUDIANTE ──────────────────────────────────────────────────

class StudentResult_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de resultados del estudiante
    ENTRADA: id_resource, name, description, status
    SALIDA: id, id_resource, name, description, status
    '''
    queryset = StudentResult.objects.all()
    serializer_class = StudentResult_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_resource',]

class Criteria_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de resultados del estudiante
    ENTRADA: id_student_result, name, description,
    level1, level2, level3, level4, level_suggested[], status
    SALIDA: id, id_student_result, name, description,
    level1, level2, level3, level4, level_suggested[], status
    LEVEL_SUGGESTED[]:\n- 0:conoce\n- 1:comprende\n- 2:aplica en un nivel intermedio\n- 3:logra el resultado del estudiante
    '''
    queryset = Criteria.objects.all()
    serializer_class = Criteria_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_student_result',]

# COMPETENCIAS ──────────────────────────────────────────────────

class Competence_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de competencias
    ENTRADA: id_resource, name, description, status, student_results[]
    SALIDA: id, id_resource, name, description, status, student_results[]
    '''
    queryset = Competence.objects.all()
    serializer_class = Competence_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_resource',]

# CURSOS ──────────────────────────────────────────────────

class Course_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de curso
    ENTRADA: id_resource, id_subarea, code, name, description, content, semester, credit,
    hours_theory, hours_seminar, hours_theopractice, hours_practice, hours_laboratory,
    type, elective, status, departments[], pre_requirements[], student_results[], competences[]
    SALIDA: id, id_resource, id_subarea, code, name, description, content, semester, credit,
    hours_theory, hours_seminar, hours_theopractice, hours_practice, hours_laboratory,
    type, elective, status, departments[], pre_requirements[], student_results[], competences[]
    TYPE:\n- 0:control\n- 1:capstone
    '''
    queryset = Course.objects.all()
    serializer_class =  Course_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_resource','id_subarea','semester']

class Course_StudentResult_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Curso por Resultado de estudiante
    ENTRADA: id_course, id_student_result, level
    SALIDA: id, id_course, id_student_result, level
    LEVEL:\n- 0:conoce\n- 1:comprende\n- 2:aplica en un nivel intermedio\n- 3:logra el resultado del estudiante\n- 4:no se desarrolla
    '''
    queryset = Course.objects.all()
    serializer_class = Course_StudentResult_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_resource']

    def get_permissions(self,):
        if self.action in ('create','destroy'): return Permission.Unauthorized()
        return Permission.Authenticated()

class Course_Competence_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Curso por competencia
    ENTRADA: id_course, id_competence
    SALIDA: id, id_course, id_competence
    '''
    queryset = Course_Competence.objects.all()
    serializer_class = Course_Competence_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_course']

# PORTAFOLIO ──────────────────────────────────────────────────

class Portfolio_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de portafolio
    ENTRADA: id_resource, semester, year, start_date,\
    end_phase1, end_phase2, end_phase3, end_phase4, status, courses[], users[], 
    SALIDA: id, id_resource, semester, year, start_date,\
    end_phase1, end_phase2, end_phase3, end_phase4, status, courses[], users[]
    SEMESTER:\n- 0:A\n- 1:B\n- 2:C
    '''
    queryset = Portfolio.objects.all()
    serializer_class =  Portfolio_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_process','id_user','semester','year']

# CURSOS ACTIVOS ──────────────────────────────────────────────────

def permission_deactive(user):
    '''
    Si tiene permisos para desactivar retorna True, de lo contrario lo elimina
    '''
    permission = user.id_role.permission_set.values_list('permission','module')
    for permiso in permission:
        if permiso[0] == Permission.DEACTIVATE[0]:
            return True
    return False

'''
class Collaborators_API(viewsets.ModelViewSet):
    
    API general para todo el CRUD de collaborators
    ENTRADA: id_users[],document
    SALIDA: id, id_users, document
    
    queryset = Collaborators.objects.all()
    serializer_class = Collaborators_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_users','documents']
'''

class CourseActive_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de curso activo
    ENTRADA: id_course, start_date, issues, laboratories, metodology, evaluate,
    silabus_dufa, status, users[], student_results[], competences[]
    SALIDA: id, id_course, start_date, issues, laboratories, metodology, evaluate,
    silabus_dufa, status, users[], student_results[], competences[]
    '''
    queryset = CourseActive.objects.all()
    serializer_class = CourseActive_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_portfolio','id_course','users']



class CourseActive_User_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Responsables de Cursos activos
    ENTRADA: id_course_active, id_user, group_thery, group_laboratory
    SALIDA: id, id_course_active, id_user, group_thery, group_laboratory
    '''
    queryset = CourseActive_User.objects.all()
    serializer_class = CourseActive_User_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_course_active','id_user']

class FacultyVitaeAbet_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de vitae facultad abet
    ENTRADA: id_user, name, professional_title, academic_degree,
    level, education, dina_register, experience, collegue, societies, service,
    awards, conferences, programs, other, idioms, course_dictates
    SALIDA: id, id_user, name, professional_title, academic_degree,
    level, education, dina_register, experience, collegue, societies, service,
    awards, conferences, programs, other, idioms, course_dictates
    '''
    queryset = FacultyVitaeAbet.objects.all()
    serializer_class = FacultyVitaeAbet_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ('id_user',)

class Material_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de materiales del silabo abet
    ENTRADA: id_course_active, title, author, url, year
    SALIDA: id, id_course_ac    tive, title, author, url, year
    '''
    queryset = Material.objects.all()
    serializer_class = Material_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_course_active',]

class CourseActive_Student_API(generics.RetrieveUpdateAPIView):
    '''
    API para cargar los usuarios de curso activo
    ENTRADA: students{}
    SALIDA: id, students{}
    '''
    queryset = CourseActive.objects.all()
    serializer_class = CourseActive_Student_Serializer

class CourseActive_Stage_API(generics.RetrieveUpdateAPIView):
    '''
    API general para todo el CRUD de estapas/fases del curso activo en su portafolio
    '''
    queryset = CourseActive.objects.all()
    serializer_class = CourseActive_Stage_Serializer

class CourseActive_ReviewPlan_API(generics.RetrieveAPIView):
    '''
    API general para los datos de revision de mejora del curso activo en su portafolio
    '''
    queryset = CourseActive.objects.all()
    serializer_class = CourseActive_ReviewPlan_Serializer

class CourseActive_SilabusAbet_API(generics.RetrieveUpdateAPIView):
    '''
    API general para los datos del silabo abet del curso activo en su portafolio
    ENTRADA:description, issues, laboratories, methodology, evaluate,\
    student_results[{id_course_student_result, description}], competences[{id_course_competence, description}]
    SALIDA:description, issues, laboratories, methodology, evaluate,\
    student_results[{id_course_student_result, description}], competences[{id_course_competence, description}]
    '''
    queryset = CourseActive.objects.all()
    serializer_class = CourseActive_SilabusAbet_Serializer

# PERIODO ACADEMICO ──────────────────────────────────────────────────

class PeriodAcademic_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Periodos Académicos
    ENTRADA: id_course_active, period, modality, pclass
    SALIDA: id, id_course_active, period, modality, pclass
    PERIOD:\n- 1:primer periodo\n- 2:segundo periodo\n- 3:tercer periodo\n- 4:periodo final
    MODALITY:\n- 1:examen\n- 2:continua\n- 3:general
    NOTe:\n- 1:enunciado\n- 2:mejor nota\n- 3:minima nota aprobatoria\n- 4:solucion
    PCLASS:\n- 1:evidencia\n- 2:reporte
    '''
    queryset = PeriodAcademic.objects.all()
    serializer_class = PeriodAcademic_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_course_active']

class PeriodAcademic_Activity_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Actividades de Periodo Académico
    ENTRADA: id_period_academic, title, notice, numberAct
    SALIDA: id, id_period_academic, title, notice
    NOTICE:\n- 1:enunciado\n- 2:mejor nota\n- 3:minima nota aprobatoria
    '''
    queryset = PeriodAcademic_Activity.objects.all()
    serializer_class = PeriodAcademic_Activity_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_period_academic']

class PeriodAcademic_StudentResult_API(generics.RetrieveUpdateAPIView):
    '''
    API general para todo el CRUD de Cursos activos por Resultados de estudiante
    ENTRADA: id_course_active, id_course_student_result, method, result
    SALIDA: id, id_course_active, id_course_student_result, method, result
    METHOD:\n- 0:rubrica\n- 1:lista de cotejos\n- 2:otros
    '''
    queryset = PeriodAcademic.objects.all()
    serializer_class = PeriodAcademic_StudentResult_Serializer

class PeriodAcademic_Competence_API(generics.RetrieveUpdateAPIView):
    '''
    API general para todo el CRUD de Cursos activos por Resultados de estudiante
    ENTRADA: id_course_active, id_course_student_result, method, result
    SALIDA: id, id_course_active, id_course_student_result, method, result
    METHOD:\n- 0:rubrica\n- 1:lista de cotejos\n- 2:otros
    '''
    queryset = PeriodAcademic.objects.all()
    serializer_class = PeriodAcademic_Competence_Serializer

class EntranceExamination_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Examen de ENTRADA
    ENTRADA: id_course_active, level, doc_exam, doc_exam_solution
    SALIDA: id, id_course_active, level, doc_exam, doc_exam_solution
    '''
    queryset = EntranceExamination.objects.all()
    serializer_class = EntranceExamination_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_course_active',]

class CourseActive_Student_API(generics.RetrieveUpdateAPIView):
    '''
    API para cargar los usuarios de curso activo
    ENTRADA: students{}
    SALIDA: id, students{}
    '''
    queryset = CourseActive.objects.all()
    serializer_class = CourseActive_Student_Serializer

class CourseActive_Observations_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Examen de ENTRADA
    ENTRADA: id_course_active, level, doc_exam, doc_exam_solution
    SALIDA: id, id_course_active, level, doc_exam, doc_exam_solution
    '''
    queryset = Observation.objects.all()    
    serializer_class = CourseActive_Observation_Serializer   
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_course_active',] 


class Evidence_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Evidencias
    ENTRADA: id_period_academic, description
    SALIDA: id, id_period_academic, description
    '''
    queryset = Evidence.objects.all()
    serializer_class = Evidence_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_period_academic',]

class EvidenceDocument_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del documento de evidencias
    ENTRADA: id_period_academic, doc_annexed
    SALIDA: id, id_period_academic, doc_annexed
    '''
    queryset = EvidenceDocument.objects.all()
    serializer_class = EvidenceDocument_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_period_academic',]

class Evaluation_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Evaluacion
    ENTRADA: id_evidence, id_criteria, aspect, criteria, level1, level2, level3, level4
    SALIDA: id, id_evidence, id_criteria, aspect, criteria, level1, level2, level3, level4
    '''
    queryset = Evaluation.objects.all()
    serializer_class = Evaluation_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_evidence','id_criteria']

# DESARROLLO ACADEMICO ──────────────────────────────────────────────────

class AcademicDevelopment_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD del desarrollo academico
    ENTRADA: id_course_active, activity, theme,
    SALIDA: id, id_course_active, activity, theme,
    '''
    queryset = AcademicDevelopment.objects.all()
    serializer_class = AcademicDevelopment_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_course_active','id_course_active__users']

class Student_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Estudiante
    ENTRADA:  id_course_active, cui, full_name, grades_entrance[], grades_criteria{}, grades_period{}
    SALIDA: id,  id_course_active, cui, full_name, grades_entrance[], grades_criteria{}, grades_period{}
    '''
    queryset = Student.objects.all()
    serializer_class = Student_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_course_active',]

class Incidence_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Incidencias
    ENTRADA: id_course_active, id_student, incidence, solution, quantity, date, evidence
    SALIDA: id, id_course_active, id_student, incidence, solution, quantity, date, evidence, semester
    '''
    queryset = Incidence.objects.all()
    serializer_class = Incidence_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_student','id_student__id_course_active']

class ContinuousImprovement_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de la Mejora Continua
    ENTRADA: id_course_active, deficiency, improvement, result
    SALIDA: id, id_course_active, deficiency, improvement, result
    '''
    queryset = ContinuousImprovement.objects.all()
    serializer_class = ContinuousImprovement_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_course_active', 'id_course_active__id_course']

class ProposalImprovement_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de la propuesta de la Mejora Continua
    ENTRADA: id_course_active, scope, proposal, expected_results
    SALIDA: id, id_course_active, scope, proposal, expected_results
    '''
    queryset = ProposalImprovement.objects.all()
    serializer_class = ProposalImprovement_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_course_active']