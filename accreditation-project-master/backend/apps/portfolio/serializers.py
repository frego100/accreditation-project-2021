from apps.portfolio.models import *
from apps.user.models import *
from apps.user.models import *
from apps.user.models import Permission
from django import forms
from rest_framework import fields, serializers
from accreditation.utils import *

# GENERAL ──────────────────────────────────────────────────

class FormationArea_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de áreas de formación'''
    class Meta:
        model  = FormationArea
        exclude = ['created', 'updated']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        remove_fields(self.context['request'].method=='POST',self.fields,'status')

class Subarea_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de las subareas'''
    class Meta:
        model  = Subarea
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        remove_fields(self.context['request'].method=='POST',self.fields,'status')

class Department_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de departamento de un curso de malla'''
    class Meta:
        model  = Department
        exclude = ['created', 'updated']

class Template_Serializer(serializers.ModelSerializer):
    '''Serializador para Plantillas de Recursos'''
    filename = serializers.CharField(source='get_filename',read_only=True)

    class Meta:
        model  = Template
        fields = ['filename','template']

# RECURSOS ──────────────────────────────────────────────────

class Resource_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de recursos'''
    process = serializers.CharField(read_only=True, source='id_process.name')

    class Meta:
        model  = Resource
        fields = ['id','id_process','process','name','description','percent_due','status']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        remove_fields(self.context['request'].method=='POST',self.fields,'status')

# RESULTADOS DEL ESTUDIANTE ──────────────────────────────────────────────────

class StudentResult_Serializer(serializers.ModelSerializer):
    '''API general para todo el CRUD de resultados del estudiante'''
    class Meta:
        model  = StudentResult
        exclude = ['created','updated']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        remove_fields(self.context['request'].method=='POST',self.fields,'status')

class Criteria_Serializer(serializers.ModelSerializer):
    '''API general para todo el CRUD de criterios de los resultados de estudiante'''
    level_suggested = serializers.MultipleChoiceField(choices=Criteria.LEVELS)

    class Meta:
        model  = Criteria
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        remove_fields(self.context['request'].method=='POST',self.fields,'status')

# COMPETENCIAS ──────────────────────────────────────────────────

class Competence_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de competencias'''
    class Meta:
        model  = Competence
        exclude = ['created','updated']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        remove_fields(self.context['request'].method=='POST',self.fields,'status')

# CURSOS ──────────────────────────────────────────────────

class Course_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de curso'''
    competences = serializers.PrimaryKeyRelatedField(label='competencias',many=True,queryset=Competence.objects.all(),required=False)

    class Meta:
        model  = Course
        exclude = ['created', 'updated']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        remove_fields(self.context['request'].method=='POST',self.fields,'status')
        self.fields['resource'] = serializers.CharField(source='id_resource.name',read_only=True)

class Course_StudentResult_NestedSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Course_StudentResult
        fields = ['id','id_student_result','level']

class Course_StudentResult_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Curso por Resultado de estudiante'''
    course_student_result = Course_StudentResult_NestedSerializer(source='course_studentresult_set',many=True,required=False,label='resultados de estudiante')
    # course_student_result = Course_StudentResult_NestedSerializer()

    class Meta:
        model  = Course
        fields = ['id','course_student_result']

    def update(self, instance, validated_data):
        StudentResults = validated_data.pop('course_studentresult_set', None)
        students_old = instance.course_studentresult_set.all()
        students_new = []
        if StudentResults==None: return instance
        for p in StudentResults:
            st = Course_StudentResult.objects.filter(id_course=instance,id_student_result=p['id_student_result'])
            if st.exists():
                stud = st[0]
                stud.level=p['level']
                stud.save()
                s = stud
            else: s = Course_StudentResult.objects.create(id_course=instance,id_student_result=p['id_student_result'],level=p['level'])
            students_new.append(s)
        for p in students_old: p.delete() if not p in students_new else False
        return instance

class Course_Competence_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Curso por Competencia'''

    class Meta:
        model  = Course_Competence
        fields = '__all__'

# PORTAFOLIO ──────────────────────────────────────────────────

class Portfolio_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de portafolio'''
    courses = serializers.PrimaryKeyRelatedField(many=True,queryset=Course.objects.all(),required=False,label='cursos')

    class Meta:
        model  = Portfolio
        # fields = ['id_resource','semester','year','start_date','end_phase1','end_phase2','end_phase3','end_phase4','users','courses_actives','collaborators','collaborator_anedufa]
        exclude = ['created', 'updated']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # si el método invocado es POST se deshabilitan algunos campos
        method = self.context['request'].method
        remove_fields(method=='POST', self.fields, 'status')
        # se verifica si el usuario logueado es super administrador
        self.user = self.context["request"].user
        if self.user.superadmin: return
        # se valida el proceso del usuario logueado
        remove_fields(method in ('POST','PUT'), self.fields, 'id_process')

    def create(self, validated_data):
        '''Función para crear un registro del modelo'''
        # si el usuario logueado no es super administrador se define el proceso al que pertenece
        if not self.user.superadmin: validated_data['id_process'] = self.user.id_role.id_process
        return super().create(validated_data)
        

# CURSOS ACTIVOS ──────────────────────────────────────────────────

class CourseActive_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de curso activo'''
    portfolio = serializers.CharField(source='id_portfolio.semester_complete',read_only=True)
    silabus_dufa_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = CourseActive
        exclude = ['created', 'updated']
        # fields = ['id','id_portfolio','id_course','silabus_dufa','status','users']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        method = self.context['request'].method
        self.fields['course'] = serializers.CharField(source='id_course.name',read_only=True)

        remove_fields(method=='POST',self.fields,'status','silabus_dufa','competences')





class Stage_NestedSerializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de estapas de curso activo'''
    class Meta:
        model  = Stage
        fields = ['stage','observation','status']

def enviar_correo(obj,instance,msj,msj2):
    # user = obj.context["request"].user
    email_title = "Estado del las etapas del su Curso Activo en el Sistema de Acreditación"
    email_body = "Hola Profesor.<br>\
                  "+msj+" en su portafolio en el Sistema de Acreditación de la UNSA:<br>\
                  <strong>" + msj2 + "</strong>"
    users = instance.users
    if not users.exists(): print('este curso no tiene responsables')
    for u in users.all():
        # print('se envio e responsables')
        User.send_email(email_title,email_body,u.email)

class CourseActive_Stage_Serializer(serializers.ModelSerializer):
    stages = Stage_NestedSerializer(source='stage_set',many=True,required=False,label='estados')

    class Meta:
        model  = CourseActive
        fields = ['stages']

    def update(self, instance, validated_data):
        stage_set = validated_data.pop('stage_set', None)
        if stage_set==None: return instance
        for p in stage_set:
            changed = False
            msj=''
            msj2=''
            stage = Stage.objects.get(id_course_active=instance,stage=p['stage'])
            if stage.observation!=p['observation']:
                stage.observation=p['observation']
                changed = True
                msj = 'tiene la siguiente observación en la etapa ' + str(stage.stage)
                msj2 = stage.observation
            if stage.status!=p['status']:
                stage.status=p['status']
                changed = True
                msj = 'se actualizo el estado en la etapa ' + str(stage.stage)
                msj2 = 'estado: ' + ('APROBADO' if stage.status else 'DESAPROBADO')
            stage.save()
            if changed: enviar_correo(self,instance,msj,msj2)
        return instance

class Student_NestedSerializer(serializers.ModelSerializer):
    '''Serializador para cargar varios Estudiantes'''
    class Meta:
        model  = Student
        fields = ['cui','full_name']

class CourseActive_Student_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de curso activo'''
    students = Student_NestedSerializer(label='estudiantes',source='student_set',many=True,required=False)
    #serializador de los resultados del estudiante para el curso activo

    class Meta:
        model  = CourseActive
        fields = ['students']

    def update(self, instance, validated_data):
        students = validated_data.pop('student_set', None)
        course_active = super().update(instance, validated_data)
        students_old = course_active.student_set.all()
        students_new = []
        if students==None: return course_active
        for p in students:
            st = Student.objects.filter(id_course_active=course_active,cui=p['cui'])
            if st.exists():
                stud = st[0]
                stud.full_name=p['full_name']
                stud.save()
                s = stud
            else: s = Student.objects.create(id_course_active=course_active,cui=p['cui'],full_name=p['full_name'])
            students_new.append(s)
        for p in students_old: p.delete() if not p in students_new else False
        return course_active

class Evaluation_NestedSerializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Evaluacion'''
    criteria = serializers.CharField(source='get_criteria_name',read_only=True)
    student_result = serializers.CharField(source='get_studentresult_name',read_only=True)
    method = serializers.CharField(source='get_studentresult_method',read_only=True)
    # id = serializers.IntegerField(label='ID', read_only=False)

    class Meta:
        model  = Evaluation
        fields = ['id','student_result','criteria','method','review_week']
        # read_only_fields = ('grade',)

class CourseActive_ReviewPlan_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de curso activo'''
    percent_due = serializers.IntegerField(source='id_course.id_resource.percent_due',read_only=True)
    evaluations = Evaluation_NestedSerializer(source='evaluation_set',many=True,required=False,label='evalaciones de criterios')

    class Meta:
        model  = CourseActive
        fields = ['percent_due','evaluations']

class CourseActive_User_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Responsables de Cursos activos'''
    user = serializers.CharField(source='id_user',read_only=True)

    class Meta:
        model  = CourseActive_User
        fields = '__all__'

class FacultyVitaeAbet_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de vitae facultad'''
    class Meta:
        model  = FacultyVitaeAbet
        fields = '__all__'

class Material_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del material del curso activo'''
    class Meta:
        model  = Material
        fields = '__all__'

class CourseActive_StudentResult_Descriptions_NestedSerializer(serializers.ModelSerializer):
    class Meta:
        model  = CourseActive_StudentResult_Descriptions
        fields = ['id_course_student_result','description']

class CourseActive_Competence_Descriptions_NestedSerializer(serializers.ModelSerializer):
    class Meta:
        model  = CourseActive_Competence_Descriptions
        fields = ['id_course_competence','description']

class CourseActive_SilabusAbet_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de curso activo'''
    student_results = CourseActive_StudentResult_Descriptions_NestedSerializer(label='estudiantes',source='courseactive_studentresult_descriptions_set',many=True,required=False)
    competences = CourseActive_Competence_Descriptions_NestedSerializer(label='estudiantes',source='courseactive_competence_descriptions_set',many=True,required=False)
    required = serializers.BooleanField(source='id_course.get_required',read_only=True)
    #serializador de los resultados del estudiante para el curso activo

    class Meta:
        model  = CourseActive
        fields = ['description','issues','laboratories','methodology','evaluate','required','student_results','competences']

    def get_or_create_student_results(self, instance, **fields): return CourseActive_StudentResult_Descriptions.objects.get_or_create(id_course_active=instance,**fields)[0]
    def get_or_create_competences(self, instance, **fields): return CourseActive_Competence_Descriptions.objects.get_or_create(id_course_active=instance,**fields)[0]
    def update(self, instance, validated_data):
        '''Función para actualizar un registro del modelo'''
        return update_nested2(self, instance, validated_data, True,
            ('courseactive_studentresult_descriptions_set',self.get_or_create_student_results,'description'),
            ('courseactive_competence_descriptions_set',self.get_or_create_competences,'description'))

# PERIODO ACADEMICO ──────────────────────────────────────────────────

class PeriodAcademic_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Rubricas'''
    class Meta:
        model  = PeriodAcademic
        exclude = ['student_results','competences']

class PeriodAcademic_Activity_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de actividades de un periodo academico'''
    #doc_annexed_filename = serializers.CharField(read_only=True)
    class Meta:
        model = PeriodAcademic_Activity
        fields =['id', 'id_period_academic','numero']



class PeriodAcademic_StudentResult_NestedSerializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Cursos activos por Resultados de estudiante'''
    class Meta:
        model  = PeriodAcademic_StudentResult
        fields = ['id','id_course_student_result','description','method','result']

class PeriodAcademic_StudentResult_Serializer(serializers.ModelSerializer):
    period_academic_student_results = PeriodAcademic_StudentResult_NestedSerializer(source='periodacademic_studentresult_set',many=True,required=False,label='resultados de estudiante')

    class Meta:
        model  = PeriodAcademic
        fields = ['period_academic_student_results']

    def update(self, instance, validated_data):
        StudentResults = validated_data.pop('periodacademic_studentresult_set', None)
        students_old = instance.periodacademic_studentresult_set.all()
        students_new = []
        if StudentResults==None: return instance
        for p in StudentResults:
            st = PeriodAcademic_StudentResult.objects.filter(id_period_academic=instance,id_course_student_result=p['id_course_student_result'])
            if st.exists():
                stud = st[0]
                stud.description=p['description']
                stud.method=p['method']
                stud.result=p['result']
                stud.save()
                s = stud
            else:
                s = PeriodAcademic_StudentResult.objects.create(
                    id_period_academic=instance,
                    id_course_student_result=p['id_course_student_result'],
                    description=p['description'],
                    method=p['method'],
                    result=p['result'],
                )
            students_new.append(s)
        for p in students_old: p.delete() if not p in students_new else False
        return instance

class PeriodAcademic_Competence_NestedSerializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Cursos activos por Resultados de estudiante'''
    class Meta:
        model  = PeriodAcademic_Competence
        fields = ['id','id_course_competence','description']

class PeriodAcademic_Competence_Serializer(serializers.ModelSerializer):
    period_academic_competences = PeriodAcademic_Competence_NestedSerializer(source='periodacademic_competence_set',many=True,required=False,label='competencias')

    class Meta:
        model  = PeriodAcademic
        fields = ['period_academic_competences']

    def update(self, instance, validated_data):
        StudentResults = validated_data.pop('periodacademic_competence_set', None)
        students_old = instance.periodacademic_competence_set.all()
        students_new = []
        if StudentResults==None: return instance
        for p in StudentResults:
            st = PeriodAcademic_Competence.objects.filter(id_period_academic=instance,id_course_competence=p['id_course_competence'])
            if st.exists():
                stud = st[0]
                stud.description=p['description']
                stud.save()
                s = stud
            else:
                s = PeriodAcademic_Competence.objects.create(
                    id_period_academic=instance,
                    id_course_competence=p['id_course_competence'],
                    description=p['description'],
                )
            students_new.append(s)
        for p in students_old: p.delete() if not p in students_new else False
        return instance

class EntranceExamination_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Examen de ENTRADA'''
    doc_exam_filename = serializers.CharField(read_only=True)
    doc_exam_solution_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = EntranceExamination
        fields = '__all__'

class CourseActive_Observation_Serializer(serializers.ModelSerializer):
    
    class Meta:
        model  = Observation
        fields = '__all__'

class Evidence_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Evidencias'''
    class Meta:
        model  = Evidence
        fields = '__all__'

class EvidenceDocument_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD del documento de evidencias'''
    doc_annexed_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = EvidenceDocument
        fields = '__all__'


class Evaluation_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Evaluacion'''
    class Meta:
        model  = Evaluation
        fields = '__all__'

# DESARROLLO ACADEMICO ──────────────────────────────────────────────────

class AcademicDevelopment_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de la propuesta de la Mejora Continua'''
    class Meta:
        model  = AcademicDevelopment
        fields = '__all__'

class GradeCriteria_NestedSerializer(serializers.ModelSerializer):
    # id = serializers.IntegerField(label='ID', read_only=False)
    id_period_academic = serializers.IntegerField(source='id_evaluation.id_evidence.id_period_academic.id',read_only=True)
    criteria = serializers.CharField(source='id_evaluation.get_criteria_name',read_only=True)
    student_result = serializers.CharField(source='id_evaluation.get_studentresult_name',read_only=True)

    class Meta:
        model  = GradeCriteria
        fields = ['id_period_academic','id_evaluation','criteria','student_result','grade']

class GradePeriod_NestedSerializer(serializers.ModelSerializer):
    class Meta:
        model  = GradePeriod
        fields = ['id_period_academic','grade']

class Student_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Estudiantes'''
    grades_evaluation = GradeCriteria_NestedSerializer(source='gradecriteria_set',many=True,required=False)
    #serializador de las calificaciones por criterio 
    grades_period = GradePeriod_NestedSerializer(source='gradeperiod_set',many=True,required=False)
    #serializador de las calificaciones periodos academicos 

    class Meta:
        model  = Student
        fields = ['id','id_course_active','cui','full_name','status','grades_entrance','grades_evaluation','grades_period']

    def get_or_create_grades_crierias(self, instance, **fields): return GradeCriteria.objects.get_or_create(id_student=instance,**fields)[0]
    def get_or_create_grades_period(self, instance, **fields): return GradePeriod.objects.get_or_create(id_student=instance,**fields)[0]
    '''Función para crear y/o obtener permisos para el rol'''
    def create(self, validated_data):
        '''Función para crear un registro del modelo'''
        return create_nested2(self, validated_data,
            ('gradecriteria_set',self.get_or_create_grades_crierias),
            ('gradeperiod_set',self.get_or_create_grades_period))
    def update(self, instance, validated_data):
        '''Función para actualizar un registro del modelo'''
        return update_nested2(self, instance, validated_data, True,
            ('gradecriteria_set',self.get_or_create_grades_crierias,'grade'),
            ('gradeperiod_set',self.get_or_create_grades_period,'grade'))

class Incidence_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Incidencias'''
    evidence_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = Incidence
        fields = '__all__'

class ContinuousImprovement_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Mejora Continua'''
    semester = serializers.CharField(source='id_course_active.id_portfolio.semester_complete',read_only=True)

    class Meta:
        model  = ContinuousImprovement
        fields = ['id','id_course_active','deficiency','improvement','result','semester']

class ProposalImprovement_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de la propuesta de la Mejora Continua'''
    class Meta:
        model  = ProposalImprovement
        fields = '__all__'

