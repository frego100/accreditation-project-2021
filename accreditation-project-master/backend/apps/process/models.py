from apps.user.models import *
from django.core.validators import RegexValidator,  MinValueValidator, MaxValueValidator
from django.db import models
from django.contrib.postgres.fields import ArrayField

P_P_CRITERIAS = ('criterio 1','criterio 2','criterio 3','criterio 4','criterio 5','criterio 6','criterio 7','criterio 8',)

class Process(models.Model):
    '''Modelo de los Procesos en la base de datos'''
    id_owner = models.ForeignKey(User,on_delete=models.PROTECT,null=True,blank=True,related_name='process_created',verbose_name='creado por')
    # id del usuario creador del proceso
    id_user = models.ForeignKey(User,on_delete=models.PROTECT,null=True,blank=True,related_name='process_managed',verbose_name='responsable')
    # id del usuario responsable del proceso
    name = models.CharField(unique=True,blank=False,max_length=32,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ\s]{2,30}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ] y espacios, de 2 a 30 caracteres'
    )]) # nombre del proceso
    description = models.TextField(blank=True,max_length=100,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 100 caracteres'
    )]) # descripción del proceso
    started = models.DateTimeField(null=True,blank=True,verbose_name='fecha de inicio')
    # fecha de inicio del proceso
    closed = models.DateTimeField(null=True,blank=True,verbose_name='fecha de cierre')
    # fecha de cierre del proceso
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del proceso
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación del registro (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización del registro (automático)

    class Meta:
        verbose_name = 'proceso'
        verbose_name_plural = 'procesos'

    def __str__(self): 
        return f'{self.name}'

    def create_data_auto(self):
        for criterias in P_P_CRITERIAS:
            Criteria.objects.get_or_create(id_process=self,name=criterias)
                
class Criteria(models.Model):
    '''Modelo de los Criterios en la base de datos'''
    id_process = models.ForeignKey(Process,on_delete=models.CASCADE,null=True,blank=False,verbose_name='proceso')
    # id del proceso al que pertenece el criterio
    collaborator= ArrayField(models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(20)]),null=False,blank=True,default=list,verbose_name='responsables')    
    name = models.CharField(blank=False,max_length=30,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ\s]{2,30}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ] y espacios, de 2 a 20 caracteres'
    )]) # nombre del criterio
    description = models.TextField(blank=True,max_length=100,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 100 caracteres'
    )]) # descripción del criterio
    status = models.BooleanField(default=True,verbose_name='estado')
    # estado del criterio
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación del registro (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización del registro (automático)

    class Meta:
        verbose_name = 'criterio'
        verbose_name_plural = 'criterios'
        #unique_together = ['id_process']

    def __str__(self): return f'{self.name} | {self.id_process}'


from django.dispatch import receiver

@receiver(models.signals.post_save, sender=Process)
def create_data_auto(sender, instance, created, **kwargs):
    if created: instance.create_data_auto()
