from apps.process.models import *
from apps.process.serializers import *
from apps.user.models import *
from rest_framework import viewsets
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import Q, query

# PROCESOS ──────────────────────────────────────────────────

class ProcessAPI(viewsets.ModelViewSet):
    '''
    API general para el CRUD de procesos
    Entrada: id_owner, id_user, name, description, started, closed, status
    Salida: id, id_owner, id_user, name, description, started, closed, status
    '''
    queryset = Process.objects.all()
    serializer_class = ProcessSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_owner','id_user']


# CRITERIOS ──────────────────────────────────────────────────

class CriteriaAPI(viewsets.ModelViewSet):
    '''
    API general para el CRUD de criterios
    Entrada: id_phase, id_user, name, description, status
    Salida: id, id_phase, id_user, name, description, status
    '''
    queryset = Criteria.objects.all()
    serializer_class =  CriteriaSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_process']
