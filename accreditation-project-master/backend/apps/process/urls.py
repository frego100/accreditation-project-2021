from django.urls import path, include
from rest_framework import routers
from apps.process.api import *

router = routers.SimpleRouter()
router.register('process', ProcessAPI)
router.register('criteria', CriteriaAPI)

urlpatterns = router.urls