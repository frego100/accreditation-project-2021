from apps.process.models import *
from rest_framework import serializers
from accreditation.utils import *

# PROCESOS ──────────────────────────────────────────────────

class ProcessSerializer(serializers.ModelSerializer):
    '''
    Serializador general de los datos de Procesos
    Entrada: id_owner, id_user, name, description, started, closed, status
    Salida: id, id_owner, id_user, name, description, started, closed, status
    '''
    class Meta:
        model = Process
        exclude = ['created','updated']
        # read_only_fields = ['id_owner',]

    def __init__(self, *args, **kwargs):
        super(ProcessSerializer, self).__init__(*args, **kwargs)
        method = self.context['request'].method
        remove_fields(method=='POST',self.fields,'id_user','status')
        remove_fields(method in ('POST','PUT','PATCH'),self.fields,'id','id_owner')
        self.user = self.context["request"].user

    def create(self, validated_data):
        '''Función para crear un registro del modelo'''
        # se asigna automaticamente al usuario logeado como creador y responsable del proceso
        validated_data['id_owner'] = self.user
        validated_data['id_user'] = self.user
        return super().create(validated_data)


# CRITERIOS ──────────────────────────────────────────────────

class CriteriaSerializer(serializers.ModelSerializer):
    '''
    Serializador general para el Criterio
    Entrada: id_phase, id_user, name, description, status
    Salida: id, id_phase, id_user, name, description, status
    '''
    class Meta:
        model  = Criteria
        exclude = ['created','updated']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        method = self.context['request'].method
        remove_fields(method=='POST', self.fields, 'status')
        # se verifica si el usuario logueado es super administrador
        self.user = self.context["request"].user
        if self.user.superadmin: return
        # se valida el proceso del usuario logueado
        remove_fields(method in ('POST','PUT'), self.fields, 'id_process')
    
    def create(self, validated_data):
        if not self.user.superadmin: validated_data['id_process'] = self.user.id_role.id_process
        return super().create(validated_data)

# CRITERIOS ──────────────────────────────────────────────────

class CriteriaSerializer(serializers.ModelSerializer):
    '''
    Serializador general para el Criterio
    Entrada: id_phase, id_user, name, description, status
    Salida: id, id_phase, id_user, name, description, status
    '''
    class Meta:
        model  = Criteria
        exclude = ['created','updated']

    def __init__(self, *args, **kwargs):
        super(CriteriaSerializer, self).__init__(*args, **kwargs)
        if self.context['request'].method == 'POST':
            self.fields.pop('status', None)
