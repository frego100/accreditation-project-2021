from apps import user
from accreditation import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.mail.message import EmailMultiAlternatives
from django.core.validators import RegexValidator, FileExtensionValidator, MaxValueValidator
from django.db import models
from rest_framework.permissions import BasePermission, IsAuthenticated
import os

def user_path(user): return f'profiles/{user.id}/{user.email.split("@")[0]}'
'''Función para obtener la dirección donde se almacena los archivos de un usuario'''

def photo_path(instance, filename): return f'{user_path(instance)}.{filename.split(".")[-1]}'
'''Función para obtener la dirección donde se almacena la imagen de perfil de usuario'''

def signature_path(instance, filename):return f'{user_path(instance)}_signature.{filename.split(".")[-1]}'
'''Función para obtener la dirección donde se almacena la firma digital de usuario'''

# ROLES ──────────────────────────────────────────────────

class Role(models.Model):
    '''Modelo de roles de procesos en la base de datos'''
    ADMINTRATOR    = 'administrador'
    ADMINISTRATIVE = 'administrativo'
    PROCESS_MANAGER = 'encargado de proceso'
    PORTFOLIO_MANAGER = 'responsable de portafolio'
    COLLABORATOR = 'colaborador'
    ACTIVE_COURSE_MANAGER = 'responsable de curso activo'
    TEACHER        = 'profesor'
    ROLES = (ADMINTRATOR,PROCESS_MANAGER,ADMINISTRATIVE,PORTFOLIO_MANAGER,COLLABORATOR,ACTIVE_COURSE_MANAGER,TEACHER)
    #ROLES = (ADMINTRATOR,ADMINISTRATIVE,TEACHER)

    id_process = models.ForeignKey("process.Process",null=True,blank=False,on_delete=models.CASCADE,verbose_name='proceso')
    # id del proceso al que pertenece el rol
    name = models.CharField(blank=False,max_length=32,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ\s]{2,32}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ] y espacios, de 2 a 32 caracteres'
    )]) # nombre del role
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del rol
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación de los datos (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización de los datos (automático)

    class Meta:
        verbose_name = 'rol'
        verbose_name_plural = 'roles'
        unique_together = ['id_process', 'name']

    def __str__(self): return f'{self.name} | {self.id_process}'

class Permission(models.Model):
    '''Modelo de Permisos en la base de datos para la gestión de módulos'''
    CREATE      = (0,'crear')
    READ        = (1,'visualizar')
    UPDATE      = (2,'editar')
    DELETE      = (3,'eliminar')
    DEACTIVATE  = (4,'desactivar')
    PERMISSIONS = (CREATE,READ,UPDATE,DELETE,DEACTIVATE)

    MODULE_ROLE      = (0,'roles')
    MODULE_USER      = (1,'usuarios')
    MODULE_PHASE     = (2,'fases')
    MODULE_PORTFOLIO = (3,'portafolio')
    MODULES = (MODULE_ROLE,MODULE_USER,MODULE_PHASE,MODULE_PORTFOLIO)

    id_role = models.ForeignKey(Role,on_delete=models.CASCADE,null=True,blank=False,verbose_name='role')
    # id del rol que tiene el permiso
    permission = models.PositiveSmallIntegerField(choices=PERMISSIONS,blank=False,verbose_name='permiso',
        validators = [MaxValueValidator(4)]
    ) # tipo de permiso
    module = models.PositiveSmallIntegerField(choices=MODULES,blank=False,verbose_name='modulo',
        validators = [MaxValueValidator(3)]
    ) # modulo que tiene el permiso

    class Meta:
        verbose_name = 'permiso'
        verbose_name_plural = 'permisos'
        unique_together = ['id_role','permission','module']

    class IsUnauthorized(BasePermission):
        '''Clase que define un permiso sin ningún acceso'''
        message = 'El usuario no tiene permitido realizar esta acción.'
        def has_permission(self, request, view): return False

    def __str__(self): return f'{self.get_permission_display()} {self.get_module_display()} | {self.id_role}'

    def get_permission(obj, module=None, query_process=None, query_user=None):
        '''Función para obtener el permiso en cada una de las solicitudes'''
        user = obj.request.user
        permission = Permission.get_permission_base(obj,user)
        if permission: return permission
        # se verifica los permisos del usuario logueado por cada solicitud
        if obj.action == 'create':
            return Permission.get_permission_by_action(obj,user,Permission.CREATE[0],module,query_process,query_user)
        elif obj.action in ('list', 'retrieve'):
            return Permission.get_permission_by_action(obj,user,Permission.READ[0],module,query_process,query_user)
        elif obj.action in ('update', 'partial_update'):
            return Permission.get_permission_by_action(obj,user,Permission.UPDATE[0],module,query_process,query_user)
        elif obj.action == 'destroy':
            return Permission.get_permission_by_action(obj,user,Permission.DELETE[0],module,query_process,query_user)
        return Permission.Authenticated()

    def get_permission_by_action(obj,user,permission,module,query_process,query_user):
        '''Función para obtener el permiso por cada solicitud'''
        process = user.id_role.id_process
        permissions = user.id_role.permission_set.values_list('permission','module')
        if (permission, module) in permissions:
            if query_process!=None: obj.queryset = query_process(process)
            return Permission.Authenticated()
        else:
            if query_user!=None: obj.queryset = query_user(user)
            if not obj.queryset.exists() and query_user!=None: obj.queryset = query_user(user)
            if not obj.queryset.exists(): return Permission.Unauthorized()
            else: return Permission.Authenticated()

    def get_permission_base(obj,user):
        '''Función para obtener el permiso básicos del usuario'''
        # se verifica si el usuario es anónimo
        if user.is_anonymous: return Permission.Authenticated()
        # se verifica si el usuario es superadministrador
        if user.superadmin:
            obj.queryset = obj.queryset.model.objects.all()
            return Permission.Authenticated()
        # se verifica si el usuario tiene un rol asignado
        if not user.id_role: return Permission.Unauthorized()
        # si los permisos del usuario aún no se determinan
        return None

    def Unauthorized(): return [permission() for permission in [Permission.IsUnauthorized]]
    '''Funciona para obtener los permisos de inaccesibilidad'''
    def Authenticated(): return [permission() for permission in [IsAuthenticated]]
    '''Función para obtener los permisos de autenticación'''

# USUARIOS ──────────────────────────────────────────────────

class UserManager(BaseUserManager):
    '''Interfaz que proporcionan las operaciones de consulta de la base de datos para Usuarios'''
    def create_user(self, email, first_name, last_name, password):
        '''función para crear usuarios'''
        if not email: raise ValueError('falta el correo electrónico')
        user = self.model(email=self.normalize_email(email),first_name=first_name,last_name=last_name)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, first_name, last_name, password):
        '''función para crear un super usuario (comando: createsuperuser)'''
        user = self.create_user(email=email,first_name=first_name,last_name=last_name,password=password)
        user.superadmin = True
        user.save()
        return user

class User(AbstractBaseUser):
    '''Modelo de Usuarios en la base de datos'''
    id_role = models.ForeignKey(Role,on_delete=models.SET_NULL,null=True,blank=True,verbose_name='rol')
    # id del role del usuario
    email = models.EmailField(blank=False,unique=True,max_length=32,verbose_name='correo electrónico',
        validators = [RegexValidator(regex=r'^([a-z]{3,20}@unsa\.edu\.pe)$',
            message = 'Solo se admite la siguiente validación: [a-z]@unsa.edu.pe, de 15 a 32 caracteres'
    )]) # correo institucional único por usuario
    first_name = models.CharField(blank=False,max_length=32,verbose_name='nombres',
        validators = [RegexValidator(regex=r'^[a-záéíóúñ\s]{2,32}$',
            message = 'Solo se admite la siguiente validación: [a-z][á-úñ] y espacios, de 2 a 32 caracteres'
    )]) # nombres completos
    last_name = models.CharField(blank=False,max_length=32,verbose_name='apellidos',
        validators = [RegexValidator(regex=r'^[a-záéíóúñ\s]{2,32}$',
            message = 'Solo se admite la siguiente validación: [a-z][á-úñ] y espacios, de 2 a 32 caracteres'
    )]) # apellidos completos
    photo = models.ImageField(upload_to=photo_path,null=True,blank=True,verbose_name='foto de perfil',
        validators = [FileExtensionValidator(allowed_extensions=['jpg','jpeg','png'],
            message = 'Solo se aceptan lo siguientes formatos: jpg,jpeg,png'
    )]) # foto de perfil del usuario
    signature = models.ImageField(upload_to=signature_path,null=True,blank=True,verbose_name='firma digital',
        validators = [FileExtensionValidator(allowed_extensions=['jpg','jpeg','png'],
            message = 'Solo se aceptan lo siguientes formatos: jpg,jpeg,png'
    )]) # firma digital del usuario
    superadmin = models.BooleanField(blank=True,default=False,verbose_name='super administrador')
    # estado de super administrador del usuario
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del usuario
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación de los datos (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización de los datos (automático)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name','last_name']

    class Meta:
        verbose_name = 'usurario'
        verbose_name_plural = 'usuarios'
        ordering = ['first_name']

    def __str__(self): return f'{self.first_name} {self.last_name}'

    def delete(self, *args, **kwargs):
        try:
            dir = f'{self.photo.path[:self.photo.path.find(self.photo.name)]}profiles/{self.id}/'
            self.photo.delete()
            if os.path.isdir(dir): os.rmdir(dir)
        except:
            print('El usuario no tiene directorio de perfil')
        super(User, self).delete(*args, **kwargs)

    def has_perm(self, perm, obj=None): return True

    def has_module_perms(self, app_label): return True

    def send_email(title, body, to_email):
        mail = EmailMultiAlternatives(title,'',settings.EMAIL_HOST_USER,[to_email])
        mail.attach_alternative(body,'text/html')
        mail.send()
        print(f'<<<<< Mensaje enviado a [{to_email}] >>>>>')

    @property
    def is_admin(self): return self.superadmin

    @property
    def is_staff(self): return self.superadmin

    @property
    def is_active(self): return self.status