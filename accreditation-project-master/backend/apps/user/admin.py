from apps.user.models import *
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from django.utils.html import format_html

'''Clases de los modelos de la aplicación de Usuarios en administración'''
class PermissionInline(admin.TabularInline):
    ''''Clase en linea de Permisos en administración'''
    model    = Permission
    ordering = ('module',)
    extra    = 0

class RoleAdmin(admin.ModelAdmin):
    ''''Clase principal de Roles en administración'''
    list_display       = ('id','name','id_process','permissions_count','status')
    list_display_links = ('name',)
    list_filter        = ('status','id_process')
    search_fields      = ('name',)
    readonly_fields    = ('created','updated')
    ordering           = ('id',)
    inlines            = (PermissionInline,)
    list_per_page      = 25
    def permissions_count(self, obj): return obj.permission_set.count()
    permissions_count.short_description = 'numero de permisos'

class UserAdmin(BaseUserAdmin):
    ''''
    Clase principal de Usuarios en administración
    - UserAdmin(admin.ModelAdmin): get_fieldsets (crear/actualizar)
    - UserAdmin(BaseUserAdmin): get_fieldsets (crear) y add_fieldsets (actualizar)
    '''
    list_display        = ('id','colored_email','first_name','last_name','role','process','status')
    list_display_links  = ('colored_email',)
    list_filter         = ('superadmin','status','id_role__id_process')
    search_fields       = ('email',)
    empty_value_display = 'ninguno'
    actions             = ('activate','deactivate')
    readonly_fields     = ('created','updated','last_login','process_created','process_managed','portfolio_managed','portfolio_collabed','course_managed')
    ordering            = ('email',)
    filter_horizontal   = ()
    list_per_page       = 25
    # campos al crear usuarios
    add_fieldsets = (
        (None,{'classes':('wide',),'fields':('email','password1','password2')}),
        ('Información Personal',{'fields':('first_name','last_name')}),
        ('Permisos',{'fields': ('id_role','superadmin','status')}),
    )
    # campos al actualizar usuarios
    fieldsets = (
        (None,{'fields': (
            'email','password',('first_name','last_name'),('photo','signature'),
            'id_role','superadmin','status','created', 'updated','last_login',
        )}),
        ('Módulos en los que se es responsable',{'classes':('collapse',),'fields':(
            'process_created','process_managed','portfolio_managed','portfolio_collabed','course_managed'
        )}),
    )
    def deactivate(self, request, queryset):
        '''Función para desactivar varios usuarios'''
        for user in queryset:
            user.status = False
            user.save(update_fields=['status'])   
    deactivate.short_description = 'Desactivar usuarios seleccionado/s'
    def activate(self, request, queryset):
        '''Función para activar varios usuarios'''
        for user in queryset:
            user.status = True
            user.save(update_fields=['status'])
    activate.short_description = 'Activar usuarios seleccionado/s'
    def colored_email(self,obj):
        '''Función para dar color al correo de los usuario según estado de superadmin'''
        color = '#E6B943' if obj.superadmin else '#6AA0BA'
        return format_html(f'<span style="color: {color};">{obj.email}</span>')
    colored_email.short_description = 'email' 
    def process(self, obj): return obj.id_role.id_process if obj.id_role else None
    process.short_description = 'proceso'
    def role(self, obj): return obj.id_role.name if obj.id_role else None
    role.short_description = 'rol'
    def process_created(self, obj): return f'{obj.process_created.count()} > {[reg.id for reg in obj.process_created.all()]}'
    process_created.short_description = 'numero de procesos a los que se es creador'
    def process_managed(self, obj): return f'{obj.process_managed.count()} > {[reg.id for reg in obj.process_managed.all()]}'
    process_managed.short_description = 'numero de procesos a los que se es responsable'
    def portfolio_managed(self, obj): return f'{obj.portfolio_set.count()} > {[reg.id for reg in obj.portfolio_set.all()]}'
    portfolio_managed.short_description = 'numero de portafolios a los que se es responsable'
    def portfolio_collabed(self, obj): return f'{obj.portfolios.count()} > {[reg.id for reg in obj.portfolios.all()]}'
    portfolio_collabed.short_description = 'numero de portafolios a los que se es colaborador'
    def course_managed(self, obj): return f'{obj.courses_actives.count()} > {[reg.id for reg in obj.courses_actives.all()]}'
    course_managed.short_description = 'numero de cursos activos a los que se es responsable'

# REGISTROS ═══════════════════════════════════════════════════════════════════════════════════════
'''Registro de los modelos de la aplicación de Usuarios'''
admin.site.register(Role, RoleAdmin)
admin.site.register(User, UserAdmin)
admin.site.unregister(Group)