from django.urls import path, include
from rest_framework import routers
from apps.criteria_six.api import *
from apps.process.api import *

router = routers.SimpleRouter()

#  ─────────────────────────────────────────────────────CRITERIO 06 ─────────────────────────────────────────────────────

'''───────────────────────────URLS DE APIS DE MODELOS SECUNDARIOS DE DOCUMENTOS DE CRITERIO 06───────────────────────────'''

router.register('organization', Organization_API)
router.register('degree_area', Degree_Area_API)
router.register('highest_degree', Highest_Degree_API)
router.register('profession', Profession_API)
router.register('research', Research_API)
router.register('internship', Internship_API)
router.register('event', Event_API)
router.register('article', Article_API)
router.register('training_program', Training_Program_API)

'''───────────────────────────URLS DE APIS DE MODELOS PRINCIPALES DE DOCUMENTOS DE CRITERIO 06───────────────────────────'''

router.register('professor_qualification', Professor_Qualification_API)
router.register('professor_researcher', Professor_Researcher_API)
router.register('professor_research', Professor_Research_API)
router.register('professor_workload', Professor_Workload_API)
router.register('professor_survey', Professor_Survey_API)
router.register('professor_recognition', Professor_Recognition_API)
router.register('professor_internship', Professor_Internship_API)
router.register('professor_training', Professor_Training_API)
router.register('professor_participation', Professor_Participation_API)

'''───────────────────────────URLS DE APIS DE MODELOS PARA EL MANEJO DE EVIDENCIAS DE DOCUMENTOS DE CRITERIO 06───────────────────────────'''

router.register('training_doc_evidence', Training_Doc_Evidence_API)
router.register('participation_doc_evidence', Participation_Doc_Evidence_API)
router.register('recognition_doc_evidence', Recognition_Doc_Evidence_API)

urlpatterns = router.urls