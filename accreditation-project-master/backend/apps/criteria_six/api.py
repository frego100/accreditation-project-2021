from django.db.models.base import Model
#from apps.criteria_six.models import *
from apps.criteria_six.serializers import *
from apps.user.models import *
from rest_framework import viewsets
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import Q

#  ─────────────────────────────────────────────────CRITERIO 06 ─────────────────────────────────────────────────
'''───────────────────────────APIS DE MODELOS SECUNDARIOS DE DOCUMENTOS DE CRITERIO 06───────────────────────────'''

class Organization_API(viewsets.ModelViewSet):
    '''
    API general para el CRUD de las organizaciones
    Entrada: name
    Salida: id, name
    '''
    queryset = Organization.objects.all()
    serializer_class = Organization_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','name']

class Degree_Area_API(viewsets.ModelViewSet):
    '''
    API general para el CRUD de las areas o campos de grados academicos
    Entrada: name
    Salida: id, name
    '''
    queryset = Degree_Area.objects.all()
    serializer_class = Degree_Area_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','name']

class Highest_Degree_API(viewsets.ModelViewSet):
    '''
    API general para el CRUD de los grados academicos mas altos
    Entrada: name, id_degree_area, id_organization, year
    Salida: id, name, id_degree_area, id_organization, year

    DEGREE_TYPE\n- 1:BACHILLER\n- 2:MAESTRO\n- 3:DOCTOR

    '''
    queryset = Highest_Degree.objects.all()
    serializer_class = Highest_Degree_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','degree_type','id_degree_area','id_organization']

class Profession_API(viewsets.ModelViewSet):
    '''
    API general para el CRUD de las profesiones de los docentes
    Entrada: 'name', 'id_degree_area', 'id_organization', 'year'
    Salida: 'id', 'name', 'id_degree_area', 'id_organization', 'year'
    '''
    queryset = Profession.objects.all()
    serializer_class = Profession_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','name','id_degree_area','id_organization']

class Research_API(viewsets.ModelViewSet):
    '''
    API general para el CRUD de las investigaciones
    Entrada: 'title', 'research_type', 'contract', 'research_line'
    Salida: 'id', 'title', 'research_type', 'contract', 'research_line', 'date_start', 'date_finish', 'closed'

    RESEARCH_TYPE\n- 1:Basic\n- 2:Applied\n- 3:Formative
    RESEARCH_LINE\n- 1:Computational Intelligence\n- 2:Software Engineering\n- 3:Computational Vision\n- 4:Leng Programac and SW Development\n- 5:Media and Knowledge\n- 6:Applied Information Technology\n- 7:Engineering in Education\n- 8:Other

    '''
    queryset = Research.objects.all()
    serializer_class = Research_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','title','research_type','research_line']

class Internship_API(viewsets.ModelViewSet):
    '''
    API general para el CRUD de las pasantias
    Entrada: 'title', 'internship_type', 'place', 'country'
    Salida: 'id', 'title', 'internship_type', 'place', 'country'

    INTERNSHIP_TYPE\n- 1:Local\n- 2:National\n- 3:International
    '''
    queryset = Internship.objects.all()
    serializer_class = Internship_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','title','internship_type','country']

class Event_API(viewsets.ModelViewSet):
    '''
    API general para el CRUD de los eventos
    Entrada: 'name', 'event_kind', 'event_category', 'date_start', 'date_finish'
    Salida: 'id', 'name', 'event_kind', 'event_category', 'date_start', 'date_finish'

    EVENT_KIND\n- 1:Congress\n- 2:Symposium\n- 3:Seminar\n- 4:Course\n- 5:Conference\n- 6:Internships\n- 7:Journal / Magazine\n- 8:Visitor to Center\n- 9:Others\n- 10:Editorial\n- 11:Journeys
    EVENT_CATEGORY\n- 1:National\n- 2:International'
    '''
    queryset = Event.objects.all()
    serializer_class = Event_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','name','event_kind','event_category']

class Article_API(viewsets.ModelViewSet):
    '''
    API general para el CRUD de los articulos
    Entrada: 'title'
    Salida: 'id', 'title'
    '''
    queryset = Article.objects.all()
    serializer_class = Article_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','title']

class Training_Program_API(viewsets.ModelViewSet):
    '''
    API general para el CRUD de los articulos
    Entrada: 'id_organization', 'training_kind', 'training_title', 'training_sub_type', 'country', 'city', 'training_category'
    Salida: 'id', 'id_organization', 'training_kind', 'training_title', 'training_sub_type', 'country', 'city', 'training_category'

    TRAINING_KIND\n- 1:Training in Teaching\n- 2:Training in Speciality\n- 3:Training in Research\n- 4:Others
    TRAINING_SUB_TYPE\n- 1:Course\n- 2:Workshop\n- 3:Seminar\n- 4:Course Workshop\n- 5:Conference\n- 6:Others
    TRAINING_CATEGORY\n- 1:National\n- 2:International
    '''
    queryset = Training_Program.objects.all()
    serializer_class = Training_Program_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','id_organization','training_kind','training_title','training_sub_type','training_category']


'''───────────────────────────APIS DE MODELOS PRINCIPALES DE DOCUMENTOS DE CRITERIO 06───────────────────────────'''

class Professor_Qualification_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Calificaciones de Docentes - Criterio 06
    ENTRADA: 'id_criteria','id_user','id_highest_degree', 'id_profession', 'teacher_category', 'teacher_regimen', 'goverment_experience', 'teaching_experience', 'unsa_experience', 'professional_organization_level', 'professional_development_level', 'consultory_level', 'status', 'observations'
    SALIDA: 'id','id_criteria','id_user','id_highest_degree', 'id_profession', 'teacher_category', 'teacher_regimen', 'goverment_experience', 'teaching_experience', 'unsa_experience', 'professional_organization_level', 'professional_development_level', 'consultory_level', 'status', 'observations'    
    
    TEACHER_CATEGORY\n- 1:DOCENTE PRINCIPAL\n- 2:DOCENTE ASOCIADO\n- 3:DOCENTE AUXILIAR
    TEACHER_REGIMEN\n- 1:TIEMPO COMPLETO\n- 2:TIEMPO PARCIAL

    LEVEL\n- 0: \n- 1: LOW\n- 2:MID\n- 3:HIGH

    STATUS\n- 1:Aprobado\n- 2:Pendiente\n- 3:Rechazado
    '''
    queryset = Professor_Qualification.objects.all()
    serializer_class =  Professor_Qualification_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','id_user','id_highest_degree','id_profession']

class Professor_Researcher_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de docente Investigador en la base de datos - Criterio 06
    ENTRADA: 'id_criteria','id_user','renacyt_code', 'current_organizations', 'validity_date_start', 'validity_date_finish', 'renacyt_group', 'renacyt_group_level', 'status', 'observations'
    SALIDA: 'id','id_criteria','id_user','renacyt_code', 'current_organizations', 'validity_date_start', 'validity_date_finish', 'renacyt_group', 'renacyt_group_level', 'status', 'observations'

    RENACYT_GROUP\n- 1:María Rostworowski\n- 2:Carlos Monge Medrano
    RENACYT_GROUP_LEVEL\n- 1:I\n- 2:II\n- 3:III\n- 4:IV

    STATUS\n- 1:Aprobado\n- 2:Pendiente\n- 3:Rechazado
    '''
    queryset = Professor_Researcher.objects.all()
    serializer_class =  Professor_Researcher_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_user','renacyt_code','current_organizations','renacyt_group','renacyt_group_level']

class Professor_Research_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Investigaciones del docente en la base de datos - Criterio 06
    ENTRADA: 'id_criteria','id_user','school','teacher_category','teacher_regimen','id_research','participation_type','status','observations'
    SALIDA: 'id','id_criteria','id_user','school','teacher_category','teacher_regimen','id_research','participation_type',,'status','observations'

    SCHOOL\n- 1:Systems Engineering\n- 2:Others
    TEACHER_CATEGORY\n- 1:DOCENTE PRINCIPAL\n- 2:DOCENTE ASOCIADO\n- 3:DOCENTE AUXILIAR
    TEACHER_REGIMEN\n- 1:TIEMPO COMPLETO\n- 2:TIEMPO PARCIAL

    PARTICIPATION_TYPE\n- 1:Investigador Principal\n- 2:Co-Investigador

    STATUS\n- 1:Aprobado\n- 2:Pendiente\n- 3:Rechazado
    '''
    queryset = Professor_Research.objects.all()
    serializer_class =  Professor_Research_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','id_user','id_research','participation_type']

class Professor_Workload_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de carga de trabajo del docente en la base de datos - Criterio 06
    ENTRADA: 'id_criteria','id_user','teacher_regimen','asigned_courses','total_hours','preparation_hours','comission_hours','other_school_hours','other_ocupation_hours','regimen_hours','teaching_program','research_program','other_program','no_program','total_program','status','observations'
    SALIDA: 'id','id_criteria','id_user','teacher_regimen','asigned_courses','total_hours','preparation_hours','comission_hours','other_school_hours','other_ocupation_hours','regimen_hours','teaching_program','research_program','other_program','no_program','total_program','status','observations'

    STATUS\n- 1:Aprobado\n- 2:Pendiente\n- 3:Rechazado
    '''
    queryset = Professor_Workload.objects.all()
    serializer_class =  Professor_Workload_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','id_user','teacher_regimen']

class Professor_Survey_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de evaluaciones a docentes del Criteio 06
    ENTRADA: id_criteria, id_user, id_portfolio, evaluation_note, status, observations
    SALIDA: id, id_criteria, id_user, id_portfolio, evaluation_note, status, observations

    STATUS\n- 1:Aprobado\n- 2:Pendiente\n- 3:Rechazado
    '''
    queryset = Professor_Survey.objects.all()
    serializer_class =  Professor_Survey_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','id_user','id_portfolio','evaluation_note']

class Professor_Recognition_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de evaluaciones a docentes del Criteio 06
    ENTRADA: id_criteria, id_user, professor_description, status, observations
    SALIDA: id, id_criteria, id_user, professor_description, status, observations
    '''
    queryset = Professor_Recognition.objects.all()
    serializer_class =  Professor_Recognition_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','id_user']

class Professor_Internship_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de pasantias del Criteio 06
    ENTRADA: id_criteria, id_user, school, teacher_category, teacher_regimen, id_internship, date_start, date_finish, status, observations
    SALIDA: id, id_criteria, id_user, school, teacher_category, teacher_regimen, id_internship, date_start, date_finish, status, observations

    STATUS\n- 1:Aprobado\n- 2:Pendiente\n- 3:Rechazado
    '''
    queryset = Professor_Internship.objects.all()
    serializer_class =  Professor_Internship_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','id_user','id_internship']

class Professor_Training_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Entrenamiento de docentes del Criteio 06
    ENTRADA: id_criteria, id_user, school, teacher_category, teacher_regimen, id_training_program, date_start, date_finish, theory_hours, practical_hours, credits, approval, note, evidence, status, observation
    SALIDA: id, id_criteria, id_user, school, teacher_category, teacher_regimen, id_training_program, date_start, date_finish, theory_hours, practical_hours, credits, approval, note, evidence, status, observation

    SCHOOL\n- 1:Systems Engineering\n- 2:Others
    TEACHER_CATEGORY\n- 1:DOCENTE PRINCIPAL\n- 2:DOCENTE ASOCIADO\n- 3:DOCENTE AUXILIAR
    TEACHER_REGIMEN\n- 1:TIEMPO COMPLETO\n- 2:TIEMPO PARCIAL

    STATUS\n- 1:Aprobado\n- 2:Pendiente\n- 3:Rechazado
    '''
    queryset = Professor_Training.objects.all()
    serializer_class =  Professor_Training_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','id_user','id_training_program','approval']

class Professor_Participation_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de Participaciones de docente del Criteio 06
    ENTRADA: id_criteria, id_user,school, teacher_category, teacher_regimen, id_event, article, id_article, author_type, rol, country, city, source, evidence, status, observations
    SALIDA: id, id_criteria, id_user,school, teacher_category, teacher_regimen, id_event, article, id_article, author_type, rol, country, city, source, evidence, status, observations

    SCHOOL\n- 1:Systems Engineering\n- 2:Others

    TEACHER_CATEGORY\n- 1:DOCENTE PRINCIPAL\n- 2:DOCENTE ASOCIADO\n- 3:DOCENTE AUXILIAR
    TEACHER_REGIMEN\n- 1:TIEMPO COMPLETO\n- 2:TIEMPO PARCIAL

    ROL\n- 1:Speaker\n- 2:Exhibitor\n- 3:Professor\n- 4:Assitant\n- 5:Student\n- 6:Reviewer\n- 7:Others
    AUTHOR_TYPE\n- 1:Author\n- 2:Coauthor
    SOURCE\n- 1:Department\n- 2:Professor

    STATUS\n- 1:Aprobado\n- 2:Pendiente\n- 3:Rechazado
    '''
    queryset = Professor_Participation.objects.all()
    serializer_class =  Professor_Participation_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','id_criteria','id_user','id_event','article','author_type','rol']

'''───────────────────────────APIS DE MODELOS PARA EL MANEJO DE EVIDENCIAS DE DOCUMENTOS DE CRITERIO 06───────────────────────────'''

class Training_Doc_Evidence_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de documentos de evidencia de entrenamientos del Criteio 06
    ENTRADA: id_training, description, doc_evidence
    SALIDA: id, id_training, description, doc_evidence
    '''
    queryset = Training_Doc_Evidence.objects.all()
    serializer_class =  Training_Doc_Evidence_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','id_training','description']

class Participation_Doc_Evidence_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de documentos de evidencia de participaciones del Criteio 06
    ENTRADA: id_participation, description, doc_evidence
    SALIDA: id, id_participation, description, doc_evidence
    '''
    queryset = Participation_Doc_Evidence.objects.all()
    serializer_class =  Participation_Doc_Evidence_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','id_participation','description']

class Recognition_Doc_Evidence_API(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de documentos de evidencia de participaciones del Criteio 06
    ENTRADA: id_recognition, description, doc_evidence
    SALIDA: id, id_recognition, description, doc_evidence
    '''
    queryset = Recognition_Doc_Evidence.objects.all()
    serializer_class =  Recognition_Doc_Evidence_Serializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id','id_recognition','description']
