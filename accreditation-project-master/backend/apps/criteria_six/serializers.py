from django.contrib.postgres import fields
from apps.criteria_six.models import *
from rest_framework import serializers
from accreditation.utils import *

#  ──────────────────────────────────────────────────────CRITERIO 06 ──────────────────────────────────────────────────────
'''───────────────────────────SERIALIZADORES DE MODELOS SECUNDARIOS DE DOCUMENTOS DE CRITERIO 06───────────────────────────'''

class Organization_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de organizaciones - Criterio 06'''
    class Meta:
        model  = Organization
        fields = ['id','name']

class Degree_Area_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de las areas o campos de los grados academicos - Criterio 06'''
    class Meta:
        model  = Degree_Area
        fields = ['id','name']

class Highest_Degree_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de los grados academicos mas altos - Criterio 06'''
    class Meta:
        model  = Highest_Degree
        fields = ['id','degree_type','id_degree_area','id_organization','year']

class Profession_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de las profesiones de los docentes - Criterio 06'''
    class Meta:
        model  = Profession
        fields = ['id','name','id_degree_area','id_organization','year']

class Research_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de la investigacione - Criterio 06'''
    class Meta:
        model  = Research
        fields = ['id','title','research_type','contract','research_line','date_start','date_finish','closed']

class Internship_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Pasantia - Criterio 06'''
    class Meta:
        model  = Internship
        fields = ['id','title','internship_type','place','country']

class Event_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Evento - Criterio 06'''
    class Meta:
        model  = Event
        fields = ['id','name','event_kind','event_category','date_start','date_finish']

class Article_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Evento - Criterio 06'''
    class Meta:
        model  = Article
        fields = ['id','title']

class Training_Program_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Evento - Criterio 06'''
    class Meta:
        model  = Training_Program
        fields = ['id','id_organization','training_kind','training_title','training_sub_type','country','city','training_category']


'''───────────────────────────SERIALIZADORES DE MODELOS PRINCIPALES DE DOCUMENTOS DE CRITERIO 06───────────────────────────'''

class Professor_Qualification_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Calificacion - Criterio 06'''
    class Meta:
        model  = Professor_Qualification
        #fields = ['id','id_criteria','id_user','id_highest_degree','id_profession','teacher_category','teacher_regimen','goverment_experience','teaching_experience','unsa_experience','professional_organization_level','professional_development_level','consultory_level','status','observations']
        fields = '__all__'

class Professor_Researcher_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Investigador - Criterio 06'''
    class Meta:
        model  = Professor_Researcher
        #fields = ['id','id_criteria','id_user','renacyt_code','organizations','validity_date_start','validity_date_finish','renacyt_group', 'renacyt_group_level', 'status', 'observations']
        fields = '__all__'

class Professor_Research_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Investigacion de docente - Criterio 06'''
    class Meta:
        model  = Professor_Research
        fields = '__all__'

class Professor_Workload_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de carga de trabajo de docente - Criterio 06'''
    class Meta:
        model  = Professor_Workload
        fields = '__all__'

class Professor_Recognition_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de reconocimiento de docente - Criterio 06'''
    class Meta:
        model  = Professor_Recognition
        fields = '__all__'

class Professor_Survey_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de evaluacion a docente - Criterio 06'''
    class Meta:
        model  = Professor_Survey
        fields = '__all__'

class Professor_Internship_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Pasantia - Criterio 06'''
    class Meta:
        model  = Professor_Internship
        #fields = ['id','id_criteria','id_user','school','teacher_category','teacher_regimen','date_start','date_finish','status','observations']
        fields = '__all__'

class Professor_Training_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Entrenamiento - Criterio 06'''

    class Meta:
        model  = Professor_Training
        fields = '__all__'

class Professor_Participation_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de Paricipacion - Criterio 06'''

    class Meta:
        model  = Professor_Participation
        fields = '__all__'

'''───────────────────────────SERIALIZADORES DE MODELOS PARA EL MANEJO DE EVIDENCIAS DE DOCUMENTOS DE CRITERIO 06───────────────────────────'''

class Training_Doc_Evidence_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de evidencia de entrenamiento - Criterio 06'''
    doc_evidence_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = Training_Doc_Evidence
        fields = '__all__'

class Participation_Doc_Evidence_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de evidencia de participacion - Criterio 06'''
    doc_evidence_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = Participation_Doc_Evidence
        fields = '__all__'

class Recognition_Doc_Evidence_Serializer(serializers.ModelSerializer):
    '''Serializador general para todo el CRUD de evidencia de participacion - Criterio 06'''
    doc_evidence_filename = serializers.CharField(read_only=True)

    class Meta:
        model  = Recognition_Doc_Evidence
        fields = '__all__'
    