from abc import abstractclassmethod
from django.contrib.auth.models import Group
from apps.user.models import *
from apps.process.models import *
from apps.portfolio.models import *
from django.contrib.postgres.fields import ArrayField
from django.core.validators import RegexValidator, FileExtensionValidator, MinValueValidator, MaxValueValidator
from django.db import models

#  ─────────────────────────────────────────────CRITERIO 06 ─────────────────────────────────────────────

def get_filename(instance): return instance.name.split("/")[-1] if instance else None

def Criteria_path(Criteria): return f'documents/{Criteria.id_phase.id_process.name}/{Criteria.id_phase.name}/{Criteria.name}'
'''Función para obtener la dirección donde se almacena los documentos de un criterio'''

def TrainingEvidenceDocument_path(instance, filename): return f'{Criteria_path(instance.id_criteria)}/ENTRENAMIENTO/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de entrenamientos - criterio 06'''

def ParticipationEvidenceDocument_path(instance, filename): return f'{Criteria_path(instance.id_criteria)}/PARTICIPACION/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de participaciones en evento - criterio 06'''

def RecognitionEvidenceDocument_path(instance, filename): return f'{Criteria_path(instance.id_criteria)}/RECONOCOMIENTO/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de reconocimientos de los docentes - criterio 06'''


'''───────────────────────────MODELOS SECUNDARIOS DE DOCUMENTOS DE CRITERIO 06───────────────────────────'''

class Organization(models.Model):
    '''Modelo de las organizaciones en la base de datos'''
    name = models.CharField(blank=False,unique=True,max_length=200,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,200}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 200 caracteres'
    )]) # nombre de la organizacion

    class Meta:
        verbose_name = 'organizacion'
        verbose_name_plural = 'organizaciones'

    def __str__(self): return f'{self.name}'

class Degree_Area(models.Model):
    '''Modelo de las areas de grado academico o profesion en la base de datos'''
    name = models.CharField(blank=False,unique=True,max_length=100,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,200}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 200 caracteres'
    )]) # nombre del campo del grado academico

    class Meta:
        verbose_name = 'area de grado academico'
        verbose_name_plural = 'grados academicos > areas'

    def __str__(self): return f'{self.name}'

class Highest_Degree(models.Model):
    '''Modelo de los grados academicos mas altos de docente en la base de datos'''
    DEGREE_TYPE = ((1, 'BACHILLER'),(2,'MAESTRO'),(3,'DOCTOR'))

    degree_type = models.PositiveSmallIntegerField(blank=False,default=1,choices=DEGREE_TYPE,verbose_name='tipo grado academico',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # Tipo del grado academico
    id_degree_area = models.ForeignKey(Degree_Area,on_delete=models.CASCADE,null=False,blank=False,verbose_name='area grado academico')
    # id del campo del grado academico
    id_organization = models.ForeignKey(Organization,on_delete=models.CASCADE,null=False,blank=False,verbose_name='organizacion responsable grado academico')
    # id de la organizacion responsable del grado academico
    year = models.PositiveSmallIntegerField(blank=True,default=2021,verbose_name='año obtencion grado academico',
        validators = [MinValueValidator(1900), MaxValueValidator(2100)],
    ) # año de obtencion de grado academico

    class Meta:
        verbose_name = 'grado academico'
        verbose_name_plural = 'grados academicos'

    def __str__(self): return f'{self.degree_type} | {self.id_degree_area.name} | {self.id_organization.name} | {self.year}'

class Profession(models.Model):
    '''Modelo de la profesion de docente en la base de datos'''
    name = models.CharField(blank=False,unique=True,max_length=100,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 100 caracteres'
    )]) # nombre de la profesion
    id_degree_area = models.ForeignKey(Degree_Area,on_delete=models.CASCADE,null=False,blank=False,verbose_name='area profesional')
    # id del campo de la profesion
    id_organization = models.ForeignKey(Organization,on_delete=models.CASCADE,null=False,blank=False,verbose_name='organizacion responsable profesion')
    # id de la organizacion responsable de la profesion
    year = models.PositiveSmallIntegerField(blank=True,default=2021,verbose_name='año obtencion profesion',
        validators = [MinValueValidator(1900), MaxValueValidator(2100)],
    ) # año de obtencion de la profesion

    class Meta:
        verbose_name = 'profesion'
        verbose_name_plural = 'profesiones'

    def __str__(self): return f'{self.name} | {self.id_degree_area.name} | {self.id_organization.name} | {self.year}'

class Research(models.Model):
    '''Modelo de Investigacion en la base de datos'''
    RESEARCH_TYPE = ((1,'Basic'),(2,'Applied'),(3,'Formative'))
    RESEARCH_LINE = ((1,'Computational Intelligence'),(2,'Software Engineering'),(3,'Computational Vision'),(4,'Leng Programac and SW Development'),(5,'Media and Knowledge'),(6,'Applied Information Technology'),(7,'Engineering in Education'),(8,'Other'))

    title = models.CharField(blank=False,unique=True,max_length=500,verbose_name='titulo investigacion',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,500}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 500 caracteres'
    )]) # titulo de la investigacion
    research_type = models.PositiveSmallIntegerField(blank=False,default=1,choices=RESEARCH_TYPE,verbose_name='tipo investigacion',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # tipo de la investigacion
    contract = models.CharField(blank=False,unique=True,max_length=25,verbose_name='numero contrato investigacion',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\()–\-{}¡!¿?\s]{0,25}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_-.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 25 caracteres'
    )]) # Numero de contrato de la investigacion
    research_line = models.PositiveSmallIntegerField(blank=False,default=1,choices=RESEARCH_LINE,verbose_name='linea de investigacion',
        validators = [MinValueValidator(1), MaxValueValidator(8)]
    ) # linea de la investigacion
    date_start = models.DateField(null=True,blank=True,verbose_name='fecha inicio investigacion'
    ) # fecha de inicio de investigacion
    date_finish = models.DateField(null=True,blank=True,verbose_name='fecha fin investigacion'
    ) # fecha de fin de investigacion
    closed = models.BooleanField(blank=True,default=False,verbose_name='investigacion finalizada')
    # Estado de la invetigacion

    class Meta:
       verbose_name = 'investigacion'
       verbose_name_plural = 'investigaciones'

    def __str__(self): return f'{self.title} | {self.research_type} | {self.research_line}'

class Internship(models.Model):
    '''Modelo de Pasantia en la base de datos'''
    INTERNSHIP_TYPE = ((1,'Local'),(2,'National'),(3,'International'))

    title = models.CharField(blank=False,unique=True,max_length=500,verbose_name='titulo pasantia',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,500}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 500 caracteres'
    )]) # titulo del pasantia
    internship_type = models.PositiveSmallIntegerField(blank=False,default=1,choices=INTERNSHIP_TYPE,verbose_name='tipo de pasantia',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # tipo de la pasantia
    place = models.CharField(blank=False,unique=True,max_length=200,verbose_name='lugar pasantia',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\()–\-{}¡!¿?\s]{0,200}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_-.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 200 caracteres'
    )]) # Lugar de la pasantia
    country = models.CharField(blank=False,max_length=50,verbose_name='pais pasantia',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 50 caracteres'
    )]) # Pais de la pasantia

    class Meta:
       verbose_name = 'pasantia'
       verbose_name_plural = 'pasantias'

    def __str__(self): return f'{self.title} | {self.internship_type} | {self.country}'

class Event(models.Model):
    '''Modelo de Evento en la base de datos'''
    EVENT_KIND = ((1,'Congress'),(2,'Symposium'),(3,'Seminar'),(4,'Course'),(5,'Conference'),(6,'Internships'),(7,'Journal / Magazine'),(8,'Visitor to Center'),(9,'Others'),(10,'Editorial'),(11,'Journeys'))
    EVENT_CATEGORY = ((1,'National'),(2,'International'))
    
    name = models.TextField(blank=True,max_length=200,verbose_name='nombre evento',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,200}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 200 caracteres'
    )]) # Nombre del evento
    event_kind = models.PositiveSmallIntegerField(blank=False,default=1,choices=EVENT_KIND,verbose_name='tipo evento',
        validators = [MinValueValidator(1), MaxValueValidator(11)]
    ) # Tipo de evento
    event_category = models.PositiveSmallIntegerField(blank=False,default=1,choices=EVENT_CATEGORY,verbose_name='categoria evento',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # Categoria del evento
    date_start = models.DateField(null=True,blank=True,verbose_name='fecha inicio')
     #fecha de inicio del evento
    date_finish = models.DateField(null=True,blank=True,verbose_name='fecha fin')
     #fecha de fin del evento

    class Meta:
       verbose_name = 'evento'
       verbose_name_plural = 'eventos'

    def __str__(self): return f'{self.name} | {self.event_kind} | {self.event_category}'

class Article(models.Model):
    '''Modelo de Evento en la base de datos'''
    
    title = models.TextField(blank=True,max_length=200,verbose_name='titulo articulo',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,200}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 200 caracteres'
    )]) # Titulo de articulo

    class Meta:
       verbose_name = 'articulo'
       verbose_name_plural = 'articulos'

    def __str__(self): return f'{self.title}'

class Training_Program(models.Model):
    '''Modelo de Evento en la base de datos'''
    
    TRAINING_KIND = ((1,'Training in Teaching'),(2,'Training in Speciality'),(3, 'Training in Research'),(4,'Others'))
    TRAINING_SUB_TYPE = ((1,'Course'),(2,'Workshop'),(3,'Seminar'),(4,'Course Workshop'),(5,'Conference'),(6,'Others'))
    TRAINING_CATEGORY = ((1,'National'),(2,'International'))

    id_organization = models.ForeignKey(Organization,on_delete=models.CASCADE,null=True,blank=True,verbose_name='organizacion entrenamiento')
    # id de la organizacion del entrenamiento
    training_kind = models.PositiveSmallIntegerField(blank=False,default=1,choices=TRAINING_KIND,verbose_name='tipo entrenamiento',
        validators = [MinValueValidator(1), MaxValueValidator(4)]
    ) # Tipo de entreamiento
    training_title = models.TextField(blank=True,max_length=100,verbose_name='titulo entrenamiento',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 100 caracteres'
    )]) # Titulo del entrenamiento
    training_sub_type = models.PositiveSmallIntegerField(blank=False,default=1,choices=TRAINING_SUB_TYPE,verbose_name='subtipo entrenamiento',
        validators = [MinValueValidator(1), MaxValueValidator(6)]
    ) # Subtipo de entreamiento
    country = models.CharField(blank=False,max_length=50,verbose_name='pais',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 50 caracteres'
    )]) # Pais de entrenamiento
    city = models.CharField(blank=False,max_length=50,verbose_name='ciudad',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 50 caracteres'
    )]) # Ciudad de entrenamiento
    training_category = models.PositiveSmallIntegerField(blank=False,default=1,choices=TRAINING_CATEGORY,verbose_name='categoria entrenamiento',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # Categoria del entrenamiento

    class Meta:
       verbose_name = 'programa entrenamiento'
       verbose_name_plural = 'entrenamientos > programas'

    def __str__(self): return f'{self.training_title} | {self.training_kind} | {self.training_sub_type} | {self.training_category}'


'''───────────────────────────MODELOS PRINCIPALES DE DOCUMENTOS DE CRITERIO 06───────────────────────────'''

class Professor_Qualification(models.Model):
    '''Modelo de Calificaciones de docente en la base de datos - Criterio 06'''
    TEACHER_CATEGORY = ((1,'DOCENTE PRINCIPAL'),(2,'DOCENTE ASOCIADO'),(3,'DOCENTE AUXILIAR'))
    TEACHER_REGIMEN = ((1,'TIEMPO COMPLETO'),(2,'TIEMPO PARCIAL'))

    LEVEL = ((0,' '),(1,'LOW'),(2,'MID'),(3,'HIGH'))

    STATUS = ((1,'Aprobado'),(2,'Pendiente'),(3,'Rechazado'))

    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='criterio')
    # id del criterio
    id_user = models.ForeignKey(User,on_delete=models.PROTECT,null=True,blank=True,verbose_name='docente')
    # id del docente
    id_highest_degree = models.ForeignKey(Highest_Degree,on_delete=models.CASCADE,null=False,blank=False,verbose_name='grado academido mas alto docente'
    ) # Grado academico mas alto del docente
    id_profession = models.ForeignKey(Profession,on_delete=models.CASCADE,null=False,blank=False,verbose_name='profesion docente'
    ) # Profesion del docente
    teacher_category = models.PositiveSmallIntegerField(blank=False,default=1,choices=TEACHER_CATEGORY,verbose_name='categoria docente',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # Categoria del docente
    teacher_regimen = models.PositiveSmallIntegerField(blank=False,default=1,choices=TEACHER_REGIMEN,verbose_name='regimen docente',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # Registro del docente
    goverment_experience = models.PositiveSmallIntegerField(null=True,blank=True,default=0,verbose_name='experiencia gobierno',
        validators = [MinValueValidator(0), MaxValueValidator(100)]
    ) # Experiencia del docente en el gobierno
    teaching_experience = models.PositiveSmallIntegerField(null=True,blank=True,default=0,verbose_name='experiencia enseñanza',
        validators = [MinValueValidator(0), MaxValueValidator(100)]
    ) # Experiencia del docente en enseñanza
    unsa_experience = models.PositiveSmallIntegerField(null=True,blank=True,default=0,verbose_name='experiencia UNSA',
        validators = [MinValueValidator(0), MaxValueValidator(100)]
    ) # Experiencia del docente en la UNSA
    professional_organization_level = models.PositiveSmallIntegerField(blank=False,default=0,choices=LEVEL,verbose_name='nivel actividad organizaciones profesionales',
        validators = [MinValueValidator(0), MaxValueValidator(3)]
    ) # Nivel de actividad en organizaciones profesionales
    professional_development_level = models.PositiveSmallIntegerField(blank=False,default=0,choices=LEVEL,verbose_name='nivel actividad desarrollo profesional',
        validators = [MinValueValidator(0), MaxValueValidator(3)]
    ) # Nivel de actividad en desarrollo profesional
    consultory_level = models.PositiveSmallIntegerField(blank=False,default=0,choices=LEVEL,verbose_name='nivel actividad consultoria',
        validators = [MinValueValidator(0), MaxValueValidator(3)]
    ) # Nivel de actividad en consultoria
    status = models.PositiveSmallIntegerField(blank=False,default=2,choices=STATUS,verbose_name='estado registro',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # Estado de registro de calificacion
    observations = ArrayField(models.TextField(max_length=1000,
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres'
    )]),null=False,blank=True,default=list,verbose_name='observaciones'
    ) # Observaciones del registro de calificacion

    class Meta:
       verbose_name = 'criterio 06: calificacion docente'
       verbose_name_plural = 'criterios > criterio 06 > calificaciones docente'

    def __str__(self): return f'{self.id_user.name} | {self.id_highest_degree.degree_type} | {self.id_profession.name}'

class Professor_Researcher(models.Model):
    '''Modelo de docente Investigador en la base de datos - Criterio 06'''

    RENACYT_GROUP = ((1,'María Rostworowski'),(2,'Carlos Monge Medrano'))
    RENACYT_GROUP_LEVEL = ((1,'I'),(2,'II'),(3,'III'),(4,'IV'))

    STATUS = ((1,'Aprobado'),(2,'Pendiente'),(3,'Rechazado'))

    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='criterio'
    ) # id del criterio
    id_user = models.ForeignKey(User,on_delete=models.PROTECT,null=True,blank=True,verbose_name='docente'
    ) # id del docente
    renacyt_code = models.CharField(blank=False,unique=True,max_length=10,verbose_name='codigo Renacyt',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,10}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 10 caracteres'
    )]) # codigo renacyt de docente
    current_organizations = models.ManyToManyField(Organization,blank=True,related_name='organizations',verbose_name='organizaciones trabajo actual'
    ) # id's de las organizaciones de trabajos actuales
    validity_date_start = models.DateField(null=True,blank=True,verbose_name='fecha inicio validez Renacyt'
    ) # fecha de inicio de validez de Renacyt
    validity_date_finish = models.DateField(null=True,blank=True,verbose_name='fecha fin validez Renacyt'
    ) # fecha de fin de validez de Renacyt
    renacyt_group = models.PositiveSmallIntegerField(blank=False,default=1,choices=RENACYT_GROUP,verbose_name='grupo Renacyt',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # grupo de Renacyt
    renacyt_group_level = models.PositiveSmallIntegerField(blank=False,default=1,choices=RENACYT_GROUP_LEVEL,verbose_name='nivel grupo Renacyt',
        validators = [MinValueValidator(1), MaxValueValidator(4)]
    ) # nivel en grupo de Renacyt
    status = models.PositiveSmallIntegerField(blank=False,default=2,choices=STATUS,verbose_name='estado registro',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # estado de registro de docente investigador
    observations = ArrayField(models.TextField(max_length=1000,
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres'
    )]),null=False,blank=True,default=list,verbose_name='observaciones'
    ) # Observaciones del registro de docente investigador

    class Meta:
        verbose_name = 'criterio 06: investigador'
        verbose_name_plural = 'criterios > criterio 06 > investigadores'

    def __str__(self): return f'{self.id_user.name} | {self.renacyt_code} | {self.renacyt_group} | {self.renacyt_group_level}'

class Professor_Research(models.Model):
    '''Modelo de las Investigaciones del docente en la base de datos - Criterio 06'''
    SCHOOL = ((1,'Systems Engineering'),(2,'Others'))
    TEACHER_CATEGORY = ((1,'DOCENTE PRINCIPAL'),(2,'DOCENTE ASOCIADO'),(3,'DOCENTE AUXILIAR'))
    TEACHER_REGIMEN = ((1,'TIEMPO COMPLETO'),(2,'TIEMPO PARCIAL'))

    PARTICIPATION_TYPE = ((1,'Principal Investigator'),(2,'Co-Investigator'))

    STATUS = ((1,'Aprobado'),(2,'Pendiente'),(3,'Rechazado'))

    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='criterio'
    ) # id del criterio
    id_user = models.ForeignKey(User,on_delete=models.PROTECT,null=True,blank=True,verbose_name='docente'
    ) # id del docente
    school = models.PositiveSmallIntegerField(blank=False,default=1,choices=SCHOOL,verbose_name='escuela profesional',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # escuela profesional a la que pertenece el docente
    teacher_category = models.PositiveSmallIntegerField(blank=False,default=1,choices=TEACHER_CATEGORY,verbose_name='categoria docente',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # Categoria del docente
    teacher_regimen = models.PositiveSmallIntegerField(blank=False,default=1,choices=TEACHER_REGIMEN,verbose_name='regimen docente',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # Regimen del docente
    id_research = models.ForeignKey(Research,on_delete=models.CASCADE,null=True,blank=True,verbose_name='investigacion docente'
    ) # id de las investigacion del docente
    participation_type = models.PositiveSmallIntegerField(blank=False,default=1,choices=PARTICIPATION_TYPE,verbose_name='tipo participacion',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # Tipo de participacion del docente
    status = models.PositiveSmallIntegerField(blank=False,default=2,choices=STATUS,verbose_name='estado registro',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # estado de registro de investigacion de docente
    observations = ArrayField(models.TextField(max_length=1000,
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres'
    )]),null=False,blank=True,default=list,verbose_name='observaciones'
    ) # Observaciones del registro de investigacion de docente

    class Meta:
        verbose_name = 'criterio 06: investigacion'
        verbose_name_plural = 'criterios > criterio 06 > investigaciones'

    def __str__(self): return f'{self.id_user.name} | {self.researchs} | {self.participation_type} | {self.closed}'

class Professor_Workload(models.Model):
    '''Modelo de carga de trabajo de docente en la base de datos - Criterio 06'''
    TEACHER_REGIMEN = ((1,'TIEMPO COMPLETO'),(2,'TIEMPO PARCIAL'))

    STATUS = ((1,'Aprobado'),(2,'Pendiente'),(3,'Rechazado'))

    id_criteria = models.OneToOneField(Criteria,on_delete=models.CASCADE,null=False,blank=False,primary_key=False,verbose_name='criterio'
    ) # id del criterio
    id_user = models.OneToOneField(User,on_delete=models.CASCADE,null=False,blank=False,primary_key=False,verbose_name='usuario'
    ) # id del docente
    teacher_regimen = models.PositiveSmallIntegerField(blank=False,default=1,choices=TEACHER_REGIMEN,verbose_name='regimen docente',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # Regimen del docente
    asigned_courses = models.ManyToManyField(CourseActive,blank=True,related_name='courses',verbose_name='cursos activos asignados docente'
    ) # id's de los cursos activos asignados al docente
    total_hours = models.FloatField(null=True,blank=False,default=0,verbose_name='total horas cursos asignados',
        validators = [MinValueValidator(0), MaxValueValidator(40)]
    ) # horas totales de los cursos activos asignados al docente
    preparation_hours = models.FloatField(null=True,blank=False,default=0,verbose_name='horas preparacion',
        validators = [MinValueValidator(0), MaxValueValidator(10)]
    ) # horas en preparacion del docente
    comission_hours = models.FloatField(null=True,blank=False,default=0,verbose_name='horas comision',
        validators = [MinValueValidator(0), MaxValueValidator(50)]
    ) # horas en comision del docente
    other_school_hours = models.FloatField(null=True,blank=False,default=0,verbose_name='horas otras escuelas',
        validators = [MinValueValidator(0), MaxValueValidator(40)]
    ) # horas en otras escuelas del docente
    other_ocupation_hours = models.FloatField(null=True,blank=False,default=0,verbose_name='horas otros cargos',
        validators = [MinValueValidator(0), MaxValueValidator(40)]
    ) # horas en otros cargos del docente
    regimen_hours = models.FloatField(null=True,blank=False,default=0,verbose_name='horas regimen docente',
        validators = [MinValueValidator(0), MaxValueValidator(40)]
    ) # horas del regimen del docente
    teaching_program = models.FloatField(null=True,blank=False,default=0,verbose_name='porcentaje actividad enseñanza',
        validators = [MinValueValidator(0), MaxValueValidator(100)]
    ) # porcentaje de actividades en enseñanza
    research_program = models.FloatField(null=True,blank=False,default=0,verbose_name='porcentaje actividad investigacion',
        validators = [MinValueValidator(0), MaxValueValidator(100)]
    ) # porcentaje de actividades en investigacion
    other_program = models.FloatField(null=True,blank=False,default=0,verbose_name='porcentaje otras actividades programa',
        validators = [MinValueValidator(0), MaxValueValidator(100)]
    ) # porcentaje de otras actividades en el programa
    no_program = models.FloatField(null=True,blank=False,default=0,verbose_name='porcentaje actividades no programa',
        validators = [MinValueValidator(0), MaxValueValidator(100)]
    ) # porcentaje de otras actividades fuera del programa
    total_program = models.FloatField(null=True,blank=False,default=0,verbose_name='porcentaje total actividades programa',
        validators = [MinValueValidator(0), MaxValueValidator(100)]
    ) # porcentaje total de  actividades  del programa
    status = models.PositiveSmallIntegerField(blank=False,default=2,choices=STATUS,verbose_name='estado registro',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # estado de registro de evaluaciones a docentes
    observations = ArrayField(models.TextField(max_length=1000,
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres'
    )]),null=False,blank=True,default=list,verbose_name='observaciones'
    ) # Observaciones del registro de evaluaciones a docentes

    class Meta:
        verbose_name = 'criterio 06: carga de trabajo'
        verbose_name_plural = 'criterios > criterio 06 > cargas de trabajo'
        unique_together = ['id_criteria', 'id_user']

    def __str__(self): return f'{self.id_user.name} | {self.id_course_active.id_course.name} | {self.id_course_active.id_course.id_subarea.id_formation_area.name} | {self.id_course_active.id_course.id_subarea.name} | {self.id_course_active.id_portfolio.year}'

class Professor_Survey(models.Model):
    '''Modelo de evaluacion a docente en la base de datos - Criterio 06'''
    
    STATUS = ((1,'Aprobado'),(2,'Pendiente'),(3,'Rechazado'))

    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='criterio'
    ) # id del criterio
    id_user = models.ForeignKey(User,on_delete=models.PROTECT,null=True,blank=True,verbose_name='docente'
    ) # id del docente
    id_portfolio = models.ForeignKey(Portfolio,on_delete=models.PROTECT,null=True,blank=True,verbose_name='portafolio'
    ) # id del portafolio
    evaluation_note = models.FloatField(null=False,blank=False,default=0,verbose_name='nota evaluacion',
        validators = [MinValueValidator(0), MaxValueValidator(20)]
    ) # nota de evaluacion a docente
    status = models.PositiveSmallIntegerField(blank=False,default=2,choices=STATUS,verbose_name='estado registro',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # estado de registro de evaluaciones a docentes
    observations = ArrayField(models.TextField(max_length=1000,
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres'
    )]),null=False,blank=True,default=list,verbose_name='observaciones'
    ) # Observaciones del registro de evaluaciones a docentes

    class Meta:
        verbose_name = 'criterio 06: encuesta de evaluacion'
        verbose_name_plural = 'criterios > criterio 06 > encuestas de evaluacion'

    def __str__(self): return f'{self.id_user.name} | {self.id_portfolio.year} | {self.id_portfolio.semester}'

class Professor_Recognition(models.Model):
    '''Modelo de reconocimiento de docente en la base de datos - Criterio 06'''

    STATUS = ((1,'Aprobado'),(2,'Pendiente'),(3,'Rechazado'))

    id_criteria = models.OneToOneField(Criteria,on_delete=models.CASCADE,null=False,blank=False,primary_key=False,verbose_name='criterio'
    ) #  id del criterio
    id_user = models.OneToOneField(User,on_delete=models.CASCADE,null=False,blank=False,primary_key=False,verbose_name='usuario'
    ) # id del docente
    professor_description = models.TextField(max_length=1000,null=False,blank=True,default=list,verbose_name='descripcion carrera docente',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres')]
    ) # descripcion de la carrera del docente
    status = models.PositiveSmallIntegerField(blank=False,default=2,choices=STATUS,verbose_name='estado registro',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # estado de registro de evaluaciones a docentes
    observations = ArrayField(models.TextField(max_length=1000,
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres'
    )]),null=False,blank=True,default=list,verbose_name='observaciones'
    ) # Observaciones del registro de evaluaciones a docentes

    class Meta:
        verbose_name = 'criterio 06: encuesta de evaluacion'
        verbose_name_plural = 'criterios > criterio 06 > encuestas de evaluacion'
        unique_together = ['id_criteria', 'id_user']

    def __str__(self): return f'{self.id_user.name} | {self.id_portfolio.year} | {self.id_portfolio.semester}'

class Professor_Internship(models.Model):
    '''Modelo de las pasantias del docente en la base de datos - Criterio 06'''
    SCHOOL = ((1,'Systems Engineering'),(2,'Others'))

    TEACHER_CATEGORY = ((1,'DOCENTE PRINCIPAL'),(2,'DOCENTE ASOCIADO'),(3,'DOCENTE AUXILIAR'))
    TEACHER_REGIMEN = ((1,'TIEMPO COMPLETO'),(2,'TIEMPO PARCIAL'))

    STATUS = ((1,'Aprobado'),(2,'Pendiente'),(3,'Rechazado'))

    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='criterio'
    ) # id del criterio
    id_user = models.ForeignKey(User,on_delete=models.PROTECT,null=True,blank=True,verbose_name='docente'
    ) # id del docente
    school = models.PositiveSmallIntegerField(blank=False,default=1,choices=SCHOOL,verbose_name='escuela profesional',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # escuela profesional a la que pertenece el docente
    teacher_category = models.PositiveSmallIntegerField(blank=False,default=1,choices=TEACHER_CATEGORY,verbose_name='categoria docente',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # Categoria del docente
    teacher_regimen = models.PositiveSmallIntegerField(blank=False,default=1,choices=TEACHER_REGIMEN,verbose_name='regimen docente',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # Regimen del docente
    id_internship = models.ForeignKey(Internship,on_delete=models.CASCADE,null=False,blank=True,verbose_name='pasantia'
    ) # id de las pasantia
    date_start = models.DateField(null=True,blank=True,verbose_name='fecha inicio pasantia'
    ) # fecha de inicio de la pasantia
    date_finish = models.DateField(null=True,blank=True,verbose_name='fecha fin pasantia'
    ) # fecha de fin de la pasantia
    status = models.PositiveSmallIntegerField(blank=False,default=2,choices=STATUS,verbose_name='estado registro',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # estado de registro de pasantia
    observations = ArrayField(models.TextField(max_length=1000,
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres'
    )]),null=False,blank=True,default=list,verbose_name='observaciones'
    ) # Observaciones del registro de pasantia

    class Meta:
        verbose_name = 'criterio 06: pasantia'
        verbose_name_plural = 'criterios > criterio 06 > pasantias'

    def __str__(self): return f'{self.id_user.name}'

class Professor_Training(models.Model):
    '''Modelo de Entrenamiento de docente en la base de datos - Criterio 06'''
    SCHOOL = ((1,'Systems Engineering'),(2,'Others'))
    TEACHER_CATEGORY = ((1,'DOCENTE PRINCIPAL'),(2,'DOCENTE ASOCIADO'),(3,'DOCENTE AUXILIAR'))
    TEACHER_REGIMEN = ((1,'TIEMPO COMPLETO'),(2,'TIEMPO PARCIAL'))

    STATUS = ((1,'Aprobado'),(2,'Pendiente'),(3,'Rechazado'))

    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='criterio')
    # id del criterio
    id_user = models.ForeignKey(User,on_delete=models.PROTECT,null=True,blank=True,verbose_name='docente')
    # id del docente del criterio
    school = models.PositiveSmallIntegerField(blank=False,default=1,choices=SCHOOL,verbose_name='escuela profesional',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # escuela profesional a la que pertenece el docente
    teacher_category = models.PositiveSmallIntegerField(blank=False,default=1,choices=TEACHER_CATEGORY,verbose_name='categoria docente',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # Categoria del docente
    teacher_regimen = models.PositiveSmallIntegerField(blank=False,default=1,choices=TEACHER_REGIMEN,verbose_name='regimen docente',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # Regimen del docente
    id_training_program = models.ForeignKey(Training_Program,on_delete=models.CASCADE,null=False,blank=False,verbose_name='programa entrenamiento')
    # id del programa de entrenamiento
    date_start = models.DateField(null=True,blank=True,verbose_name='fecha inicio entrenamiento')
     #fecha de inicio de entrenamiento
    date_finish = models.DateField(null=True,blank=True,verbose_name='fecha fin entrenamiento')
     #fecha de fin de entrenamiento
    theory_hours = models.PositiveSmallIntegerField(null=True,blank=False,default=0,verbose_name='horas teoria',
        validators = [MinValueValidator(0), MaxValueValidator(200)]
    ) # Horas de teoria
    practical_hours = models.PositiveSmallIntegerField(null=True,blank=False,default=0,verbose_name='horas practica',
        validators = [MinValueValidator(0), MaxValueValidator(200)]
    ) # Horas de practica
    credits = models.FloatField(null=False,blank=False,default=0,verbose_name='creditos',
        validators = [MinValueValidator(0), MaxValueValidator(10)]
    ) # Numero de creditos
    approval = models.BooleanField(blank=True,default=True,verbose_name='aprobacion')
    # Estado de aprobacion
    note = models.FloatField(null=True,blank=False,default=0,verbose_name='nota entrenamiento',
        validators = [MinValueValidator(0), MaxValueValidator(20)]
    ) # Nota del entrenamiento
    evidence = models.BooleanField(blank=True,default=False,verbose_name='evidencia entrenamiento')
    # Existencia de la Evidencia del entrenamiento
    status = models.PositiveSmallIntegerField(blank=False,default=2,choices=STATUS,verbose_name='estado registro',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # Estado de registro de entrenamiento
    observations = ArrayField(models.TextField(max_length=1000,
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres'
    )]),null=False,blank=True,default=list,verbose_name='observaciones'
    ) # Observaciones del registro de entrenamiento

    class Meta:
        verbose_name = 'criterio 06: entrenamiento'
        verbose_name_plural = 'criterios > criterio 06 > entrenamientos'

    def __str__(self): return f'{self.id_criteria.name} | {self.id_training_program.training_title}'

class Professor_Participation(models.Model):
    '''Modelo de Participacion en evento de docente en la base de datos - Criterio 06'''
    SCHOOL = ((1, 'Systems Engineering'),(2,'Others'))

    TEACHER_CATEGORY = ((1, 'DOCENTE PRINCIPAL'),(2,'DOCENTE ASOCIADO'),(3,'DOCENTE AUXILIAR'))
    TEACHER_REGIMEN = ((1, 'TIEMPO COMPLETO'),(2, 'TIEMPO PARCIAL'))

    ROL = ((1,'Speaker'),(2,'Exhibitor'),(3,'Professor'),(4,'Assitant'),(5,'Student'),(6,'Reviewer'),(7, 'Others'))
    AUTHOR_TYPE = ((1,'Author'),(2,'Coauthor'))
    SOURCE = ((1,'Department'),(2,'Professor'))

    STATUS = ((1,'Aprobado'),(2,'Pendiente'),(3,'Rechazado'))
    '''Modelo de los Eventos del criterio 6 en la base de datos'''
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=False,blank=False,verbose_name='criterio')
    # id del criterio
    id_user = models.ForeignKey(User,on_delete=models.PROTECT,null=False,blank=False,verbose_name='docente')
    # id del usuario responsable del criterio
    school = models.PositiveSmallIntegerField(blank=False,default=1,choices=SCHOOL,verbose_name='escuela profesional',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # escuela profesional a la que pertenece el docente
    teacher_category = models.PositiveSmallIntegerField(blank=False,default=1,choices=TEACHER_CATEGORY,verbose_name='categoria docente',
        validators = [MinValueValidator(1), MaxValueValidator(2)])
    # Categoria del docente
    teacher_regimen = models.PositiveSmallIntegerField(blank=False,default=1,choices=TEACHER_REGIMEN,verbose_name='regimen docente',
        validators = [MinValueValidator(1), MaxValueValidator(2)]
    ) # Regimen del docente
    id_event = models.ForeignKey(Event,on_delete=models.PROTECT,null=False,blank=False,verbose_name='evento')
    # id del evento
    article = models.BooleanField(blank=True,default=True,verbose_name='articulo')
    # Booleano que indica que si es un articulo
    id_article = models.ForeignKey(Article,on_delete=models.PROTECT,null=False,blank=False,verbose_name='nombre articulo')
    # id del articulo
    author_type = models.PositiveSmallIntegerField(blank=False,default=1,choices=AUTHOR_TYPE,verbose_name='tipo autoria',
        validators = [MinValueValidator(1), MaxValueValidator(2)])
    # Autor o co-autor del articulo
    rol = models.PositiveSmallIntegerField(blank=False,default=1,choices=ROL,verbose_name='rol participacion',
        validators = [MinValueValidator(1), MaxValueValidator(7)])
    # Rol de participación
    country = models.CharField(blank=False,max_length=50,verbose_name='pais',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 50 caracteres'
    )]) # Pais del evento
    city = models.CharField(blank=False,max_length=50,verbose_name='ciudad',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 50 caracteres'
    )]) # Ciudad del evento
    source = models.PositiveSmallIntegerField(blank=False,default=1,choices=SOURCE,verbose_name='fuente informacion',
        validators = [MinValueValidator(1), MaxValueValidator(2)]) 
    # Fuente de informacion
    evidence = models.BooleanField(blank=True,default=False,verbose_name='evidencia evento')
    # Existencia de la Evidencia de la participacion en evento
    status = models.PositiveSmallIntegerField(blank=False,default=2,choices=STATUS,verbose_name='estado registro',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # Estado de registro del evento
    #observation = models.TextField(blank=True,max_length=1000,verbose_name='observaciones',
    #    validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
    #        message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres'
    #)]) # Observacion del registro del evento
    observations = ArrayField(models.TextField(max_length=1000,
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres'
    )]),null=False,blank=True,default=list,verbose_name='observaciones'
    ) # Observaciones del registro del evento
    
    class Meta:
        verbose_name = 'criterio 6: participacion'
        verbose_name_plural = 'criterios > criterio 6 > participaciones'

    def __str__(self): return f'{self.id_user.name} | {self.event_title} | {self.kind}'


'''───────────────────────────MODELOS PARA EL MANEJO DE EVIDENCIAS DE DOCUMENTOS DE CRITERIO 06───────────────────────────'''

class Training_Doc_Evidence(models.Model):
    '''Modelo de documento de evidencia del entrenamiento en la base de datos'''
    id_training = models.ForeignKey(Professor_Training,on_delete=models.CASCADE,null=False,blank=False,verbose_name='entrenamiento')
    # id del registro del entrenamiento
    description = models.TextField(blank=True,max_length=200,verbose_name='descripcion',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,200}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 200 caracteres'
    )]) # Nombre del documento
    doc_evidence = models.FileField(null=False,blank=False,upload_to=TrainingEvidenceDocument_path,verbose_name='documento evidencia entrenamiento',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pdf','odt','jpg','png'],
        message = 'Solo se aceptan los siguientes formatos: docx,pdf,odt,jpg,png'
    )]) # Documento de evidencia del entrenamiento
    
    class Meta:
        verbose_name = 'documento evidencia entrenamiento'
        verbose_name_plural = 'entrenamientos > evidencias'

    def __str__(self): return f'{self.id_training.id_user.name} | {self.name}'
    def doc_evidence_filename(self): return get_filename(self.doc_evidence)

class Participation_Doc_Evidence(models.Model):
    '''Modelo de documento de evidencia del entrenamiento en la base de datos'''
    id_participation = models.ForeignKey(Professor_Participation,on_delete=models.CASCADE,null=False,blank=False,verbose_name='participacion')
    # id del registro de la participacion
    description = models.TextField(blank=True,max_length=200,verbose_name='descripcion',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,200}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 200 caracteres'
    )]) # Nombre del documento
    doc_evidence = models.FileField(null=False,blank=False,upload_to=ParticipationEvidenceDocument_path,verbose_name='documento evidencia entrenamiento',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pdf','odt','jpg','png'],
        message = 'Solo se aceptan los siguientes formatos: docx,pdf,odt,jpg,png'
    )]) # Documento de evidencia de la participacion en evento

    class Meta:
        verbose_name = 'documento evidencia entrenamiento'
        verbose_name_plural = 'entrenamientos > evidencias'

    def __str__(self): return f'{self.id_participation.id_user.name} | {self.name}'
    def doc_evidence_filename(self): return get_filename(self.doc_evidence)

class Recognition_Doc_Evidence(models.Model):
    '''Modelo de documento de reconocimiento del docente en la base de datos'''
    id_recognition = models.ForeignKey(Professor_Recognition,on_delete=models.CASCADE,null=False,blank=False,verbose_name='participacion')
    # id del registro de la participacion
    description = models.TextField(blank=True,max_length=200,verbose_name='descripcion',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,200}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 200 caracteres'
    )]) # Nombre del documento
    doc_evidence = models.FileField(null=False,blank=False,upload_to=RecognitionEvidenceDocument_path,verbose_name='documento evidencia entrenamiento',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pdf','odt','jpg','png'],
        message = 'Solo se aceptan los siguientes formatos: docx,pdf,odt,jpg,png'
    )]) # Documento de evidencia del reconocimiento de docente

    class Meta:
        verbose_name = 'documento evidencia entrenamiento'
        verbose_name_plural = 'entrenamientos > evidencias'

    def __str__(self): return f'{self.id_participation.id_user.name} | {self.name}'
    def doc_evidence_filename(self): return get_filename(self.doc_evidence)
