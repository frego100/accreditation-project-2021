from apps.user.apps import UserConfig
from apps.process.apps import ProcessConfig
from apps.portfolio.apps import PortfolioConfig
from apps.criteria_one.apps import CriteriaoneConfig
from apps.criteria_six.apps import CriteriaSixConfig 

class UserAppConfig(UserConfig):
    name = 'apps.user'
    verbose_name = 'usuario'

class ProcessAppConfig(ProcessConfig):
    name = 'apps.process'
    verbose_name = 'proceso'

class PortfolioAppConfig(PortfolioConfig):
    name = 'apps.portfolio'
    verbose_name = 'portafolio'

class CriteriaoneAppConfig(CriteriaoneConfig):
    name = 'apps.criteria_one'
    verbose_name = 'criteria_one'

class CriteriaSixAppConfig(CriteriaSixConfig):
    name = 'apps.criteria_six'
    verbose_name = 'criteria_six'
