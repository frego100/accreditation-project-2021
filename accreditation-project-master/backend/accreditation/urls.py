from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

api_urls = [
    path('user/', include('apps.user.urls',)),
    path('process/', include('apps.process.urls',)),
    path('process/portfolio/', include('apps.portfolio.urls',)),
    path('process/criteria_one/', include('apps.criteria_one.urls',)),
    path('process/criteria_six/', include('apps.criteria_six.urls',)),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(api_urls)),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)