# BACKEND

## Instalación

### Linux

Instalación de paquetes necesarios  en Linux.

```bash
$ sudo apt install build-essential
$ sudo apt install python3 python3-pip
$ sudo apt install postgresql postgresql-client postgresql-contrib
$ sudo apt install libpq-dev
```

Base de datos Postgresql con la configuración que usted desea:

```python
DEBUG = True
ALLOWED_HOSTS = ['*']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'NOMBRE_DE_LA_BASE_DE_DATOS',
        'USER': 'NOMBRE_DEL_USUARIO',
        'PASSWORD': 'CONTRASEÑA',
        'HOST': 'localhost',
        'PORT': 5432,
    }
}
```

Es recomendable desplegar el proyecto en un entorno virtual: [Virtualenv](https://virtualenv.pypa.io/en/latest/ "Entorno Virtuales de python")

Pasos para instalar las dependencia y las migraciones.

```shell
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
```

