export const DataResearcher = {
  data_name:"La invertigacion de Instructor",
  data_name_two:"Invertigacion de Instructor",
  renacyt_group: [
      { value: 1, text: "SMaría Rostworowski" },
      { value: 2, text: "Carlos Monge Medrano" },
    ],
    renacyt_group_level: [
    { value: 1, text: "I" },
    { value: 2, text: "II" },
    { value: 3, text: "III" },
    { value: 4, text: "IV" },
  ],

  status: [
    { value: "1", text: "Aprobado" },
    { value: "2", text: "Pendiente" },
    { value: "3", text: "Rechazado" },
  ],
}
export const ModelResearcher = {
  "id": "",
  "id_criteria": 6,
  "id_user": null,
  "renacyt_code": "",
  "current_organizations": [],
  "validity_date_start": "",
  "validity_date_finish": "",
  "renacyt_group": null,
  "renacyt_group_level": null,
  "status": 2,
  "observations": []
}
