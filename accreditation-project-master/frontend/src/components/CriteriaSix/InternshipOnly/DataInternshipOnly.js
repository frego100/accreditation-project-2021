export const ModelInternshipOnly = {
  "id": "",
  "title": "",
  "internship_type": null,
  "place": "",
  "country": null
}
export const DataInternshipOnly = {
  data_name:"La organización",
  data_name_two:"Organización",
  countries: [
    { value: 1, text: "Peru" },
    { value: 2, text: "Otro" },
  ],
  type: [
    { value: 1, text: "Local" },
    { value: 2, text: "National" },
    { value: 3, text: "International" },
  ],
}
