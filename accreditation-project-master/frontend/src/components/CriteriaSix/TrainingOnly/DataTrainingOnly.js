export const ModelTrainingOnly = {
  "id":"",
  "id_organization": null,
  "training_kind": null,
  "training_title": "",
  "training_sub_type": null,
  "country": null,
  "city": null,
  "training_category": null
}
export const DataTrainingOnly = {
  data_name:"La organización",
  data_name_two:"Organización",

  kinds: [
    { value: 1, text: "Training in Teaching" },
    { value: 2, text: "Training in Speciality" },
    { value: 3, text: "Training in Research" },
    { value: 4, text: "Others" },
  ],
  subTypes: [
    { value: 1, text: "Course" },
    { value: 2, text: "Workshop" },
    { value: 3, text: "Seminar" },
    { value: 4, text: "Course Workshop" },
    { value: 5, text: "Conference" },
    { value: 6, text: "Others" },
  ],
  eventCategories: [
    { value: 1, text: "National" },
    { value: 2, text: "International" },
  ],
  countries: [
    { value: 1, text: "Peru" },
    { value: 2, text: "Otro" },
  ],
  cities: [
    { value: 1, text: "Arequipa" },
    { value: 2, text: "Otro" },
  ],
}
