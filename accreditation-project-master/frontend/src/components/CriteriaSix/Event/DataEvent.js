export const ModelEvent = {
  "id": "",
  "name": "",
  "event_kind": null,
  "event_category": null,
  "date_start": "", 
  "date_finish": ""
}
export const DataEvent = {
  data_name:"El evento",
  data_name_two:"Evento",
  event_kind:[
    { value: 1, text: "Congress" },
    { value: 2, text: "Symposium" },
    { value: 3, text: "Seminar" },
    { value: 4, text: "Course" },
    { value: 5, text: "Conference" },
    { value: 6, text: "Internships" },
    { value: 7, text: "Journal / Magazine" },
    { value: 8, text: "Visitor to Center" },
    { value: 9, text: "Others" },
    { value: 10, text: "Editorial" },
    { value: 11, text: "Journeys" },
  ],
  event_category:[
    { value: 1, text: "National" },
    { value: 2, text: "International" },
  ]
}
