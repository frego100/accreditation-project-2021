export const DataParticipation = {
  data_name:"El Entrenamiento",
  data_name_two:"Entrenamiento",
  schools: [
      { value: 1, text: "System Enginnering" },
      { value: 2, text: "Other" },
    ],
    categories: [
      { value: 1, text: "AS" },
      { value: 2, text: "AUX" },
      { value: 2, text: "Prueba" },
    ],
    registers: [
      { value: 1, text: "TC" },
      { value: 2, text: "TP" },
    ],
    
    kinds: [
      { value: 1, text: "Participation in Teaching" },
      { value: 2, text: "Participation in Speciality" },
      { value: 3, text: "Participation in Research" },
      { value: 4, text: "Others" },
    ],
    subTypes: [
      { value: 1, text: "Course" },
      { value: 2, text: "Workshop" },
      { value: 3, text: "Seminar" },
      { value: 4, text: "Course Workshop" },
      { value: 5, text: "Conference" },
      { value: 6, text: "Others" },
    ],
    eventCategories: [
      { value: 1, text: "National" },
      { value: 2, text: "International" },
    ],
    countries: [
      { value: 1, text: "Peru" },
      { value: 2, text: "Otro" },
    ],
    cities: [
      { value: 1, text: "Arequipa" },
      { value: 2, text: "Otro" },
    ],
    status: [
      { value: "1", text: "Aprobado" },
      { value: "2", text: "Pendiente" },
      { value: "3", text: "Rechazado" },
    ],
    source: [
      { value: "1", text: "Department" },
      { value: "2", text: "Professor" },
    ],
    rol: [
      { value: "1", text: "Speaker" },
      { value: "2", text: "Exhibitor" },
      { value: "3", text: "Professor" },
      { value: "4", text: "Assitant" },
      { value: "5", text: "Student" },
      { value: "6", text: "Reviewer" },
      { value: "7", text: "Others" },
    ],
    author_type:[
      { value: "1", text: "Author" },
      { value: "2", text: "Coauthor" },
    ]
}
export const ModelParticipation = {
  "school": null,
  "teacher_category": null,
  "teacher_regimen": null,
  "article": false,
  "author_type": null,
  "rol": null,
  "country": null,
  "city": null,
  "source": null,
  "evidence": false,
  "status": 2,
  "observations": [],
  "id_criteria": 6,
  "id_user": null,
  "id_event": null,
  "id_article": null
}
