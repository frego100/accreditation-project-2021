export const DataResearch = {
  data_name:"La investigación del instructor",
  data_name_two:"Investigación del instructor",
  schools: [
    { value: 1, text: "System Enginnering" },
    { value: 2, text: "Other" },
  ],
    participation: [
      { value: 1, text: "Principal Investigator" },
      { value: 2, text: "Co-Investigator" },
    ],
    categories: [
      { value: 1, text: "AS" },
      { value: 2, text: "AUX" },

    ],
    registers: [
      { value: 1, text: "TC" },
      { value: 2, text: "TP" },
    ],
    
    kinds: [
      { value: 1, text: "Training in Teaching" },
      { value: 2, text: "Training in Speciality" },
      { value: 3, text: "Training in Research" },
      { value: 4, text: "Others" },
    ],
    subTypes: [
      { value: 1, text: "Course" },
      { value: 2, text: "Workshop" },
      { value: 3, text: "Seminar" },
      { value: 4, text: "Course Workshop" },
      { value: 5, text: "Conference" },
      { value: 6, text: "Others" },
    ],
    eventCategories: [
      { value: 1, text: "National" },
      { value: 2, text: "International" },
    ],
    countries: [
      { value: "Peru", text: "Peru" },
      { value: "Otro", text: "Otro" },
    ],
    cities: [
      { value: "Arequipa", text: "Arequipa" },
      { value: "Otro", text: "Otro" },
    ],
    status: [
      { value: "1", text: "Aprobado" },
      { value: "2", text: "Pendiente" },
      { value: "3", text: "Rechazado" },
    ],
}
export const ModelResearch = {
    "id": "",
    "school": null,
    "teacher_category": null,
    "teacher_regimen": null,
    "participation_type": null,
    "status": 2,
    "observations": [],
    "id_criteria": 6,
    "id_user": null,
    "id_research": null
}

