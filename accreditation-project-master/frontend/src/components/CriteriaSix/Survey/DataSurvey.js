export const DataSurvey = {
  data_name:"La encuesta",
  data_name_two:"Encuesta ",
  schools: [
      { value: 1, text: "System Enginnering" },
      { value: 2, text: "Other" },
    ],
    categories: [
      { value: 1, text: "AS" },
      { value: 2, text: "AUX" },
      { value: 2, text: "Prueba" },
    ],
    registers: [
      { value: 1, text: "TC" },
      { value: 2, text: "TP" },
    ],
    
    kinds: [
      { value: 1, text: "Survey in Teaching" },
      { value: 2, text: "Survey in Speciality" },
      { value: 3, text: "Survey in Research" },
      { value: 4, text: "Others" },
    ],
    subTypes: [
      { value: 1, text: "Course" },
      { value: 2, text: "Workshop" },
      { value: 3, text: "Seminar" },
      { value: 4, text: "Course Workshop" },
      { value: 5, text: "Conference" },
      { value: 6, text: "Others" },
    ],
    eventCategories: [
      { value: 1, text: "National" },
      { value: 2, text: "International" },
    ],
    countries: [
      { value: 1, text: "Peru" },
      { value: 2, text: "Otro" },
    ],
    cities: [
      { value: 1, text: "Arequipa" },
      { value: 2, text: "Otro" },
    ],
    status: [
      { value: "1", text: "Aprobado" },
      { value: "2", text: "Pendiente" },
      { value: "3", text: "Rechazado" },
    ],

    portfolios: [
      { value: "1", text: "A" },
      { value: "2", text: "B" },
      { value: "3", text: "C" },
    ],
}
export const ModelSurvey = {
  "id": "",
  "evaluation_note": 0,
  "status": 2,
  "observations": [],
  "id_criteria": 6,
  "id_user": null,
  "id_portfolio": null
}
