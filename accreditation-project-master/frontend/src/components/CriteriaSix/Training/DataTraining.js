export const DataTraining = {
  data_name:"El Formacion Docente",
  data_name_two:"Formacion Docente",
  schools: [
      { value: 1, text: "System Enginnering" },
      { value: 2, text: "Other" },
    ],
    categories: [
      { value: 1, text: "AS" },
      { value: 2, text: "AUX" },
      { value: 2, text: "Prueba" },
    ],
    registers: [
      { value: 1, text: "TC" },
      { value: 2, text: "TP" },
    ],
    
    kinds: [
      { value: 1, text: "Training in Teaching" },
      { value: 2, text: "Training in Speciality" },
      { value: 3, text: "Training in Research" },
      { value: 4, text: "Others" },
    ],
    subTypes: [
      { value: 1, text: "Course" },
      { value: 2, text: "Workshop" },
      { value: 3, text: "Seminar" },
      { value: 4, text: "Course Workshop" },
      { value: 5, text: "Conference" },
      { value: 6, text: "Others" },
    ],
    eventCategories: [
      { value: 1, text: "National" },
      { value: 2, text: "International" },
    ],
    countries: [
      { value: 1, text: "Peru" },
      { value: 2, text: "Otro" },
    ],
    cities: [
      { value: 1, text: "Arequipa" },
      { value: 2, text: "Otro" },
    ],
    status: [
      { value: "1", text: "Aprobado", icon:"check-circle", variant: "success"},
      { value: "2", text: "Pendiente", icon:"exclamation-circle", variant: "warning"},
      { value: "3", text: "Rechazado", icon:"x-circle", variant: "danger"},
    ],
}
export const ModelTraining = {
  "id": "",
  "school": null,
  "teacher_category": null,
  "teacher_regimen": null,
  "date_start": "",
  "date_finish": "",
  "theory_hours": "",
  "practical_hours": "",
  "credits": "",
  "approval": false,
  "note": "",
  "evidence": false,
  "status": 2,
  "observations": [],
  "id_criteria": 6,
  "id_user": null,
  "id_training_program": null

}
