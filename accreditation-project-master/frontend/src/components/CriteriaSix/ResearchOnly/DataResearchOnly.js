export const ModelResearchOnly = {
  "id": "",
  "title": "",
  "research_type": null,
  "contract": "",
  "research_line": null,
  "date_start": "",
  "date_finish": "",
  "closed": false
}
export const DataResearchOnly = {
  data_name:"La investigación",
  data_name_two:"Investigación",
  research_type:[
    { value: 1, text: "Basic" },
    { value: 2, text: "Applied" },
    { value: 2, text: "Formative" },
  ],
  research_line:[
    { value: 1, text: "Computational Intelligence" },
    { value: 1, text: "Software Engineering" },
    { value: 1, text: "Computational Vision" },
    { value: 1, text: "Leng Programac and SW Development" },
    { value: 1, text: "Media and Knowledge" },
    { value: 1, text: "Applied Information Technology" },
    { value: 2, text: "Other" },
  ]
}
