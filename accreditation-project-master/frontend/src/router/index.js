import Vue from "vue";
import VueRouter from "vue-router";

import Home from "@/components/Home";
import EditIndicator from "@/components/Indicator/EditIndicator";
import EditCriteria from "@/components/Criteria/EditCriteria";
import DetailCriteria from "@/components/Criteria/DetailCriteria";
import ViewCriteria from "@/components/Criteria/ViewCriteria";
import ListCriteria from "@/components/Criteria/ListCriteria";
/*import EditPhase from "@/components/Phase/EditPhase";
import ViewPhase from "@/components/Phase/ViewPhase";
import DetailPhase from "@/components/Phase/DetailPhase";
*/
import EditProcess from "@/components/Process/EditProcess";
import ListProcess from "@/components/Process/ListProcess";
import NewProcess from "@/components/Process/NewProcess";
import ViewProcess from "@/components/Process/ViewProcess";
import DetailProcess from "@/components/Process/DetailProcess";
import LoginUser from "@/components/Sesions/LoginUser";
import Recover from "@/components/Sesions/Recover";
import ResetPassword from "@/components/Sesions/ResetPassword";
import DeleteUser from "@/components/User/DeleteUser";
import DeactivateUser from "@/components/User/DeactivateUser";
import ActivateUser from "@/components/User/ActivateUser";
import EditUser from "@/components/User/EditUser";
import ListUser from "@/components/User/ListUser";
import PerfilUser from "@/components/User/PerfilUser";
import ViewUsers from "@/components/User/ViewUsers";
import RegisterUser from "@/components/User/RegisterUser";
import ProfileUser from "@/components/ProfileUser/ProfileUser";
import ViewProfileUser from "@/components/ProfileUser/ViewProfileUser";
import EditProfileUser from "@/components/ProfileUser/EditProfileUser";
import EditPermissions from "@/components/Roles/EditPermissions";
import ViewRoles from "@/components/Roles/ViewRoles";

import ViewStudyPlan from "@/components/StudyPlan/ViewStudyPlan";
import ListStudyPlan from "@/components/StudyPlan/ListStudyPlan";
import NewStudyPlan from "@/components/StudyPlan/NewStudyPlan";
import DetailStudyPlan from "@/components/StudyPlan/DetailStudyPlan";
import EditStudyPlan from "@/components/StudyPlan/EditStudyPlan";

import ViewCompetence from "@/components/Competence/ViewCompetence";
import ListCompetence from "@/components/Competence/ListCompetence";
import NewCompetence from "@/components/Competence/NewCompetence";
import DetailCompetence from "@/components/Competence/DetailCompetence";
import EditCompetence from "@/components/Competence/EditCompetence";

import ListFormationArea from "@/components/FormationArea/ListFormationArea";
import ViewFormationArea from "@/components/FormationArea/ViewFormationArea";
import NewFormationArea from "@/components/FormationArea/NewFormationArea";
import DetailFormationArea from "@/components/FormationArea/DetailFormationArea";
import EditFormationArea from "@/components/FormationArea/EditFormationArea";


import ListStudentResult from "@/components/StudentResult/ListStudentResult";
import ViewStudentResult from "@/components/StudentResult/ViewStudentResult";
import NewStudentResult from "@/components/StudentResult/NewStudentResult";
import DetailStudentResult from "@/components/StudentResult/DetailStudentResult";
import EditStudentResult from "@/components/StudentResult/EditStudentResult";

import ViewCurriculum from "@/components/Curriculum/ViewCurriculum";
import ListCCurse from "@/components/Curriculum/ListCCurse"
import AddCCurse from "@/components/Curriculum/AddCCurse"
import DetailCCurse from "@/components/Curriculum/DetailCCurse"
import EditCCurse from "@/components/Curriculum/EditCCurse"

import ListSommeliers from "@/components/Sommeliers/ListSommeliers";
import ViewSommeliers from "@/components/Sommeliers/ViewSommeliers";
import NewSommeliers from "@/components/Sommeliers/NewSommeliers";

import ViewMatchCourseVsResults from "@/components/MatchCourseVsResults/ViewMatchCourseVsResults";
import ViewMatchCourseVsCompetence from "@/components/MatchCourseVsCompetence/ViewMatchCourseVsCompetence";
import ViewMatchCompetencesVsResults from "@/components/MatchCompetencesVsResults/ViewMatchCompetencesVsResults";

import ViewCurriculumVitae from "@/components/CurriculumVitae/ViewCurriculumVitae";
import AdminViewCurriculumVitae from "@/components/CurriculumVitae/AdminViewCurriculumVitae";

import ViewSilaboABET from "@/components/Silabo/ViewSilaboABET";

import ViewSilaboDufa from "@/components/SilaboDufa/ViewSilaboDufa";
import ViewEvaluacionSilaboDufa from "@/components/SilaboDufa/ViewEvaluacionSilaboDufa";
import EvaluationAcademicPeriod from "@/components/SilaboDufa/EvaluationAcademicPeriod";
import DeterminarRubricasEvaluacion from "@/components/SilaboDufa/DeterminarRubricasEvaluacion";
import DeterminarCompetencias from "@/components/SilaboDufa/DeterminarCompetencias";
import ViewCompetenciaSilaboDufa from "@/components/SilaboDufa/ViewCompetenciaSilaboDufa";
import ViewMedResultSilaboDufa from "@/components/SilaboDufa/ViewMedResultSilaboDufa";
import DeterminarCriterios from "@/components/SilaboDufa/DeterminarCriterios";
import SilaboDufa from "@/components/SilaboDufa/SilaboDufa";

import ExamMenu from "@/components/EntranceExamination/ExamMenu";
import Exam from "@/components/EntranceExamination/Exam";
import Solution from "@/components/EntranceExamination/Solution";
import Report from "@/components/EntranceExamination/Report";


import Fperiod from "@/components/AcademicWork/Fperiod";
import EvaluationReport from "@/components/AcademicWork/EvaluationReport";
import EvaluationReportPDF from "@/components/AcademicWork/EvaluationReportPDF";
import ExamEvidences from "@/components/AcademicWork/ExamEvidences";
import ContinuosEvidences from "@/components/AcademicWork/ContinuosEvidences";
import CourseEvidences from "@/components/AcademicWork/CourseEvidences";

import Speriod from "@/components/AcademicWork/Speriod";
import EvaluationReportSecond from "@/components/AcademicWork/EvaluationReportSecond";
import EvaluationReportSecondPDF from "@/components/AcademicWork/EvaluationReportSecondPDF";
import ExamEvidencesSecond from "@/components/AcademicWork/ExamEvidencesSecond";
import ContinuosEvidencesSecond from "@/components/AcademicWork/ContinuosEvidencesSecond";
import CourseEvidencesSecond from "@/components/AcademicWork/CourseEvidencesSecond";

import Tperiod from "@/components/AcademicWork/Tperiod";
import EvaluationReportThird from "@/components/AcademicWork/EvaluationReportThird";
import EvaluationReportThirdPDF from "@/components/AcademicWork/EvaluationReportThirdPDF";
import ExamEvidencesThird from "@/components/AcademicWork/ExamEvidencesThird";
import ContinuosEvidencesThird from "@/components/AcademicWork/ContinuosEvidencesThird";
import CourseEvidencesThird from "@/components/AcademicWork/CourseEvidencesThird";


import ViewCourses from "@/components/Courses/ViewCourses";
import ListCourses from "@/components/Courses/ListCourses";
import AddCourses from "@/components/Courses/AddCourses";
import EditCourses from "@/components/Courses/EditCourses";
import DeleteCourses from "@/components/Courses/DeleteCourses";
import DeactivateCourse from "@/components/Courses/DeactivateCourse";
import ActivateCourse from "@/components/Courses/ActivateCourse";

import ViewStages from "@/components/Courses/ViewStages";
import Stages from "@/components/Courses/Stages";
import EditStages from "@/components/Courses/EditStages";

import ViewDocentes from "@/components/Courses/ViewDocentes";
import ResponsableCourses from "@/components/Courses/ResponsableCourses";
import EditResponsable from "@/components/Courses/EditResponsable";
import DeleteResponsable from "@/components/Courses/DeleteResponsable";


import AddPortfolio from "@/components/Portfolio/AddPortfolio";
import ListPortfolios from "@/components/Portfolio/ListPortfolios";
import EditPortfolio from "@/components/Portfolio/EditPortfolio";
import DeletePortfolio from "@/components/Portfolio/DeletePortfolio";
import DeactivatePortfolio from "@/components/Portfolio/DeactivatePortfolio";
import ViewPortfolio from "@/components/Portfolio/ViewPortfolio";

import ListSyllabus from "@/components/Courses/ListSyllabus";

import Reports from "@/components/Reports/Reports";

import ViewReportContinuousImprovement from "@/components/ReportContinuousImprovement/ViewReportContinuousImprovement";
import DetailReportContinuousImprovement from "@/components/ReportContinuousImprovement/DetailReportContinuousImprovement";

import ViewReportAcademicAdvisory from "@/components/ReportAcademicAdvisory/ViewReportAcademicAdvisory";
import DetailReportAcademicAdvisory from "@/components/ReportAcademicAdvisory/DetailReportAcademicAdvisory";

import ListStudents from "@/components/Student/ListStudents"
import ViewStudent from "@/components/Student/ViewStudent"

import EditAcademicDevelopment from "@/components/ReportAcademicDevelopment/EditAcademicDevelopment";
import ListAcademicDevelopment from "@/components/ReportAcademicDevelopment/ListAcademicDevelopment";
import NewAcademicDevelopment from "@/components/ReportAcademicDevelopment/NewAcademicDevelopment";
import ViewAcademicDevelopment from "@/components/ReportAcademicDevelopment/ViewAcademicDevelopment";

import ViewReportPerformance from "@/components/ReportPerformance/ViewReportPerformance"
import ReportPerformance from "@/components/ReportPerformance/ReportPerformance"

import ViewReportAverage from "@/components/ReportAverage/ViewReportAverage";
import ListReportAverage from "@/components/ReportAverage/ListReportAverage";

import ViewReportStudentResult from "@/components/ReportStudentResult/ViewReportStudentResult"
import FormReportStudentResult from "@/components/ReportStudentResult/FormReportStudentResult"

import ViewCriteriaOne from "@/components/CriteriaOne/ViewCriteriaOne"

import ViewAcademicFriday from "@/components/CriteriaOne/AcademicFridays/ViewAcademicFridays"
import listAcademicFriday from "@/components/CriteriaOne/AcademicFridays/ListAcademicFridays"
import addAcademicFridays from "@/components/CriteriaOne/AcademicFridays/addAcademicFridays";


import ViewStudentEvaluation from "@/components/CriteriaOne/StudentEvaluation/ViewStudentEvaluation"
import listStudentEvaluation from "@/components/CriteriaOne/StudentEvaluation/ListStudentEvaluation"
import addStudentEvaluation from "@/components/CriteriaOne/StudentEvaluation/addStudentEvaluation";

import ViewBachellorDegree from "@/components/CriteriaOne/BachellorDegreeVerificationProcess/ViewBachellorDegreeVerificationProcess"
import listBachellorDegree from "@/components/CriteriaOne/BachellorDegreeVerificationProcess/ListBachellorDegreeVerificationProcess"
import addBachellorDegree from "@/components/CriteriaOne/BachellorDegreeVerificationProcess/addBachellorDegreeVerificationProcess";

import ViewRegulation from "@/components/CriteriaOne/Regulation/ViewRegulation"
import listRegulation from "@/components/CriteriaOne/Regulation/ListRegulation"
import addRegulation from "@/components/CriteriaOne/Regulation/addRegulation";

import ViewStudentMobility from "@/components/CriteriaOne/StudentMobility/ViewStudentMobility"
import lisStudentMobility from "@/components/CriteriaOne/StudentMobility/ListStudentMobility"
import addStudentMobility from "@/components/CriteriaOne/StudentMobility/addStudentMobility";

import ViewUnsaAnnualMemories from "@/components/CriteriaOne/UnsaAnnualMemories/ViewUnsaAnnualMemories"
import lisUnsaAnnualMemories from "@/components/CriteriaOne/UnsaAnnualMemories/ListUnsaAnnualMemories"
import addUnsaAnnualMemories from "@/components/CriteriaOne/UnsaAnnualMemories/addUnsaAnnualMemories";

import ViewInternationalAgreements from "@/components/CriteriaOne/InternationalAgreements/ViewInternationalAgreements"
import listInternationalAgreements from "@/components/CriteriaOne/InternationalAgreements/ListInternationalAgreements"
import addInternationalAgreements from "@/components/CriteriaOne/InternationalAgreements/addInternationalAgreements";

import ViewGraduatedVerificationProcess from "@/components/CriteriaOne/GraduatedVerificationProcess/ViewGraduatedVerificationProcess"
import listGraduatedVerificationProcess from "@/components/CriteriaOne/GraduatedVerificationProcess/ListGraduatedVerificationProcess"
import addGraduatedVerificationProcess from "@/components/CriteriaOne/GraduatedVerificationProcess/addGraduatedVerificationProcess";

import ViewProfessionalEngineerVerificationProcess from "@/components/CriteriaOne/ProfessionalEngineerVerificationProcess/ViewProfessionalEngineerVerificationProcess"
import listProfessionalEngineerVerificationProcess from "@/components/CriteriaOne/ProfessionalEngineerVerificationProcess/ListProfessionalEngineerVerificationProcess"
import addProfessionalEngineerVerificationProcess from "@/components/CriteriaOne/ProfessionalEngineerVerificationProcess/addProfessionalEngineerVerificationProcess";

import ViewPreProfessionalPractices from "@/components/CriteriaOne/PreProfessionalPractices/ViewPreProfessionalPractices"
import listPreProfessionalPractices from "@/components/CriteriaOne/PreProfessionalPractices/ListPreProfessionalPractices"
import addPreProfessionalPractices from "@/components/CriteriaOne/PreProfessionalPractices/addPreProfessionalPractices";

import ViewTransferStudent from "@/components/CriteriaOne/TransferStudent/ViewTransferStudent"
import listTransferStudent from "@/components/CriteriaOne/TransferStudent/ListTransferStudent"
import addTransferStudent from "@/components/CriteriaOne/TransferStudent/addTransferStudent";

import ViewCriteriaSix from "@/components/CriteriaSix/ViewCriteriaSix"

import ListParticipation from "@/components/CriteriaSix/Participation/ListParticipation"
import ViewParticipation from "@/components/CriteriaSix/Participation/ViewParticipation"

import ListTraining from "@/components/CriteriaSix/Training/ListTraining"
import FullListTraining from "@/components/CriteriaSix/Training/FullListTraining"
import ViewTraining from "@/components/CriteriaSix/Training/ViewTraining"

import NotFound  from "@/components/Error/NotFound"

import ListResearch from "@/components/CriteriaSix/Research/ListResearch"
import ViewResearch from "@/components/CriteriaSix/Research/ViewResearch"

import ListResearcher from "@/components/CriteriaSix/Researcher/ListResearcher"
import ViewResearcher from "@/components/CriteriaSix/Researcher/ViewResearcher"

import ListQualification from "@/components/CriteriaSix/Qualification/ListQualification"
import ViewQualification from "@/components/CriteriaSix/Qualification/ViewQualification"

import ListInternship from "@/components/CriteriaSix/Internship/ListInternship"
import ViewInternship from "@/components/CriteriaSix/Internship/ViewInternship"

import ListWorkload from "@/components/CriteriaSix/Workload/ListWorkload"
import ViewWorkload from "@/components/CriteriaSix/Workload/ViewWorkload"

import ListSurvey from "@/components/CriteriaSix/Survey/ListSurvey"
import ViewSurvey from "@/components/CriteriaSix/Survey/ViewSurvey"

Vue.use(VueRouter);

const routes = [{
        path: "",
        name: "LoginUser",
        component: LoginUser,
    },
    {
        path: "*",
        name: "LoginUser",
        component: NotFound,
    },
    {
        path: "/",
        name: "Home",
        component: Home,
        children: [
            /*{
              path: "",
              name: "ControlPanel",
              component: ControlPanel,
            },*/
            {
                path: "process/criteria/:criteriaId",
                name: "CriteriaOne",
                component: ViewCriteriaOne,
            },
            {
                path: "process/criteria/:criteriaId",
                name: "CriteriaSix",
                component: ViewCriteriaSix,

            },
            {
                path: "process/criteria/:criteriaId/AcademicFridays",
                name: "AcademicFridays",
                component: ViewAcademicFriday,
                children: [{
                        path: "list",
                        name: "listAcademicFriday",
                        component: listAcademicFriday
                    },
                    {
                        path: "add",
                        name: "addAcademicFridays",
                        component: addAcademicFridays
                    },

                ],
            },
            //-------------
            {
                path: "process/criteria/:criteriaId/participation",
                name: "ViewParticipation",
                component: ViewParticipation,
                children: [{
                        path: "list/:status",
                        name: "ListParticipation",
                        component: ListParticipation
                    },

                ],
            },
            //-------------
            {
                path: "process/criteria/:criteriaId/BachellorDegree",
                name: "BachellorDegree",
                component: ViewBachellorDegree,
                children: [{
                        path: "list",
                        name: "listBachellorDegree",
                        component: listBachellorDegree
                    },
                    {
                        path: "add",
                        name: "addBachellorDegree",
                        component: addBachellorDegree
                    },

                ],
            },
            //-------------
            {
                path: "process/criteria/:criteriaId/BachellorDegreeVerificationProcess",
                name: "StudentEvaluation",
                component: ViewStudentEvaluation,
                children: [{
                        path: "list",
                        name: "listStudentEvaluation",
                        component: listStudentEvaluation
                    },
                    {
                        path: "add",
                        name: "addStudentEvaluation",
                        component: addStudentEvaluation
                    },

                ],
            },
            //-------------
            {
                path: "process/criteria/:criteriaId/regulation",
                name: "Regulation",
                component: ViewRegulation,
                children: [{
                        path: "list",
                        name: "listRegulation",
                        component: listRegulation
                    },
                    {
                        path: "add",
                        name: "addRegulation",
                        component: addRegulation
                    },

                ],
            },
            //-------------
            {
                path: "process/criteria/:criteriaId/studentMobility",
                name: "StudentMobility",
                component: ViewStudentMobility,
                children: [{
                        path: "list",
                        name: "listStudentMobility",
                        component: lisStudentMobility
                    },
                    {
                        path: "add",
                        name: "addStudentMobility",
                        component: addStudentMobility
                    },

                ],
            },
            //-------------
            {
                path: "process/criteria/:criteriaId/unsaAnnualMemories",
                name: "UnsaAnnualMemories",
                component: ViewUnsaAnnualMemories,
                children: [{
                        path: "list",
                        name: "listUnsaAnnualMemories",
                        component: lisUnsaAnnualMemories
                    },
                    {
                        path: "add",
                        name: "addUnsaAnnualMemories",
                        component: addUnsaAnnualMemories
                    },
                    {
                        path: "fulllist/:status",
                        name: "FullListTraining",
                        component: FullListTraining
                    },

                ],
            },

            {
                path: "process/criteria/:criteriaId/research",
                name: "ViewResearch",
                component: ViewResearch,
                children: [{
                        path: "list/:status",
                        name: "ListResearch",
                        component: ListResearch
                    },

                ],
            },

            {
                path: "process/criteria/:criteriaId/researcher",
                name: "ViewResearcher",
                component: ViewResearcher,
                children: [{
                        path: "list/:status",
                        name: "ListResearcher",
                        component: ListResearcher
                    },

                ],
            },
            {
                path: "process/criteria/:criteriaId/qualification",
                name: "ViewQualification",
                component: ViewQualification,
                children: [{
                        path: "list/:status",
                        name: "ListQualification",
                        component: ListQualification
                    },

                ],
            },

            {
                path: "process/criteria/:criteriaId/internship",
                name: "ViewInternship",
                component: ViewInternship,
                children: [{
                        path: "list/:status",
                        name: "ListInternship",
                        component: ListInternship
                    },

                ],
            },

            {
                path: "process/criteria/:criteriaId/workload",
                name: "ViewWorkload",
                component: ViewWorkload,
                children: [{
                        path: "list/:status",
                        name: "ListWorkload",
                        component: ListWorkload
                    },

                ],
            },

            {
                path: "process/criteria/:criteriaId/survey",
                name: "ViewSurvey",
                component: ViewSurvey,
                children: [{
                        path: "list/:status",
                        name: "ListSurvey",
                        component: ListSurvey
                    },

                ],
            },
            //-------------
            {
                path: "process/criteria/:criteriaId/InternationalAgreements",
                name: "InternationalAgreements",
                component: ViewInternationalAgreements,
                children: [{
                        path: "list",
                        name: "listInternationalAgreements",
                        component: listInternationalAgreements
                    },
                    {
                        path: "add",
                        name: "addInternationalAgreements",
                        component: addInternationalAgreements
                    },

                ],
            },
            //------------- 
            {
                path: "process/criteria/:criteriaId/GraduatedVerificationProcess",
                name: "GraduatedVerificationProcess",
                component: ViewGraduatedVerificationProcess,
                children: [{
                        path: "list",
                        name: "listGraduatedVerificationProcess",
                        component: listGraduatedVerificationProcess
                    },
                    {
                        path: "add",
                        name: "addGraduatedVerificationProcess",
                        component: addGraduatedVerificationProcess
                    },

                ],
            },
            //------------- 
            {
                path: "process/criteria/:criteriaId/ProfessionalEngineerVerificationProcess",
                name: "ProfessionalEngineerVerificationProcess",
                component: ViewProfessionalEngineerVerificationProcess,
                children: [{
                        path: "list",
                        name: "listProfessionalEngineerVerificationProcess",
                        component: listProfessionalEngineerVerificationProcess
                    },
                    {
                        path: "add",
                        name: "addProfessionalEngineerVerificationProcess",
                        component: addProfessionalEngineerVerificationProcess
                    },

                ],
            },
            //------------- 
            {
                path: "process/criteria/:criteriaId/preProfessionalPractices",
                name: "PreProfessionalPractices",
                component: ViewPreProfessionalPractices,
                children: [{
                        path: "list",
                        name: "listPreProfessionalPractices",
                        component: listPreProfessionalPractices
                    },
                    {
                        path: "add",
                        name: "addPreProfessionalPractices",
                        component: addPreProfessionalPractices
                    },

                ],
            },
            //------------- 
            {
                path: "process/criteria/:criteriaId/transferStudent",
                name: "TransferStudent",
                component: ViewTransferStudent,
                children: [{
                        path: "list",
                        name: "listTransferStudent",
                        component: listTransferStudent
                    },
                    {
                        path: "add",
                        name: "addTransferStudent",
                        component: addTransferStudent
                    },

                ],
            },
            //------------- 

            //---------FIN---------

            {
                path: "process/criteria/:criteriaId/training",
                name: "ViewTraining",
                component: ViewTraining,
                children: [{
                        path: "list/:status",
                        name: "ListTraining",
                        component: ListTraining
                    },

                ],
            },

            {
                path: "process",
                name: "Process",
                component: ViewProcess,
                children: [{
                        path: "list",
                        name: "ListProcess",
                        component: ListProcess
                    },
                    {
                        path: "add",
                        name: "NewProcess",
                        component: NewProcess
                    },
                    {
                        path: ":processId/detail",
                        name: "DetailProcess",
                        component: DetailProcess,
                    },
                    {
                        path: ":processId/edit",
                        name: "EditProcess",
                        component: EditProcess,
                    },
                ],
            },
            {
                path: "criteria",
                name: "Criteria",
                component: ViewCriteria,
                children: [{
                        path: "list",
                        name: "ListCriteria",
                        component: ListCriteria
                    },
                    {
                        path: ":criteriaId/detail",
                        name: "DetailCriteria",
                        component: DetailCriteria,
                    },
                    {
                        path: ":criteriaId/edit",
                        name: "EditCriteria",
                        component: EditCriteria,
                    },
                ],
            },

            {
                path: "users",
                name: "Users",
                component: ViewUsers,
                children: [{
                        path: "list",
                        component: ListUser,
                        name: "ListUser"
                    },
                    {
                        path: ":userId/edit",
                        name: "EditUser",
                        component: EditUser
                    },
                    {
                        path: ":userId/perfil",
                        name: "PerfilUser",
                        component: PerfilUser,
                    },
                    {
                        path: ":userId/delete",
                        name: "DeleteUser",
                        component: DeleteUser,
                    },
                    {
                        path: ":userId/deactivate",
                        name: "DeactivateUser",
                        component: DeactivateUser,
                    },
                    {
                        path: ":userId/activate",
                        name: "ActivateUser",
                        component: ActivateUser,
                    },
                    {
                        path: "register",
                        name: "RegisterUser",
                        component: RegisterUser,
                    },
                ],
            },
            {
                path: "profile",
                name: "Profile",
                component: ViewProfileUser,
                children: [{
                        path: "/profileuser",
                        name: "ProfileUser",
                        component: ProfileUser
                    },
                    {
                        path: "/edit",
                        name: "EditProfileUser",
                        component: EditProfileUser,
                    },
                ],
            },
            {
                path: "roles",
                name: "Roles",
                component: ViewRoles,
                children: [{
                    path: "/editPermissions",
                    name: "EditPermissions",
                    component: EditPermissions,
                }, ],
            },
            {
                path: "process/:processId/studyPlan",
                name: "StudyPlan",
                component: ViewStudyPlan,
                children: [{
                        path: "list",
                        name: "ListStudyPlan",
                        component: ListStudyPlan
                    },
                    {
                        path: "add",
                        name: "NewStudyPlan",
                        component: NewStudyPlan
                    },
                    {
                        path: ":studyPlanId/detail",
                        name: "DetailStudyPlan",
                        component: DetailStudyPlan,
                    },
                    {
                        path: ":studyPlanId/edit",
                        name: "EditStudyPlan",
                        component: EditStudyPlan
                    },
                ],
            },
            {
                path: "process/:processId/studyPlan/:studyPlanId/competence",
                name: "Competence",
                component: ViewCompetence,
                children: [{
                        path: "list",
                        name: "ListCompetence",
                        component: ListCompetence
                    },
                    {
                        path: "add",
                        name: "NewCompetence",
                        component: NewCompetence
                    },
                    {
                        path: ":competenceId/detail",
                        name: "DetailCompetence",
                        component: DetailCompetence,
                    },
                    {
                        path: ":competenceId/edit",
                        name: "EditCompetence",
                        component: EditCompetence,
                    },
                ],
            },
            {
                path: "process/:processId/studyPlan/:studyPlanId/formationArea",
                name: "FormationArea",
                component: ViewFormationArea,
                children: [{
                        path: "list",
                        name: "ListFormationArea",
                        component: ListFormationArea
                    },
                    {
                        path: "add",
                        name: "NewFormationArea",
                        component: NewFormationArea
                    },
                    {
                        path: ":formationAreaId/detail",
                        name: "DetailFormationArea",
                        component: DetailFormationArea,
                    },
                    {
                        path: ":formationAreaId/edit",
                        name: "EditFormationArea",
                        component: EditFormationArea,
                    }
                ],
            },
            {
                path: "process/:processId/studyPlan/:studyPlanId/sommeliers",
                name: "Sommeliers",
                component: ViewSommeliers,
                children: [{
                        path: "list",
                        name: "ListSommeliers",
                        component: ListSommeliers
                    },
                    {
                        path: "add",
                        component: NewSommeliers
                    }
                ],
            },
            {
                path: "process/:processId/studyPlan/:studyPlanId/matchCourseVsResults",
                name: "MatchCourseVsResults",
                component: ViewMatchCourseVsResults,
            },
            {
                path: "process/:processId/studyPlan/:studyPlanId/matchCourseVsCompetence",
                name: "MatchCourseVsCompetence",
                component: ViewMatchCourseVsCompetence,
            },
            {
                path: 'process/:processId/studyPlan/:studyPlanId/matchCompetencesVsResults',
                name: 'MatchCompetencesVsResults',
                component: ViewMatchCompetencesVsResults
            },
            {
                path: "process/:processId/studyPlan/:studyPlanId/studentResult",
                name: "StudentResult",
                component: ViewStudentResult,
                children: [{
                        path: "list",
                        name: "ListStudentResult",
                        component: ListStudentResult
                    },
                    {
                        path: "add",
                        name: "NewStudentResult",
                        component: NewStudentResult
                    },
                    {
                        path: ":studentResultId/detail",
                        name: "DetailStudentResult",
                        component: DetailStudentResult,
                    },
                    {
                        path: ":studentResultId/edit",
                        name: "EditStudentResult",
                        component: EditStudentResult,
                    },
                ],
            },
            {
                path: "process/:processId/studyPlan/:studyPlanId/course",
                name: "curriculum",
                component: ViewCurriculum,
                children: [{
                        path: "list",
                        name: "ListCCurse",
                        component: ListCCurse
                    },
                    {
                        path: "add",
                        name: "AddCCurse",
                        component: AddCCurse
                    },
                    {
                        path: ":ccurseId/detail",
                        name: "DetailCCurse",
                        component: DetailCCurse,
                    },
                    {
                        path: ":ccurseId/edit",
                        name: "EditCCcurse",
                        component: EditCCurse,
                    },
                ],
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/curriculumVitae/:idUser",
                name: "CurriculumVitae",
                component: ViewCurriculumVitae,
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/curriculumVitaeList",
                name: "CurriculumVitaeList",
                component: AdminViewCurriculumVitae,
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/entranceExamination",
                name: "ExamMenu",
                component: ExamMenu,
                children: [{
                        path: "exam",
                        name: "Exam",
                        component: Exam
                    },
                    {
                        path: "solution",
                        name: "Solution",
                        component: Solution
                    },
                    {
                        path: "report",
                        name: "Report",
                        component: Report
                    },


                ]
            },

            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/fperiod",
                name: "Fperiod",
                component: Fperiod,
                children: [{
                        path: "evaluationReport",
                        name: "EvaluationReport",
                        component: EvaluationReport,
                    },
                    {
                        path: "evaluationReportPDF",
                        name: "EvaluationReportPDF",
                        component: EvaluationReportPDF,
                    },
                    {
                        path: "examEvidences",
                        name: "ExamEvidences",
                        component: ExamEvidences,
                    },
                    {
                        path: "continuosEvidences",
                        name: "ContinuosEvidences",
                        component: ContinuosEvidences,
                    },
                    {
                        path: "courseEvidences",
                        name: "CourseEvidences",
                        component: CourseEvidences,
                    },

                ]
            },

            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/speriod",
                name: "Speriod",
                component: Speriod,
                children: [{
                        path: "evaluationReportSecond",
                        name: "EvaluationReportSecond",
                        component: EvaluationReportSecond,
                    },
                    {
                        path: "evaluationReportSecondPDF",
                        name: "EvaluationReportSecondPDF",
                        component: EvaluationReportSecondPDF,
                    },
                    {
                        path: "examEvidencesSecond",
                        name: "ExamEvidencesSecond",
                        component: ExamEvidencesSecond,
                    },
                    {
                        path: "continuosEvidencesSecond",
                        name: "ContinuosEvidencesSecond",
                        component: ContinuosEvidencesSecond,
                    },
                    {
                        path: "courseEvidencesSecond",
                        name: "CourseEvidencesSecond",
                        component: CourseEvidencesSecond,
                    },
                ]
            },

            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/tperiod",
                name: "Tperiod",
                component: Tperiod,
                children: [{
                        path: "evaluationReportThird",
                        name: "EvaluationReportThird",
                        component: EvaluationReportThird,
                    },
                    {
                        path: "evaluationReportThirdPDF",
                        name: "EvaluationReportThirdPDF",
                        component: EvaluationReportThirdPDF,
                    },
                    {
                        path: "examEvidencesThird",
                        name: "ExamEvidencesThird",
                        component: ExamEvidencesThird,
                    },
                    {
                        path: "continuosEvidencesThird",
                        name: "ContinuosEvidencesThird",
                        component: ContinuosEvidencesThird,
                    },
                    {
                        path: "courseEvidencesThird",
                        name: "CourseEvidencesThird",
                        component: CourseEvidencesThird,
                    },
                ]
            },

            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/silaboABET",
                name: "SilaboABET",
                component: ViewSilaboABET,
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/",
                name: "viewCourses",
                component: ViewCourses,
                children: [{
                        path: "ListCourses",
                        name: "listCourses",
                        component: ListCourses,
                    },
                    {
                        path: "AddCourses",
                        name: "AddCourses",
                        component: AddCourses,
                    },
                    {
                        path: ":courseId/EditCourses",
                        name: "editCourses",
                        component: EditCourses,
                    },
                    {
                        path: ":courseId/DeleteCourses",
                        name: "deleteCourses",
                        component: DeleteCourses,
                    },
                    {
                        path: ":courseId/DeactivateCourse",
                        name: "deactivateCourse",
                        component: DeactivateCourse,
                    },
                    {
                        path: ":courseId/ActivateCourse",
                        name: "activateCourse",
                        component: ActivateCourse,
                    },


                ]
            },

            {
                path: "Portfolio/:portfolioId/viewStages",
                name: "viewStages",
                component: ViewStages,
                children: [{
                        path: ":courseId/Stages",
                        name: "stages",
                        component: Stages,
                    },
                    {
                        path: ":courseId/EditStages",
                        name: "editStages",
                        component: EditStages,
                    },

                ]
            },

            {
                path: "Portfolio/:portfolioId/viewDocentes",
                name: "viewDocentes",
                component: ViewDocentes,
                children: [{
                        path: ":courseId/ResponsableCourses",
                        name: "responsableCourses",
                        component: ResponsableCourses,
                    },
                    {
                        path: ":courseId/EditResponsable",
                        name: "editResponsable",
                        component: EditResponsable,
                    },
                    {
                        path: ":courseId/DeleteResponsable",
                        name: "deleteResponsable",
                        component: DeleteResponsable,
                    },
                ]
            },

            {
                path: "Portfolio/:portfolioId/students",
                name: "ListStudents",
                component: ViewStudent,
                children: [{
                    path: ":courseId/list",
                    name: "ListStudents",
                    component: ListStudents
                }]
            },
            {
                path: "ViewPortfolio",
                name: "viewPortfolio",
                component: ViewPortfolio,
                children: [{
                        path: "AddPortfolio",
                        name: "addPortfolio",
                        component: AddPortfolio,
                    },
                    {
                        path: "ListPortfolios",
                        name: "listPortfolios",
                        component: ListPortfolios,
                    },
                    {
                        path: ":portfolioId/EditPortfolio",
                        name: "editPortfolio",
                        component: EditPortfolio,
                    },
                    {
                        path: "DeletePortfolio",
                        name: "deletePortfolio",
                        component: DeletePortfolio,
                    },
                    {
                        path: "DeactivatePortfolio",
                        name: "deactivatePortfolio",
                        component: DeactivatePortfolio,
                    },
                ]
            },



            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/ListSyllabus",
                name: "listSyllabus",
                component: ListSyllabus,
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports",
                name: "Reports",
                component: Reports,
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/Performance",
                name: "Performance",
                component: ViewReportPerformance,
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/Performance/report",
                name: "ReportPerformance",
                component: ReportPerformance,
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/ContinuousImprovement",
                name: "ContinuousImprovement",
                component: ViewReportContinuousImprovement,
                children: [{
                    path: "",
                    name: "DetailReportContinuousImprovement",
                    component: DetailReportContinuousImprovement,
                }, ],
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/AcademicAdvisory",
                name: "AcademicAdvisory",
                component: ViewReportAcademicAdvisory,
                children: [{
                    path: "",
                    name: "DetailReportAcademicAdvisory",
                    component: DetailReportAcademicAdvisory,
                }],
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/Average",
                name: "Average",
                component: ViewReportAverage,
                children: [{
                    path: "",
                    name: "ListReportAverage",
                    component: ListReportAverage,
                }, ],
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/StudentResult",
                name: "ViewReportStudentResult",
                component: ViewReportStudentResult,
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/StudentResult/Form/:academicPeriodId",
                name: "FormReportStudentResult",
                component: FormReportStudentResult,
            },
            {
                path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/AcademicDevelopment",
                name: "AcademicDevelopment",
                component: ViewAcademicDevelopment,
                children: [{
                        path: "",
                        name: "ListAcademicDevelopment",
                        component: ListAcademicDevelopment
                    },
                    {
                        path: "add",
                        name: "NewAcademicDevelopment",
                        component: NewAcademicDevelopment
                    },
                    {
                        path: ":idAD/edit",
                        name: "EditAcademicDevelopment",
                        component: EditAcademicDevelopment,
                    },
                ],
            },
            {
                path: "Portfolio/:portfolioId/:courseId/silaboDufa",
                name: "SilaboDufa",
                component: ViewSilaboDufa,
                children: [{
                        path: "viewEvaluacionSilaboDufa",
                        name: "ViewEvaluacionSilaboDufa",
                        component: ViewEvaluacionSilaboDufa,
                        children: [{
                            path: "evaluationAcademicPeriod/:academicPeriodId",
                            name: "EvaluationAcademicPeriod",
                            component: EvaluationAcademicPeriod,
                            children: [{
                                    path: "determinarRubricasEvaluacion",
                                    name: "DeterminarRubricasEvaluacion",
                                    component: DeterminarRubricasEvaluacion
                                },
                                {
                                    path: "determinarCompetencias",
                                    name: "DeterminarCompetencias",
                                    component: DeterminarCompetencias
                                },
                                {
                                    path: "determinarCriterios",
                                    name: "DeterminarCriterios",
                                    component: DeterminarCriterios
                                },

                            ]
                        }, ]
                    },
                    {
                        path: "viewCompetenciaSilaboDufa",
                        name: "ViewCompetenciaSilaboDufa",
                        component: ViewCompetenciaSilaboDufa,
                        children: [

                        ]
                    },
                    {
                        path: "viewMedResultSilaboDufa",
                        name: "ViewMedResultSilaboDufa",
                        component: ViewMedResultSilaboDufa,
                        children: [

                        ]
                    },
                    {
                        path: "silaboDufa",
                        name: "SilaboDufa",
                        component: SilaboDufa,
                        children: [

                        ]
                    },
                ],
            },

        ],
    },

    {
        path: "/login",
        name: "LoginUser",
        component: LoginUser,
    },
    {
        path: "/recover",
        name: "Recover",
        component: Recover,
    },
    {
        path: "/ResetPassword/:uidb64/:tokenReset",
        name: "ResetPassword",
        component: ResetPassword,
    },
    {
        path: "/process/new",
        name: "NewProcess",
        component: NewProcess,
    },

    {
        path: "/criteria/:criteriaId/edit",
        name: "EditCriteria",
        component: EditCriteria,
    },

    {
        path: "/indicator/:indicatorId/edit",
        name: "EditIndicator",
        component: EditIndicator,
    },

];

const router = new VueRouter({
    base: process.env.BASE_URL,
    routes,
});

export default router;