import axios from 'axios';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'bootstrap/dist/css/bootstrap.css';
import Vue from 'vue';
import VueResource from 'vue-resource';
import App from './App.vue';
import router from './router';
import store from './store';
import { VueSpinners} from '@saeris/vue-spinners'

Vue.use(VueResource)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueSpinners)

Vue.config.productionTip = false

//Condcion para saber si existe un usuario logueado mediante su token
var token = store.state.token;
if (token) {
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
} else {
  axios.defaults.headers.common['Authorization'] = null;
}


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
  