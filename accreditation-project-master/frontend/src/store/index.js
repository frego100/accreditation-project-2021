import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';


Vue.use(Vuex)

//Variables que se usaran para hacer persistentes en el sistema usando el vuex-store
const getDefaultState = () => {
  return {
    token: '',
    id: '',
    p: [],
    idProceso: '',
    usuarios: [],
    superadmin: '',
    isResProceso: '',
    isRespPortCurso: '',
    isColabPortCurso: '',
    isRespCursoAct: '',
    fechaActual: '',
    fechaInicio: '',
    fechaFinFase1: '',
    fechaFinFase2: '',
    fechaFinFase3: '',
    fechaFinFase4: '',
    //Agregando esto las Variables de Colaboradores como entorno
    collaborator_silabet: '',
    collaborator_siladufa:'',
    collaborator_anedufa:'',
    collaborator_currivitae:'',
    collaborator_examentrada:'',
    collaborator_eviexamen_priperiodo:'',
    collaborator_evievacontinua_priperiodo:'',
    collaborator_evicurso_priperiodo:'',
    collaborator_reportes_priperiodo:'',
    collaborator_eviexamen_segperiodo:'',
    collaborator_evievacontinua_segperiodo:'',
    collaborator_evicurso_segperiodo:'',
    collaborator_reportes_segperiodo:'',
    collaborator_eviexamen_terperiodo:'',
    collaborator_evievacontinua_terperiodo:'',
    collaborator_evicurso_terperiodo:'',
    collaborator_reportes_terperiodo:'',
    collaborator_infdesempeno:'',
    collaborator_infresestudiante:'',
    collaborator_infaseacademica:'',
    collaborator_infmejcontinua:'',
    collaborator_inffinal:'',
    /*criterio
    criterio01_responsable:'',
    criterio02_responsable:'',
    criterio03_responsable:'',
    criterio04_responsable:'',
    criterio05_responsable:'',
    criterio06_responsable:'',
    criterio07_responsable:'',
    criterio08_responsable:'',*/
  };
};

//Aca se manejan el state (que son las variables que declare arriba para que se hagan persistente)
//luego usamos getters que son metodos para obtener las variables declaradas
//el mutations donde se llenan datos en las variables
//actiones donde se llama al mutatios a ejecutar desde codigo mismo 
export default new Vuex.Store({
  state: getDefaultState(),
  getters: {
    isLoggedIn: state => {
      return state.token;
    },
    getId: state => {
      return state.id;
    },
    idProceso: state => {
      return state.idProceso;
    },
    getPermisos: state => {
      return state.p;
    },
    isSuperadmin: state => {
      return state.superadmin;
    },
    isResProceso: state => {
      return state.isResProceso;
    },
    isRespPortCurso: state => {
      return state.isRespPortCurso;
    },
    isColabPortCurso: state => {
      return state.isColabPortCurso;
    },
    isRespCursoAct: state => {
      return state.isRespCursoAct;
    },
    getFechaActual: state => {
      return state.fechaActual;
    },
    getFechaInicio: state => {
      return state.fechaInicio;
    },
    getFechaFinFase1: state => {
      return state.fechaFinFase1;
    },
    getFechaFinFase2: state => {
      return state.fechaFinFase2;
    },
    getFechaFinFase3: state => {
      return state.fechaFinFase3;
    },
    getFechaFinFase4: state => {
      return state.fechaFinFase4;
    },
    getUsuarios: state => {
      return state.usuarios;
    },
    isColabSilAbet: state => {
      return state.collaborator_silabet;
    },
    isColabSilDuf: state => {
      return state.collaborator_siladufa;
    },
    isColabAneDuf: state => {
      return state.collaborator_anedufa;
    },
    isColabCurVit: state => {
      return state.collaborator_currivitae;
    },
    isColabExamEnt: state => {
      return state.collaborator_examentrada;
    },
    isColabEviExamPri: state => {
      return state.collaborator_eviexamen_priperiodo;
    },
    isColabEviEvaConPri: state => {
      return state.collaborator_evievacontinua_priperiodo;
    },
    isColabEviCursPri: state => {
      return state.collaborator_evicurso_priperiodo;
    },
    isColabRepPri: state => {
      return state.collaborator_reportes_priperiodo;
    },
    isColabEviExamSeg: state => {
      return state.collaborator_eviexamen_segperiodo;
    },
    isColabEviEvaConSeg: state => {
      return state.collaborator_evievacontinua_segperiodo;
    },
    isColabEviCursSeg: state => {
      return state.collaborator_evicurso_segperiodo;
    }, 
    isColabRepSeg: state => {
      return state.collaborator_reportes_segperiodo;
    },
    isColabEviExamter: state => {
      return state.collaborator_eviexamen_terperiodo;
    },
    isColabEviEvaConTer: state => {
      return state.collaborator_evievacontinua_terperiodo;
    },
    isColabEviCursTer: state => {
      return state.collaborator_evicurso_terperiodo;
    },
    isColabRepTer: state => {
      return state.collaborator_reportes_terperiodo;
    },
    isColabInfDes: state => {
      return state.collaborator_infdesempeno;
    },
    isColabInfEst: state => {
      return state.collaborator_infresestudiante;
    },
    isColabInfAca: state => {
      return state.collaborator_infaseacademica;
    },
    isColabInfMejcon: state => {
      return state.collaborator_infmejcontinua;
    },
    isColabInfFin: state => {
      return state.collaborator_inffinal;
    },
    // INICIO
    //criteria
    isCriterio01Responsable: state => {
      return state.criterio01_responsable;
    },
    //criteria
    isCriterio02Responsable: state => {
      return state.criterio02_responsable;
    }/*,
    //criteria 
    isCriterio03Responsable: state => {
      return state.criterio03_responsable;
    },
    //criteria
    isCriterio04Responsable: state => {
      return state.criterio04_responsable;
    },
    //criteria
    isCriterio05Responsable: state => {
      return state.criterio05_responsable;
    },
    //criteria
    isCriterio06Responsable: state => {
      return state.criterio06_responsable;
    },
    //criteria
    isCriterio07Responsable: state => {
      return state.criterio07_responsable;
    },
    //criteria
    isCriterio08Responsable: state => {
      return state.criterio08_responsable;
    }*/

  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_ID: (state, id) => {
      state.id = id;
    },
    SET_IDPROCESO: (state, idProceso) => {
      state.idProceso = idProceso;
    },
    SET_PERMISOS: (state, p) => {
      state.p = p;
    },
    SET_SUPERADMIN: (state, superadmin) => {
      state.superadmin = superadmin;
    },
    SET_ISRESPPROCESO: (state, isResProceso) => {
      state.isResProceso = isResProceso;
    },
    SET_ISRESPPORTCURSO: (state, isRespPortCurso) => {
      state.isRespPortCurso = isRespPortCurso;
    },
    SET_ISCOLABPORTCURSO: (state, isColabPortCurso) => {
      state.isColabPortCurso = isColabPortCurso;
    },
    SET_ISRESPCURSOACT: (state, isRespCursoAct) => {
      state.isRespCursoAct = isRespCursoAct;
    },
    SET_FECHAACTUAL: (state, fechaActual) => {
      state.fechaActual = fechaActual;
    },
    SET_FECHAINICIO: (state, fechaInicio) => {
      state.fechaInicio = fechaInicio;
    },
    SET_FECHAFINFASE1: (state, fechaFinFase1) => {
      state.fechaFinFase1 = fechaFinFase1;
    },
    SET_FECHAFINFASE2: (state, fechaFinFase2) => {
      state.fechaFinFase2 = fechaFinFase2;
    },
    SET_FECHAFINFASE3: (state, fechaFinFase3) => {
      state.fechaFinFase3 = fechaFinFase3;
    },
    SET_FECHAFINFASE4: (state, fechaFinFase4) => {
      state.fechaFinFase4 = fechaFinFase4;
    },
    SET_USUARIOS: (state, usuarios) => {
      state.usuarios = usuarios;
    },

    // inicio del mame
    SET_collaborator_silabet: (state,collaborator_silabet) => {
      state.collaborator_silabet =collaborator_silabet;
    },
    SET_collaborator_siladufa: (state,collaborator_siladufa) => {
      state.collaborator_siladufa =collaborator_siladufa;
    },
    SET_collaborator_anedufa: (state,collaborator_anedufa) => {
      state.collaborator_anedufa =collaborator_anedufa;
    },
    SET_collaborator_currivitae: (state,collaborator_currivitae) => {
      state.collaborator_currivitae =collaborator_currivitae;
    },
    SET_collaborator_examentrada: (state,collaborator_examentrada) => {
      state.collaborator_examentrada =collaborator_examentrada;
    },
    SET_collaborator_eviexamen_priperiodo: (state,collaborator_eviexamen_priperiodo) => {
      state.collaborator_eviexamen_priperiodo =collaborator_eviexamen_priperiodo;
    },
    SET_collaborator_evievacontinua_priperiodo: (state,collaborator_evievacontinua_priperiodo) => {
      state.collaborator_evievacontinua_priperiodo =collaborator_evievacontinua_priperiodo;
    },
    SET_collaborator_evicurso_priperiodo: (state,collaborator_evicurso_priperiodo) => {
      state.collaborator_evicurso_priperiodo =collaborator_evicurso_priperiodo;
    },
    SET_collaborator_reportes_priperiodo: (state,collaborator_reportes_priperiodo) => {
      state.collaborator_reportes_priperiodo =collaborator_reportes_priperiodo;
    },
    SET_collaborator_eviexamen_segperiodo: (state,collaborator_eviexamen_segperiodo) => {
      state.collaborator_eviexamen_segperiodo =collaborator_eviexamen_segperiodo;
    },
    SET_collaborator_evievacontinua_segperiodo: (state,collaborator_evievacontinua_segperiodo) => {
      state.collaborator_evievacontinua_segperiodo =collaborator_evievacontinua_segperiodo;
    },
    SET_collaborator_evicurso_segperiodo: (state,collaborator_evicurso_segperiodo) => {
      state.collaborator_evicurso_segperiodo =collaborator_evicurso_segperiodo;
    },
    SET_collaborator_reportes_segperiodo: (state,collaborator_reportes_segperiodo) => {
      state.collaborator_reportes_segperiodo =collaborator_reportes_segperiodo;
    },
    SET_collaborator_eviexamen_terperiodo: (state,collaborator_eviexamen_terperiodo) => {
      state.collaborator_eviexamen_terperiodo =collaborator_eviexamen_terperiodo;
    },
    SET_collaborator_evievacontinua_terperiodo: (state,collaborator_evievacontinua_terperiodo) => {
      state.collaborator_evievacontinua_terperiodo =collaborator_evievacontinua_terperiodo;
    },
    SET_collaborator_evicurso_terperiodo: (state,collaborator_evicurso_terperiodo) => {
      state.collaborator_evicurso_terperiodo =collaborator_evicurso_terperiodo;
    },
    SET_collaborator_reportes_terperiodo: (state,collaborator_reportes_terperiodo) => {
      state.collaborator_reportes_terperiodo =collaborator_reportes_terperiodo;
    },
    SET_collaborator_infdesempeno: (state,collaborator_infdesempeno) => {
      state.collaborator_infdesempeno =collaborator_infdesempeno;
    },
    SET_collaborator_infresestudiante: (state,collaborator_infresestudiante) => {
      state.collaborator_infresestudiante =collaborator_infresestudiante;
    },
    SET_collaborator_infaseacademica: (state,collaborator_infaseacademica) => {
      state.collaborator_infaseacademica =collaborator_infaseacademica;
    },
    SET_collaborator_infmejcontinua: (state,collaborator_infmejcontinua) => {
      state.collaborator_infmejcontinua =collaborator_infmejcontinua;
    },
    SET_collaborator_inffinal: (state,collaborator_inffinal) => {
      state.collaborator_inffinal =collaborator_inffinal;
    },


    //fin del mame
    RESET: state => {
      Object.assign(state, getDefaultState());
    },
  },
  actions: {
    login: ({ commit }, { token }) => {
      commit('SET_TOKEN', token);
    },
    userId: ({ commit }, { id }) => {
      commit('SET_ID', id);
    },
    idProceso: ({ commit }, { idProceso }) => {
      commit('SET_IDPROCESO', idProceso);
    },
    permisos: ({ commit }, { p }) => {
      commit('SET_PERMISOS', p);
    },
    superadmin: ({ commit }, { superadmin }) => {
      commit('SET_SUPERADMIN', superadmin);
    },
    isResProceso: ({ commit }, { isResProceso }) => {
      commit('SET_ISRESPPROCESO', isResProceso);
    },
    isRespPortCurso: ({ commit }, { isRespPortCurso }) => {
      commit('SET_ISRESPPORTCURSO', isRespPortCurso);
    },
    isColabPortCurso: ({ commit }, { isColabPortCurso }) => {
      commit('SET_ISCOLABPORTCURSO', isColabPortCurso);
    },
    isRespCursoAct: ({ commit }, { isRespCursoAct }) => {
      commit('SET_ISRESPCURSOACT', isRespCursoAct);
    },
    fechaActual: ({ commit }, { fechaActual }) => {
      commit('SET_FECHAACTUAL', fechaActual);
    },
    fechaInicio: ({ commit }, { fechaInicio }) => {
      commit('SET_FECHAINICIO', fechaInicio);
    },
    fechaFinFase1: ({ commit }, { fechaFinFase1 }) => {
      commit('SET_FECHAFINFASE1', fechaFinFase1);
    },
    fechaFinFase2: ({ commit }, { fechaFinFase2 }) => {
      commit('SET_FECHAFINFASE2', fechaFinFase2);
    },
    fechaFinFase3: ({ commit }, { fechaFinFase3 }) => {
      commit('SET_FECHAFINFASE3', fechaFinFase3);
    },
    fechaFinFase4: ({ commit }, { fechaFinFase4 }) => {
      commit('SET_FECHAFINFASE4', fechaFinFase4);
    },
    usuarios: ({ commit }, { usuarios }) => {
      commit('SET_USUARIOS', usuarios);
    },
    //inicio de Fedeada
    collaborator_silabet: ({ commit }, { collaborator_silabet }) => {
      commit('SET_collaborator_silabet', collaborator_silabet);
    },
    collaborator_siladufa:({ commit }, { collaborator_siladufa }) => {
      commit('SET_collaborator_siladufa', collaborator_siladufa);
    },
    collaborator_anedufa:({ commit }, { collaborator_anedufa }) => {
      commit('SET_collaborator_anedufa', collaborator_anedufa);
    },
    collaborator_currivitae:({ commit }, { collaborator_currivitae }) => {
      commit('SET_collaborator_currivitae', collaborator_currivitae);
    },
    collaborator_examentrada:({ commit }, { collaborator_examentrada }) => {
      commit('SET_collaborator_examentrada', collaborator_examentrada);
    },
    collaborator_eviexamen_priperiodo:({ commit }, { collaborator_eviexamen_priperiodo }) => {
      commit('SET_collaborator_eviexamen_priperiodo', collaborator_eviexamen_priperiodo);
    },
    collaborator_evievacontinua_priperiodo:({ commit }, { collaborator_evievacontinua_priperiodo }) => {
      commit('SET_collaborator_evievacontinua_priperiodo', collaborator_evievacontinua_priperiodo);
    },
    collaborator_evicurso_priperiodo:({ commit }, { collaborator_evicurso_priperiodo }) => {
      commit('SET_collaborator_evicurso_priperiodo', collaborator_evicurso_priperiodo);
    },
    collaborator_reportes_priperiodo:({ commit }, { collaborator_reportes_priperiodo }) => {
      commit('SET_collaborator_reportes_priperiodo', collaborator_reportes_priperiodo);
    },
    collaborator_eviexamen_segperiodo:({ commit }, { collaborator_eviexamen_segperiodo }) => {
      commit('SET_collaborator_eviexamen_segperiodo', collaborator_eviexamen_segperiodo);
    },
    collaborator_evievacontinua_segperiodo:({ commit }, { collaborator_evievacontinua_segperiodo }) => {
      commit('SET_collaborator_evievacontinua_segperiodo', collaborator_evievacontinua_segperiodo);
    },
    collaborator_evicurso_segperiodo:({ commit }, { collaborator_evicurso_segperiodo }) => {
      commit('SET_collaborator_evicurso_segperiodo', collaborator_evicurso_segperiodo);
    },
    collaborator_reportes_segperiodo:({ commit }, { collaborator_reportes_segperiodo }) => {
      commit('SET_collaborator_reportes_segperiodo', collaborator_reportes_segperiodo);
    },
    collaborator_eviexamen_terperiodo:({ commit }, { collaborator_eviexamen_terperiodo }) => {
      commit('SET_collaborator_eviexamen_terperiodo', collaborator_eviexamen_terperiodo);
    },
    collaborator_evievacontinua_terperiodo:({ commit }, { collaborator_evievacontinua_terperiodo }) => {
      commit('SET_collaborator_evievacontinua_terperiodo', collaborator_evievacontinua_terperiodo);
    },
    collaborator_evicurso_terperiodo:({ commit }, { collaborator_evicurso_terperiodo }) => {
      commit('SET_collaborator_evicurso_terperiodo', collaborator_evicurso_terperiodo);
    },
    collaborator_reportes_terperiodo:({ commit }, { collaborator_reportes_terperiodo }) => {
      commit('SET_collaborator_reportes_terperiodo', collaborator_reportes_terperiodo);
    },
    collaborator_infdesempeno:({ commit }, { collaborator_infdesempeno }) => {
      commit('SET_collaborator_infdesempeno', collaborator_infdesempeno);
    },
    collaborator_infresestudiante:({ commit }, { collaborator_infresestudiante }) => {
      commit('SET_collaborator_infresestudiante', collaborator_infresestudiante);
    },
    collaborator_infaseacademica:({ commit }, { collaborator_infaseacademica }) => {
      commit('SET_collaborator_infaseacademica', collaborator_infaseacademica);
    },
    collaborator_infmejcontinua:({ commit }, { collaborator_infmejcontinua }) => {
      commit('SET_collaborator_infmejcontinua', collaborator_infmejcontinua);
    },
    collaborator_inffinal:({ commit }, { collaborator_inffinal }) => {
      commit('SET_collaborator_inffinal', collaborator_inffinal);
    },
    //fin de Fedeada
    logout: ({ commit }) => {
      commit('RESET', '');
    }
  },
  plugins: [createPersistedState()],
})
