import axios from 'axios';
const url = process.env.VUE_APP_RUTA_API;

export default {
    //metodo para obtener token de un usuario enviando sus credenciales (correo,contraseña)
    async obtain_token (credentials) {
        const response = await axios
            .post(url + 'user/obtain_token/', credentials);
        return response.data;
    },
    //metodo para extender el tiempo de vida de un token
    async refresh_token (token) {
        const response = await axios
            .post(url + 'user/refresh_token/', token);
        return response.data;
    },
    //metodo para registrar un usuario enviando sus credenciales(correo,contraseña)
    async register (credentials) {
        const response = 
        await axios
            .post(url + 'user/user/', credentials).then((response) => {
                console.log(response);
              }, (error) => {
                console.log(error);
              });
        //return response.data;
        return response;
    }
    ,
    //metodo para obtener todos los datos de un usuario logueado
    async getUser () {
        const response = await axios.get(url + 'user/active/');
        return response.data;
    },
    //metodo para obtener el rol de un usuario mediante su id
    async getRol (id) {
        const response = await axios.get(url + 'user/role/' + id + '/');
        return response.data;
    },
    async request_reset_email (email) {
        const response = await axios.post(url + 'user/request-reset-email', email);
        return response.data;
    },
    async changePassword (credentials) {
        const response = await axios.patch(url + 'user/password-reset-complete', credentials);
        return response.data;
    },
    async validationTokenChangePassword (uidb64, token) {
        const response = await axios.get(url + 'user/password-reset/' + uidb64 + '/' + token + '/');
        return response.data;
    }

};