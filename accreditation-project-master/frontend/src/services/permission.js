export const Permission = {
    Create: 0,
    View: 1,
    Edit: 2,
    Delete: 3,
    Disabled: 4
}


